<?php

// comment out the following two lines when deployed to production
$isDebug = include __DIR__ . '/config/debug.php';
defined('YII_DEBUG') or define('YII_DEBUG', $isDebug);
defined('YII_ENV') or define('YII_ENV', $isDebug ? 'dev' : 'prod');

require(__DIR__ . '/vendor/autoload.php');
require(__DIR__ . '/vendor/yiisoft/yii2/Yii.php');

$config = require(__DIR__ . '/config/web.php');

(new yii\web\Application($config))->run();
