<?php

namespace app\models;

use Yii;
use yii\base\Model;

class UserForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $repeatpassword;
    
    public function attributeLabels() {
        return [
            'username' => 'Логин',
            'email' => 'Email',
            'password' => 'Пароль',
            'repeatpassword' => 'Повтор пароля'
        ];
    }
    
    public function rules() {
        return [
            [['username', 'email', 'password', 'repeatpassword'], 'required', 'on' => ['insert']],
            [['username'], 'string', 'min' => '4', 'max' => 128, 'on' => ['insert']],
            [['email'], 'email', 'on' => ['insert']],
            [['password', 'repeatpassword'], 'string', 'min' => '6', 'max' => 128, 'on' => ['insert']],
            ['repeatpassword', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match", 'on' => ['insert'] ],
        ];
    }
}