<?php

namespace app\models;
/**
 * @property bool    $isAdmin
 * @property bool    $isBlocked
 * @property bool    $isConfirmed
 * 
 * @property string $username
 * @property string $email
 * @property string $password_hash
 * @property string $auth_key
 * @property integer $confirmed_at
 * @property string $unconfirmed_email
 * @property string $blocked_at
 * @property string $registration_ip
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $flags
 * @property integer $last_login_at
 * @property string $ios_token
 * @property string $android_token
 * @property Array $orgs модели организаций
 */
class User extends \dektrium\user\models\User implements \conquer\oauth2\OAuth2IdentityInterface
{
    /**
     * роли пользователя, полученные методом \Yii::$app->authManager->getRoles()
     * @var NULL|Array 
     */
    protected $currentUserRoles = NULL; 
    
    public static function find() 
    {
        return \Yii::createObject(UserQuery::className(), [get_called_class()]);
    }
    
    /**
     * роли пользователя, полученные методом \Yii::$app->authManager->getRoles()
     * @return Array
     */
    public function getCurrentUserRoles()
    {
        if(is_array($this->currentUserRoles))
        {
            return $this->currentUserRoles;
        }
        return $this->currentUserRoles = \Yii::$app->authManager->getRoles();
    }
    
    /**
     * поиск незаблокированного пользователя по логину
     * @param string $username
     * @return User|null
     */
    public static function findIdentityByUsername($username)
    {
        return self::find()
                ->andWhere(['blocked_at' => NULL])
                ->andWhere([
                    'OR', 
                    ['username' => (string)$username,], 
                    ['email' => (string)$username]
                   ])->one();
    }
    
    /**
     * валидация пароля
     * @param string $password
     * @return boolean
     */
    public function validatePassword($password)    
    {
        return \Yii::$app->security->validatePassword($password, $this->password_hash);
    }
    
    private $_roleList = NULL;
    /**
     * 
     * @return array
     */
    public function getRoleList()
    {
        if(is_array($this->_roleList))
        {
            return $this->_roleList;
        }
        $this->_roleList = \Yii::$app->authManager->getRolesByUser($this->id);
        return $this->_roleList;
    }
    
    public function getOrgs()
    {
        return $this->hasMany(\app\modules\org\models\Orgs::className(), ['id' => 'org_id'])
                ->viaTable(\app\modules\org\models\Staff::tableName(), ['user_id' => 'id']);
    }
    
    public function isDonor()
    {
        return in_array('donor', array_keys($this->getRoleList()));
    }
    
    public function isManager()
    {
        return in_array('manager', array_keys($this->getRoleList()));
    }
    
    public function isAdmin()
    {
        return in_array('admin', array_keys($this->getRoleList()));
    }
    
    public function beforeDelete() {
        if(\app\modules\donor\models\Donor::find()->where(['id' => $this->id])->limit(1)->count())
        {
            return false;
        }
        return parent::beforeDelete();
    }
    
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
    }
    
    public function create()
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        $transaction = $this->getDb()->beginTransaction();

        try {
            $this->password = $this->password == null ? \dektrium\user\helpers\Password::generate(8) : $this->password;

            $this->trigger(self::BEFORE_CREATE);

            if (!$this->save()) {
                $transaction->rollBack();
                return false;
            }

            $this->confirm();

            $this->sendWelcomeMessage();
            $this->trigger(self::AFTER_CREATE);

            $transaction->commit();

            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::warning($e->getMessage());
            throw $e;
        }
    }
    
    public function register()
    {
        if ($this->getIsNewRecord() == false) {
            throw new \RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }

        $transaction = $this->getDb()->beginTransaction();

        try {
            $this->confirmed_at = $this->module->enableConfirmation ? null : time();
            $this->password     = $this->module->enableGeneratingPassword ? \dektrium\user\helpers\Password::generate(8) : $this->password;

            $this->trigger(self::BEFORE_REGISTER);

            if (!$this->save()) {
                $transaction->rollBack();
                return false;
            }

            if ($this->module->enableConfirmation) {
                /** @var Token $token */
                $token = \Yii::createObject(['class' => \dektrium\user\models\Token::className(), 'type' => \dektrium\user\models\Token::TYPE_CONFIRMATION]);
                $token->link('user', $this);
            }

            $this->sendWelcomeMessage();
            $this->trigger(self::AFTER_REGISTER);

            $transaction->commit();

            return true;
        } catch (\Exception $e) {
            $transaction->rollBack();
            \Yii::warning($e->getMessage());
            throw $e;
        }
    }
    
    protected function sendWelcomeMessage()
    {
        \Yii::$app->mailer->compose('@app/mail/notifications/welcome', [])
                ->setFrom('no-reply@donorsystem.org')
                ->setTo($this->email)
                ->setSubject('Добро пожаловать на donorsystem.org')
                ->send();
    }
}

class UserQuery extends \yii\db\ActiveQuery
{
}

