<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$vk = require(__DIR__ . '/vk.php');
$requestCookieValidationKey = require(__DIR__ . '/request.php');
$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => $requestCookieValidationKey,
            'baseUrl' => '',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
            'defaultRoles' => ['admin']
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'app\components\User',
            'identityClass' => 'app\models\User',
        ],
        'mutex' => [
            'class' => 'yii\mutex\FileMutex'
        ],
      'view' => [
              'theme' => [
                  'pathMap' => [
                      '@dektrium/user/views' => '@app/views/user'
                  ],
              ],
          ],   
        'authClientCollection' => [
              'class' => 'yii\authclient\Collection',
              'clients' => [
                  'vkontakte' => $vk,
              ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
         'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => 'site/index',
                '<action:\w+>' => 'site/<action>',
                '<module:rest>/<controller:\w+>/<id:\d+>' => '<module>/<controller>/view',
                '<module:rest>/<controller:\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
                '<module:rest>/<controller:\w+>/<action:\w+>' => '<module>/<controller>/<action>',
            ],
           ],
    ],
    'modules' => [
      'rbac' => 'dektrium\rbac\RbacWebModule',
      'user' => [
            'class' => 'dektrium\user\Module',
            'admins' => ['admin'],
//            'controllerMap' => [
//            'registration' => [
//                'class' => \dektrium\user\controllers\RegistrationController::className(),
//                'on' . \dektrium\user\controllers\RegistrationController::EVENT_AFTER_REGISTER => 
//                /**
//                 * @param \dektrium\user\events\FormEvent $e
//                 */
//                function ($e) {
//                  $user = $e->sender;
//                  \Yii::$app->authManager->assign('user', $user->id);
//                }
//            ],
//        ],
        ],
        'rest' => 'app\modules\rest\Module',
        'donation' => 'app\modules\donation\Module',
        'donor' => 'app\modules\donor\Module',
        'org' => 'app\modules\org\Module',
        'time' => 'app\modules\time\Module'
      ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
