<?php
namespace modules\donor\components;
use app\modules\rest\components\DonorHelper;
class DonorHelperTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /**
     *
     * @var \yii\db\Transaction 
     */
    private $transaction;
    
    protected function _before()
    {
        $this->transaction = \Yii::$app->db->beginTransaction();
    }

    protected function _after()
    {
        $this->transaction->rollBack();
    }
    
    public function testUpdate()
    {
        \Yii::$app->user->logout();
        $helper = new DonorHelper();
        $data = [
            'surname' => 'TestSurnameUpdate',
            'name' => 'TestSurnameUpdate',
            'patronymic' => 'TestSurnameUpdate'
        ];

        //запрос при осутствии пользоватлея
        $resultArr = $helper->update($data, 0);    
        $this->tester->assertTrue($resultArr['code'] === DonorHelper::CODE_USER_NOT_FOUND);
        
        //запрос без авторизации
        $user = new \app\models\User();
        $user->username = uniqid();
        $user->password = uniqid('TEST');
        $user->email = uniqid().'@'.uniqid().'.ru';
        $user->confirmed_at = time();
        $user->save(false);
        $resultArr = $helper->update($data, $user->id);    
        $this->tester->assertTrue($resultArr['code'] === DonorHelper::CODE_NOT_ALLOWED_EDIT);
        
        //запрос с авторизацией и с донором
        \Yii::$app->user->login($user);
        $donor = new \app\modules\donor\models\Donor(['scenario' => 'insert']);
        $donor->id = $user->id;
        $donor->surname = 'TestSurname';
        $donor->name = 'TestName';
        $donor->patronymic = 'TestPatronymic';
        $donor->resus = true;
        $donor->group = 1;
        $donor->save();
        $resultArr = $helper->update($data, $user->id); 
        $updatedDonor = \app\modules\donor\models\Donor::find()->where(['id' => $donor->id])->limit(1)->one();
        $this->tester->assertTrue($resultArr['code'] === DonorHelper::CODE_SUCCESS
                && $updatedDonor->id === $donor->id
                && $updatedDonor->surname === $data['surname']
                && $updatedDonor->name === $data['name']
                && $updatedDonor->patronymic === $data['patronymic']);
        
        //запрос с авторизацией и без донора
        $updatedDonor->delete();
        $resultArr = $helper->update($data, $user->id); 
        $updatedDonor = \app\modules\donor\models\Donor::find()->where(['id' => $donor->id])->limit(1)->one();
        $this->tester->assertTrue($resultArr['code'] === DonorHelper::CODE_SUCCESS
                && $updatedDonor->id === $donor->id
                && $updatedDonor->surname === $data['surname']
                && $updatedDonor->name === $data['name']
                && $updatedDonor->patronymic === $data['patronymic']);
        
        //запрос с авторизацией и с ошибкой
        $data = [
            'surname' => '>',
            'name' => '',
            'patronymic' => '12121321564'
        ];
        $resultArr = $helper->update($data, $user->id); 
        $updatedDonor = \app\modules\donor\models\Donor::find()->where(['id' => $donor->id])->limit(1)->one();
        $this->tester->assertTrue($resultArr['code'] === DonorHelper::CODE_HAS_ERROR);
    }

    public function testView()
    {
        \Yii::$app->user->logout();
        $user = new \app\models\User();
        $user->username = uniqid();
        $user->password = uniqid('TEST');
        $user->email = uniqid().'@'.uniqid().'.ru';
        $user->confirmed_at = time();
        $user->save(false);
        $helper = new DonorHelper();
        
        //запрос без авторизации
        $resultArr = $helper->view($user->id);    
        $this->tester->assertTrue($resultArr['code'] === DonorHelper::CODE_NOT_ALLOWED_VIEW);
        
        //запрос с авторизацией без роли и без донора
        \Yii::$app->user->login($user);
        $resultArr = $helper->view($user->id);    
        $this->tester->assertTrue($resultArr['code'] == DonorHelper::CODE_DONOR_NOT_FOUND);
        
        //запрос с авторизацией без роли и с донором
        $donor = new \app\modules\donor\models\Donor(['scenario' => 'insert']);
        $donor->id = $user->id;
        $donor->surname = 'TestSurname';
        $donor->name = 'TestName';
        $donor->patronymic = 'TestPatronymic';
        $donor->resus = true;
        $donor->group = 1;
        $donor->save();
        $resultArr = $helper->view($user->id);    
        $this->tester->assertTrue($resultArr['code'] == DonorHelper::CODE_DONOR_FOUND
                && $resultArr['id'] === $donor->id
                && $resultArr['surname'] === $donor->surname
                && $resultArr['name'] === $donor->name
                && $resultArr['patronymic'] === $donor->patronymic
                && $resultArr['resus'] === $donor->resus
                && $resultArr['group'] === $donor->group);
    }
}