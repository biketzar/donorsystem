<?php
namespace app\modules\donor\models;
/**
 * @property integer $id 
 * @property integer $id_settings идентификатор настройки
 * @property integer $id_org идентификатор организации
 * @property string $val значение
 * @property integer $id_donor тдентификатор донора
 * @property Donor $donor модель донора
 * @property \app\modules\org\models\Orgs $org модель организации
 */
class ExtensionSettings extends \yii\db\ActiveRecord
{
    /**
     * разрешена сдача компонентов крови
     */
    CONST ENABLE_BLOOD_COMPONENT_DONATION = 1;
    
    public static function tableName() {
        return 'Extension_settings_donor_org';
    }
    
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_org' => 'Организация',
            'id_donor' => 'Донор',
            'id_settings' => 'Настройка',
            'val' => 'Значение'
        ];
    }
    
    public function rules() {
        return [
            [['id_org', 'id_donor', 'id_settings'], 'required', 'on' => ['insert', 'update']],
            [['val'], 'string', 'max' => 255, 'on' => ['insert', 'update']],
            [['id_org', 'id_donor', 'id_settings', 'val'], 'integer', 'on' => ['insert', 'update']],
            [['id_org', 'id_donor', 'id_settings', 'val'], 'safe', 'on' => 'search']
        ];
    }
    
    public function getOrg()
    {
        return $this->hasOne(\app\modules\org\models\Orgs::className(), ['id' => 'id_org']);
    }
    
    public function getDonor()
    {
        return $this->hasOne(Donor::className(), ['id' => 'id_donor']);
    }
    
    public static function getSettingsList()
    {
        return [
            self::ENABLE_BLOOD_COMPONENT_DONATION => 'Разрешена сдача компонентов крови'
        ];
    }
    
    public function beforeSave($insert) {
        if(!$this->checkEnableSettings())
        {
            return false;
        }
        return parent::beforeSave($insert);
    }
    
    /**
     * может ли пользователь сохранить указанные настройки
     * @return boolean
     */
    public function checkEnableSettings()
    {
        if((int)$this->id_settings === (int)self::ENABLE_BLOOD_COMPONENT_DONATION
                && $this->org
                && (string)$this->org->enable_donation_of_blood_components === '1'
                )
        {
            return true;
        }
        return FALSE;
    }
    
    public static function getDefaultVal($id_settings)
    {
        if((int)$id_settings === self::ENABLE_BLOOD_COMPONENT_DONATION)
        {
            return '0';
        }
    }
    
    /**
     * получение настроек из массива натсроек
     * @param array $settings
     * @param integer $id_donor
     * @param integer $id_settings
     * @param integer $id_org
     * @return string
     */
    public static function getValue($settings, $id_donor, $id_settings, $id_org)
    {
        foreach($settings as $oneSettings)
        {
            if((int)$oneSettings['id_org'] === (int)$id_org
                    && (int)$oneSettings['id_donor'] === (int)$id_donor
                    && (int)$oneSettings['id_settings'] === (int)$id_settings)
            {
                return $oneSettings['val'];
            }
        }
        return self::getDefaultVal($id_settings);
    }
}