<?php
namespace app\modules\donor\models;
/**
 * модель для работы с данными донора
 * доступные сценарии: insert, update
 * @property int $id
 * @property int $group Группа крови
 * @property bool $rhesus Резус-фактор
 * @property string $name Имя
 * @property string $surname Фамилия
 * @property string $patronymic Отчество
 * @property string $dob дата рождения
 * @property \app\models\User $user модель с данными пользователя
 */
class Donor extends \yii\db\ActiveRecord
{
    /**
     * дополнительные настройки для донора, которые устанавливаются менеджерами
     * @var array 
     */
    public $extraSettings = [];
    
    public static function tableName() {
        return 'Donor';
    }
    
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'group' => 'Группа крови',
            'rhesus' => 'Резус-фактор',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'dob' => 'Дата рождения'
        ];
    }
    
    public function rules() {
        return [
            [['id', 'name', 'surname'], 'required', 'on' => ['insert', 'update']],
            [['patronymic'], 'string', 'on' => ['insert', 'update']],
            [['dob'], 'date', 'format' => 'php:d.m.Y', 'on' => ['insert', 'update']],
            [['rhesus'], 'boolean', 'on' => ['insert', 'update']],
            ['group', 'in', 'range' => [1, 2, 3, 4], 'on' => ['insert', 'update']],
            //[['patronymic', 'name', 'surname'], 'match', 'pattern' => '/^[a-zа-яёЁ]*$/iu'],
            [['id'], 'validateID'],
            [['id', 'name', 'surname', 'patronymic', 'rhesus', 'group'], 'safe', 'on' => 'search']
        ];
    }
    
    /**
     * валидация id - пользователь должен существовать
     * @param string $attribute
     * @param array $params
     */
    public function validateID($attribute, $params)
    {
        if (!\app\models\User::findIdentity($this->$attribute)) {
            $this->addError($attribute, 'Пользователь не найден');
        }
    }
    
    /**
     * связь с пользователем
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'id']);
    }
    
    /**
     * 
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert) {
        if(!\app\modules\rest\components\DonorHelper::canUpdate($this->id))
        {//если пользователь не может редактировать донора, то правка отклоняется
            return false;
        }
        $timestamp = strtotime($this->dob);
        $this->dob = $timestamp !== false ? date('Y-m-d', $timestamp) : NULL;
        return parent::beforeSave($insert);
    }
    
    /**
     * 
     * @return \yii\db\ActiveQuery
     */
    public static function find() {
        return \Yii::createObject(DonorQuery::className(), [get_called_class()]);
    }
    
    /**
     * 
     * @return array
     */
    public static function getResusList()
    {
        return [
            1 => 'Положительный',
            0 => 'Отрицательный'
        ];
    }

    /**
     *
     * @return array
     */
    public static function getShortResusList()
    {
        return [
            1 => '+',
            0 => '-'
        ];
    }
    
    /**
     * 
     * @return array
     */
    public static function getGroupList()
    {
        return [1 => 1, 2 => 2, 3 => 3, 4 => 4];
    }
    
    public function afterSave($insert, $changedAttributes) {
        if(!in_array($this->id, \Yii::$app->authManager->getUserIdsByRole('donor')))
        {//привязка пользователя к роли донора
            $role = \Yii::$app->authManager->getRole('donor');
            \Yii::$app->authManager->assign($role, $this->user->id);
        }
        return parent::afterSave($insert, $changedAttributes);
    }
    
    /**
     * сохранение дополнительных настроек для донора
     */
    public function saveExtraSettings()
    {
        if((\Yii::$app->user->isManager()
                || \Yii::$app->user->isAdmin()
                )
                && is_array($this->extraSettings))
        {
            $transaction = self::getDb()->beginTransaction();
            foreach($this->extraSettings as $extraSettings)
            {
                if(array_key_exists('id_settings', $extraSettings)
                        && array_key_exists('id_org', $extraSettings)
                        && array_key_exists('val', $extraSettings)
                        && \app\modules\rest\components\OrgHelper::canUpdate($extraSettings['id_org'])
                    )
                {
                    $model = ExtensionSettings::find()
                            ->where(['id_settings' => $extraSettings['id_settings'],
                                'id_org' => $extraSettings['id_org'],
                                'id_donor' => $this->id,
                                ])->limit(1)->one();
                    if(!$model)
                    {
                        $model = new ExtensionSettings(['scenario' => 'insert']);
                        $model->id_settings = (int)$extraSettings['id_settings'];
                        $model->id_donor = $this->id;
                        $model->id_org = (int)$extraSettings['id_org'];
                    }
                    $model->val = (string)$extraSettings['val'];
                    if(!$model->save())
                    {
                        $transaction->rollBack();
                        return false;
                    }
                    
                }
            }
            $transaction->commit();
            return true;
        }
        return false;
    }
    
    public function afterDelete() {
        //отвязка пользователя от роли донора
        $role = \Yii::$app->authManager->getRole('donor');
        \Yii::$app->authManager->revoke($role, $this->user->id);
        return parent::afterDelete();
    }
    
    /**
     * полное имя донора
     * @return string
     */
    public function getFullName()
    {
        return ($this->surname ? $this->surname.' ' : '')
                .($this->name ? $this->name.' ' : '')
                .($this->patronymic ? $this->patronymic : '');
    }
    
    public function beforeDelete() {
        if(\app\modules\donation\models\Donation::find()->where(['donor_id' => $this->id])->limit(1)->count() > 0)
        {
            return false;
        }
        return parent::beforeDelete();
    }
    
    public static function getDonorList()
    {
        $list = [];
        $rows = self::find()->orderBy('surname ASC')->asArray()->all();
        foreach($rows as $row)
        {
            $list[$row['id']] = ($row['surname'] ? $row['surname'].' ' : '')
                .($row['name'] ? $row['name'].' ' : '')
                .($row['patronymic'] ? $row['patronymic'] : '');
        }
        return $list;
    }
    
    public function afterFind() {
        parent::afterFind();
        $timestamp = strtotime($this->dob);
        $this->dob = $timestamp !== false ? date('d.m.Y', $timestamp) : NULL;
    }
}

class DonorQuery extends \yii\db\ActiveQuery
{
}

