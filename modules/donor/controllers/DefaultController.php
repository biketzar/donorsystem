<?php
namespace app\modules\donor\controllers;
use yii\helpers\ArrayHelper;
use Yii;
use app\modules\rest\components\DonorHelper;
class DefaultController extends \yii\web\Controller
{
    //public $layout = '/sidebar';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //'only' => ['update', 'view', 'admin'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['update', 'view'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['list', 'settings'],
                        'roles' => ['admin', 'manager'],
                    ],
                    
                ],
            ],
        ];
    }
    
    public function actionUpdate()
    {
        $id = (int)\Yii::$app->request->get('id', Yii::$app->user->getId());
        $helper = new DonorHelper();
        $model = new \app\modules\donor\models\Donor(['scenario' => 'update']);
        $data = $helper->view($id);
        if(!$data['result'] && $data['code'] != DonorHelper::CODE_DONOR_NOT_FOUND)
        {
            throw new \yii\web\HttpException(404, $data['msg']);
        }
        $model->setAttributes($data, false);
        if(Yii::$app->request->isPost)
        {
            $postData = ArrayHelper::getValue(Yii::$app->request->post(), $model->formName());
            $model->load(Yii::$app->request->post());
            $updateResult = $helper->update($postData, $id);
            if($updateResult['result'])
            {
                \Yii::$app->session->setFlash('donor_update_success', $updateResult['msg']);
            }
            elseif($updateResult['code'] != DonorHelper::CODE_HAS_ERROR)
            {
                \Yii::$app->session->setFlash('donor_update_error', $updateResult['msg']);
            }
            if($updateResult['code'] == DonorHelper::CODE_HAS_ERROR)
            {
                $model->addErrors($updateResult['errors']);
            }
        }
        return $this->render('_form', [
            'model' => $model
            ]);
    }
    
    public function actionView()
    {
        $id = (int)\Yii::$app->request->get('id', Yii::$app->user->getId());
        $helper = new \app\modules\rest\components\DonorHelper();
        $model = new \app\modules\donor\models\Donor(['scenario' => 'update']);
        $data = $helper->view($id);
        if($data['code'] === 5 && !\Yii::$app->request->get('id'))
        {
            return $this->redirect(['/donor/default/update']);
        }
        if(!$data['result'])
        {
            throw new \yii\web\HttpException(404, $data['msg']);
        }
        $model->setAttributes($data, false);
        return $this->render('view', ['model' => $model]);
    }
    
    public function actionList()
    {
        $query = \app\modules\donor\models\Donor::find();
        if(!in_array('admin', array_keys(\Yii::$app->user->getIdentity()->getRoleList()))
                && !in_array('manager', array_keys(\Yii::$app->user->getIdentity()->getRoleList())))
        {
            $query->andWhere(['id' => \Yii::$app->user->getId()]);
        }
        $filterModel = new \app\modules\donor\models\Donor(['scenario' => 'search']);
        $filterModel->load(\Yii::$app->request->get());
        $query = DonorHelper::addFiter($filterModel, $query);
        $query = DonorHelper::addOrder($query, \Yii::$app->request->get('sort'));
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('list', [
            'dataProvider' => $dataProvider,
            'filterModel' => $filterModel
            ]);
    }
    
    public function actionSettings()
    {
        $id = (int)\Yii::$app->request->get('id', Yii::$app->user->getId());
        $helper = new \app\modules\rest\components\DonorHelper();
        $model = new \app\modules\donor\models\Donor(['scenario' => 'update']);
        $data = $helper->view($id);
        if(!$data['result'])
        {
            throw new \yii\web\HttpException(404, $data['msg']);
        }
        $model->setAttributes($data, false);
        if(\Yii::$app->request->isPost)
        {
            $saveResult = $helper->saveSettings($model->id, \Yii::$app->request->post('settings'));
            if($saveResult['result'] === true)
            {
                \Yii::$app->session->setFlash('donor_settings_update_success', $saveResult['msg']);
            }
            else
            {
                \Yii::$app->session->setFlash('donor_update_success', $saveResult['msg']);
            }
        }
        $settings = [];
        $settingsInfo = $helper->getSettings($id);
        if($settingsInfo['result'] === true)
        {
            $settings = $settingsInfo['settings'];
        }
        return $this->render('settings', [
            'model' => $model,
            'donorSettings' => $settings
            ]);
    }
}

