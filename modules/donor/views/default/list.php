<?php
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $filterModel app\modules\donor\models\Donor */
$this->title = 'Список доноров';
?>
<h1>Список доноров</h1>
<?php
echo yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'columns' => [
        'id', 'surname', 'name', 'patronymic', 'dob',
        [
            'attribute' => 'group',
            'filter' => app\modules\donor\models\Donor::getGroupList()
        ],
        [
            'attribute' => 'rhesus',
            'filter' => app\modules\donor\models\Donor::getResusList(),
            'value' => function($data){
                $arr = app\modules\donor\models\Donor::getResusList();
                $key = ($data['rhesus'] === true || $data['rhesus'] === "1" || $data['rhesus'] === 1) 
                        ? 1 
                        : (($data['rhesus'] === false || $data['rhesus'] === "0" || $data['rhesus'] === 0) ? 0 : -1);
                return array_key_exists($key, $arr) 
                        ? $arr[$key] : '-';
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '<div class="grid-action-buttons">{view} {update} {settings}</div>',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    if(!app\modules\rest\components\DonorHelper::canView($model->id))
                    {
                        return ' ';
                    }
                    return \yii\helpers\Html::a('<i class="fa fa-eye"></i> ', $url, [
                        'class' => 'btn btn-info btn-sm',
                        'title' => 'Просмотр'
                        ]);
                },
                'update' => function ($url, $model, $key) {
                    if(!app\modules\rest\components\DonorHelper::canUpdate($model->id))
                    {
                        return ' ';
                    }
                    return \yii\helpers\Html::a('<i class="fa fa-edit"></i> ', $url, [
                        'class' => 'btn btn-success btn-sm',
                        'title' => 'Редактировать'
                        ]);
                }, 
                'settings' => function ($url, $model, $key) {
                    if(!app\modules\rest\components\DonorHelper::canViewExtensionSetting($model->id))
                    {
                        return ' ';
                    }
                    return \yii\helpers\Html::a('<i class="fa fa-cog" style="color:white"></i> ', $url, [
                        'class' => 'btn btn-warning btn-sm',
                        'title' => 'Настройки'
                        ]);
                }, 
            ]
        ],
    ]
]);

