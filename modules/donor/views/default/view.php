<?php
/* @var $this yii\web\View */
/* @var $model app\modules\donor\models\Donor */
$this->title = 'Информация о доноре';
?>
<h1><?=$this->title?></h1>
<?php
echo \yii\widgets\DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id', 'surname', 'name', 'patronymic', 'dob', 'group',
        'rhesus' => [
            'attribute' => 'rhesus',
            'value' => ($model->rhesus === NULL ? '' : ($model->rhesus ? 'Положительный' : 'Отрицательный'))
        ]
]]);
?>
<div class="text-right">
    <?php
    if(app\modules\rest\components\DonorHelper::canUpdate(\Yii::$app->user->getId()))
    {
        ?>
        <a href="<?= yii\helpers\Url::to(['/donor/default/update', 'id' => $model->id]);?>" class="btn btn-md btn-success">
            <i class="fa fa-edit"></i> Редактировать
        </a>    
        <?php
    }
    ?>
</div>


