<?php
/* @var $this yii\web\View */
/* @var $model app\modules\donor\models\Donor */
use yii\helpers\Html;
$this->registerCssFile('@web2/jquery-ui/jquery-ui.min.css');
$this->registerCssFile('@web2/jquery-ui/jquery-ui.theme.min.css');
$this->registerJsFile('@web2/inputmask/jquery.inputmask.bundle.min.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web2/jquery-ui/jquery-ui.min.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web2/multidatespicker/datepicker-ru.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web2/jquery-ui/jquery-ui.min.js', ['depends' => 'yii\web\YiiAsset']);
$this->title = 'Обновление информации о доноре';
?>
<h1><?=$this->title;?></h1>
<?php
if(\Yii::$app->session->hasFlash('donor_update_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
      <?=\Yii::$app->session->getFlash('donor_update_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('donor_update_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
      <?=\Yii::$app->session->getFlash('donor_update_error')?>
    </div>
    <?php
}
?>
<?=Html::beginForm('', 'POST', ['id' => 'donor-form']);?>
    <?=Html::errorSummary($model);?>
    <div class="form-group">
      <?=Html::activeLabel($model, 'surname');?>
      <?=Html::activeTextInput($model, 'surname', ['class' => 'form-control', 'required' => true])?>
    </div>
    <div class="form-group">
      <?=Html::activeLabel($model, 'name');?>
      <?=Html::activeTextInput($model, 'name', ['class' => 'form-control', 'required' => true])?>
    </div>
    <div class="form-group">
      <?=Html::activeLabel($model, 'patronymic');?>
      <?=Html::activeTextInput($model, 'patronymic', ['class' => 'form-control'])?>
    </div>
    <div class="form-group">
      <?=Html::activeLabel($model, 'dob');?>
      <?=Html::activeTextInput($model, 'dob', [
          'class' => 'form-control', 
          'autocomplete' => 'off',
          'placeholder' => "Например, ".date('d.m.Y')])?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'group');?>
        <?=Html::activeDropDownList($model, 'group', app\modules\donor\models\Donor::getGroupList(), [
            'class' => 'form-control'
            ]);?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'rhesus');?>
        <?=Html::activeDropDownList($model, 'rhesus', app\modules\donor\models\Donor::getResusList(), [
            'class' => 'form-control'
            ]);?>
    </div>
    <div class="form-group text-right">
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
<?=Html::endForm();?>
<script type="text/javascript">
window.addEventListener('load', function(){
    
    var datePickerElem = $(<?=json_encode('#'.Html::getInputId($model, 'dob'));?>);
    var date = new Date(<?=date('Y')?>, <?=date('m')?>, <?=date('d')?>);
    datePickerElem.datepicker({
        language: 'ru',
        maxDate: '-18y',
        minDate: '-60y',
        yearRange: (date.getFullYear() - 60) + ':' + (date.getFullYear() - 18),
        changeMonth: true,
        changeYear: true
    });
    datePickerElem.inputmask("99.99.9999");
});
</script>


