<?php
/* @var $this yii\web\View */
/* @var $model app\modules\donor\models\Donor */
use yii\helpers\Html;
use app\modules\donor\models\ExtensionSettings;
$this->title = 'Дополнительные настройки';
?>
<h1>Дополнительные настройки: <?=$model->surname.' '.$model->name.' '.$model->patronymic?></h1>
<?php
if(\Yii::$app->session->hasFlash('donor_settings_update_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
      <?=\Yii::$app->session->getFlash('donor_settings_update_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('donor_settings_update_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
      <?=\Yii::$app->session->getFlash('donor_settings_update_error')?>
    </div>
    <?php
}
?>

<?php
$orgs = app\modules\org\models\Orgs::find()
        ->where(['IN', 'id', \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity())])
        ->all();
foreach($orgs as $org){ 
    if((string)$org->enable_donation_of_blood_components !== '1')
    {
        continue;
    }
    ?>
    <?=Html::beginForm('', 'POST', []);?>
    <div class="form-group">
        <h2><?=$org->name;?></h2>
    </div>
<?php
if((string)$org->enable_donation_of_blood_components === '1')
{
    $settingsId = ExtensionSettings::ENABLE_BLOOD_COMPONENT_DONATION;
    ?>
    <div class="form-group">
            <label>Разрешена сдача компонентов крови</label>
            <?=Html::hiddenInput("settings[{$settingsId}][id_donor]", $model->id);?>
            <?=Html::hiddenInput("settings[{$settingsId}][id_org]", $org->id);?>
            <?=Html::hiddenInput("settings[{$settingsId}][id_settings]", $settingsId);?>
            <?=Html::dropDownList("settings[{$settingsId}][val]", 
                    ExtensionSettings::getValue($donorSettings, $model->id, $settingsId, $org->id), 
                    ['0' => 'Не разрешена', '1' => 'Разрешена'],
                    ['class' => 'form-control']);?>
    </div>
<?php
}
?>
    <div class="form-group text-right">
        <button type="submit" class="btn btn-primary" name="save" value="1">Сохранить</button>
    </div>
    <?=Html::endForm();?>
<?php
}
?>