<?php
namespace app\modules\donor;

class Module extends \yii\base\Module
{
  public $controllerNamespace = 'app\modules\donor\controllers';
  
  public function init()
  {
      parent::init();
  }
}
