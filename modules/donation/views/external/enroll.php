<?php
/* @var $this yii\web\View */
/* @var $model \app\modules\donation\models\ExternalForm */
use yii\bootstrap\Html;
use yii\widgets\Pjax;
$this->title = 'Запись на сдачу';
$this->registerCssFile('@web2/jquery-ui/jquery-ui.min.css');
$this->registerCssFile('@web2/jquery-ui/jquery-ui.theme.min.css');
$this->registerJsFile('@web2/inputmask/jquery.inputmask.bundle.min.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web2/jquery-ui/jquery-ui.min.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web2/multidatespicker/datepicker-ru.js', ['depends' => 'yii\web\YiiAsset']);
?>
<?php Pjax::begin(['enablePushState' => false]); ?>
<?php
//var_dump($model->attributes);
?>
<div class="col-md-6 offset-md-3 col-12">
    <h2><?=$this->title;?></h2>
    <?=Html::beginForm('', 'POST', ['id' => 'enroll-form', 'data-pjax' => '']);?>
    <?=Html::errorSummary($model);?>
    <div class="form-group <?=$model->hasErrors('email') ? 'has-danger' : ''?>">
        <?=Html::activeLabel($model, 'email', ['class' => 'form-control-label']);?>
        <?=Html::activeTextInput($model, 'email', [
            'type' => 'email', 
            'required' => true, 
            'class' => 'form-control',
            ]);?>
        <small class="form-control-feedback"><?=$model->getFirstError('email')?></small>
    </div>
    <div class="form-group <?=$model->hasErrors('surname') ? 'has-danger' : ''?>">
        <?=Html::activeLabel($model, 'surname', ['class' => 'form-control-label']);?>
        <?=Html::activeTextInput($model, 'surname', [
            'class' => 'form-control',
            ]);?>
        <small class="form-control-feedback"><?=$model->getFirstError('surname')?></small>
    </div>
    <div class="form-group <?=$model->hasErrors('name') ? 'has-danger' : ''?>">
        <?=Html::activeLabel($model, 'name', ['class' => 'form-control-label']);?>
        <?=Html::activeTextInput($model, 'name', [
            'class' => 'form-control',
            ]);?>
        <small class="form-control-feedback"><?=$model->getFirstError('name')?></small>
    </div>
    <div class="form-group <?=$model->hasErrors('patronymic') ? 'has-danger' : ''?>">
        <?=Html::activeLabel($model, 'patronymic');?>
        <?=Html::activeTextInput($model, 'patronymic', [
            'class' => 'form-control',
            ]);?>
        <small class="form-control-feedback"><?=$model->getFirstError('patronymic')?></small>
    </div>
    <div class="form-group <?=$model->hasErrors('dob') ? 'has-danger' : ''?>">
      <?=Html::activeLabel($model, 'dob');?>
      <?=Html::activeTextInput($model, 'dob', [
          'class' => 'form-control', 
          'autocomplete' => 'off',
          'placeholder' => "Например, ".date('d.m.Y')])?>
        <small class="form-control-feedback"><?=$model->getFirstError('dob')?></small>
    </div>
    <div class="form-group <?=$model->hasErrors('group') ? 'has-danger' : ''?>">
        <?=Html::activeLabel($model, 'group');?>
        <?=Html::activeDropDownList($model, 'group', app\modules\donor\models\Donor::getGroupList(), [
            'class' => 'form-control'
            ]);?>
        <small class="form-control-feedback"><?=$model->getFirstError('group')?></small>
    </div>
    <div class="form-group <?=$model->hasErrors('rhesus') ? 'has-danger' : ''?>">
        <?=Html::activeLabel($model, 'rhesus');?>
        <?=Html::activeDropDownList($model, 'rhesus', app\modules\donor\models\Donor::getResusList(), [
            'class' => 'form-control'
            ]);?>
        <small class="form-control-feedback"><?=$model->getFirstError('rhesus')?></small>
    </div>
    <div class="form-group <?=$model->hasErrors('date') ? 'has-danger' : ''?>">
      <?=Html::activeLabel($model, 'date');?>
      <?=Html::activeDropDownList($model, 'date', $dateList, [
          'class' => 'form-control'
          ])?>
        <small class="form-control-feedback"><?=$model->getFirstError('date')?></small>
    </div>
    <div class="form-group <?=$model->hasErrors('time_id') ? 'has-danger' : ''?>">
      <?=Html::activeLabel($model, 'time_id');?>
      <?=Html::activeDropDownList($model, 'time_id', $timeList, [
          'class' => 'form-control'
          ])?>
        <small class="form-control-feedback"><?=$model->getFirstError('time_id')?></small>
    </div>
    <div class="form-group text-right">
        <input type="hidden" id="save_val" name="save" value="0"/>
        <button type="submit" class="btn btn-primary" name="save" value="1" onclick="$('#save_val').val('1');">Сохранить</button>
    </div>
    <?=Html::endForm();?>
</div>
<?php Pjax::end(); ?>
<script type="text/javascript">
    window.addEventListener('load', function(){
        function init()
        {
            $('#enroll-form').find('input, select').change(function(){
                $('#enroll-form').submit();
            });
            var datePickerElem = $(<?=json_encode('#'.Html::getInputId($model, 'dob'));?>);
            var date = new Date(<?=json_encode((int)date('Y'))?>, <?=json_encode((int)date('m') - 1)?>, <?=json_encode((int)date('d'))?>);
            datePickerElem.datepicker({
                language: 'ru',
                maxDate: '-18y',
                minDate: '-60y',
                yearRange: (date.getFullYear() - 60) + ':' + (date.getFullYear() - 18),
                changeMonth: true,
                changeYear: true
            });
            datePickerElem.inputmask("99.99.9999");
        }
        $(document).on('pjax:success', function() {
            init();
        });
        $(document).on('pjax:error', function() {});
        init();
    });
</script>
