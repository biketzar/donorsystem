<?php
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $pagination \yii\data\Pagination */
/* @var $filterModel app\modules\donation\models\Donation */
use app\modules\donation\models\Donation;
use app\modules\rest\components\DonationHelper;
use yii\helpers\Html;
use app\modules\donation\components\StatusHelper;
$this->title = "Записи на сдачу";
$this->registerCssFile('@web2/EasyAutocomplete/easy-autocomplete.min.css');
$this->registerJsFile('@web2/EasyAutocomplete/jquery.easy-autocomplete.min.js', ['depends' => 'yii\web\YiiAsset']);
?>
<h1><?=$this->title?></h1>
<div class="text-right">
    <?php
    if(\Yii::$app->user->isManager() || \Yii::$app->user->isAdmin())
    {
        ?>
        <a class="btn btn-danger" href="/donation/default/donorlist">
            Список доноров на день
        </a>
        <a href="<?= yii\helpers\Url::to(['/donation/default/add'])?>" class="btn btn-primary">
            <i class="fa fa-plus"></i> Добавить
        </a>
        <?php
    }
    ?>
</div>
<?php
if(\Yii::$app->session->hasFlash('donation_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
      <?=\Yii::$app->session->getFlash('donation_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('donation_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
      <?=\Yii::$app->session->getFlash('donation_error')?>
    </div>
    <?php
}
?>
<?php
if(\Yii::$app->session->hasFlash('enroll_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
      <?=\Yii::$app->session->getFlash('enroll_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('enroll_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
      <?=\Yii::$app->session->getFlash('enroll_error')?>
    </div>
    <?php
}
?>
<?php
$actions = [[
            'class' => 'yii\grid\ActionColumn',
            'template' => '<div class="grid-action-buttons">{denial} {cancel} {complete} {log} {notification}</div>',
            'buttons' => [
                'denial' => function ($url, $model, $key) {
                    if(!StatusHelper::canTransitions($model['event_id'], StatusHelper::STATUS_MANAGER_CANCELED)
                            || !DonationHelper::canDenial($model['id']))
                    {
                        return ' ';
                    }
                    return \yii\helpers\Html::a('<i class="fa fa-exclamation-triangle"></i> ', '#', [
                        'class' => 'btn btn-danger btn-sm',
                        'title' => 'Отмена сдачи по медицинским причинам',
                        'data-action' => 'denial',
                        'data-id' => $model['id'],
                        'data-action-name' => 'Отмена записи на сдачу'
                        ]);
                },
                'cancel' => function ($url, $model, $key) {
                    if(!StatusHelper::canTransitions($model['event_id'], StatusHelper::STATUS_DONOR_CANCELED)
                            || !DonationHelper::canCancel($model['id']))
                    {
                        return ' ';
                    }
                    $url = yii\helpers\Url::to(['/time/default/update', 'id' => $model['id']]);
                    return \yii\helpers\Html::a('<i class="fa fa-times-circle"></i> ', '#', [
                        'class' => 'btn btn-danger btn-sm',
                        'title' => 'Отмена записи',
                        'data-action' => 'cancel',
                        'data-id' => $model['id'],
                        'data-action-name' => 'Отмена записи на сдачу'
                        ]);
                },
                'complete' => function ($url, $model, $key) {
                    if(!StatusHelper::canTransitions($model['event_id'], StatusHelper::STATUS_DONATED)
                            || !DonationHelper::canComplete($model['id']))
                    {
                        return ' ';
                    }
                    return \yii\helpers\Html::a('<i class="fa fa-check"></i> ', '#', [
                        'class' => 'btn btn-success btn-sm',
                        'title' => 'Потверждение сдачи',
                        'data-action' => 'complete',
                        'data-id' => $model['id'],
                        'data-action-name' => 'Потверждение сдачи'
                        ]);
                },
                'log' =>  function($url, $model, $key){
                    if(!DonationHelper::canViewLog($model['id']))
                    {
                        return ' ';
                    }
                    return ' ';
                    $url = ['/donation/deafault/log', 'id' => $model['id']];
                    return \yii\helpers\Html::a('<i class="fa fa-list"></i> ', $url, [
                        'class' => 'btn btn-info btn-sm',
                        'title' => 'Лог',
                        ]);
                },
                'notification' =>  function($url, $model, $key){
                    if(!app\modules\rest\components\NotificationHelper::canSendNotification())
                    {
                        return '';
                    }
                    $url = ['#'];
                    return \yii\helpers\Html::a('<i class="fa fa-bullhorn"></i> ', $url, [
                        'class' => 'btn btn-info btn-sm send-notification-btn',
                        'title' => 'Уведомление',
                        'data-recepient' => $model['donor_id']
                        ]);
                }
            ]
        ]];
$dataColumns = [
        [
            'attribute' => 'org_id',
            'value' => function($data){ return $data['organisation'];},
            'filter' => DonationHelper::getOrgsForFilter()
        ],
        [
            'attribute' => 'time_id',
            'value' => function($data){ return date('d.m.Y H:i', $data['date_time']);},
            'filter' => trntv\yii\datetime\DateTimeWidget::widget([
                            'model' => $filterModel,
                            'attribute' => 'filterTime',
                            'phpDatetimeFormat' => 'dd.MM.yyyy',
                            'phpMomentMapping' => ['dd.MM.yyyy' => 'DD.MM.YYYY'],
                            'momentDatetimeFormat' => null,
                            'clientOptions' => [
                                'useCurrent' => false
                            ],
                            'clientEvents' => [
                                    'dp.change' => 'function(date, oldDate ){$(".trntv").val(date.toString()).change();}'
                           ]
                        ]). Html::hiddenInput('', '', ['class' => 'trntv'])
            
        ],
        [
            'attribute' => 'filterName',
            'format' => 'raw',
            'value' => function($data){
                if(\app\modules\rest\components\DonorHelper::canView($data['donor_id']))
                {
                    return Html::a($data['donor_name'], ['/donor/default/view', 'id' => $data['donor_id']]);
                }
                return $data['donor_name'];
                
            },
            //'filter' => Html::activeTextInput($filterModel, 'filterName')//DonationHelper::getDonorsForFilter()
        ],
        [
            'attribute' => 'event_id',
            'format' => 'raw',
            'value' => function($data){
                $statusList = StatusHelper::getStatusList();
                $value = array_key_exists($data['event_id'], $statusList) ? $statusList[$data['event_id']] : NULL;
                return Html::tag('SPAN', $value);
            },
            'filter' => StatusHelper::getStatusList()
        ]
];               
$columns = array_merge($dataColumns, $actions);
echo app\widgets\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'pagination' => $pagination,
    'filterUrl' => $filterUrlArr,
    'columns' => $columns
]);
?>
<div id="confirm-action-modal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <?=Html::beginForm('', 'POST');?>  
      <?=Html::hiddenInput('id', '');?>
      <div class="modal-header">
        <h4 class="modal-title">Потверждение операции</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
          <div class="form-group">
           <?=Html::label('Причина/комментарий:');?>
           <?=Html::textarea('reason', '', ['class' => 'form-control']);?>
          </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Потвердить</button> 
          <a href="#" class="btn btn-default" data-dismiss="modal">Отменить</a>
      </div>
      <?=Html::endForm();?>
    </div>

  </div>
</div>

<?php
$actionList = [
    'denial' => '/donation/default/denial',
    'cancel' => '/donation/default/cancel',
    'complete' => '/donation/default/complete'
];
?>
<script type="text/javascript">
    window.addEventListener('load', function(){
        var actionList = <?= yii\helpers\Json::encode($actionList)?>;
        var ActionHelper = function(){
            this.init = function(){
                registerActions();
            }
            
            function registerActions()
            {
                $(document).on('click', 'a[data-action=denial], a[data-action=cancel], a[data-action=complete]', function(e){
                    e.preventDefault();
                    showPopup($(this).attr('data-id'), actionList[$(this).attr('data-action')], $(this).attr('data-action-name'));
                    return false;
                });
            }
            
            function showPopup(id, action, actionName)
            {
                var modal = $('#confirm-action-modal');
                modal.find('input[name=id]').val(id);
                modal.find('form').prop('action', action);
                modal.find('h4.modal-title').text(actionName);
                modal.modal('show');
            }
        };
        
        var actionHelper = new ActionHelper();
        actionHelper.init();
    });
</script>
<?= app\modules\notifications\widgets\NotificationForm::widget();?>

