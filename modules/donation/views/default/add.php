<?php
/* @var $this yii\web\View */
$this->title = 'Добавление донации';
use yii\helpers\Html;
use app\modules\time\models\Time;
use yii\widgets\Pjax;
$this->registerCssFile('@web2/jquery-ui/jquery-ui.min.css');
$this->registerCssFile('@web2/jquery-ui/jquery-ui.theme.min.css');
$this->registerJsFile('@web2/jquery-ui/jquery-ui.min.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web2/multidatespicker/datepicker-ru.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web2/inputmask/jquery.inputmask.bundle.min.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerCssFile('@web2/select2/css/select2.min.css');
$this->registerjsFile('@web2/select2/js/select2.min.js', ['depends' => 'yii\web\YiiAsset']);
?>
<?php $pjax = Pjax::begin(['enablePushState' => false]); ?>
<?php
if(\Yii::$app->session->hasFlash('donation_add_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
      <?=\Yii::$app->session->getFlash('donation_add_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('donation_add_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
      <?=\Yii::$app->session->getFlash('donation_add_error')?>
    </div>
    <?php
}
?>
<?=Html::beginForm('', 'POST', ['id' => 'donation-add-form', 'data-pjax' => '']);?>
    <?=Html::errorSummary($model);?>
    <div class="form-group">
        <?=Html::activeLabel($model, 'org_id');?>
        <?=Html::activeDropDownList($model, 'org_id', Time::getOrgList(), [
            'class' => 'form-control',
            ]);?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'donor_id');?>
        <?=Html::activeDropDownList($model, 'donor_id', app\modules\donor\models\Donor::getDonorList(), [
            'class' => 'form-control select2',
            ]);?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'date');?>
        <?=Html::activeTextInput($model, 'date', [
            'class' => 'form-control', 
            'autocomplete' => 'off',
            'placeholder' => "Например, ".date('d.m.Y')])?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'time_id', ['label' => 'Время сдачи']);?>
        <?=Html::activeDropDownList($model, 'time_id', $timeList, [
            'class' => 'form-control',
            ]);?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'event_id');?>
        <?=Html::activeDropDownList($model, 'event_id', \app\modules\donation\components\StatusHelper::getStatusList(), [
            'class' => 'form-control',
            ]);?>
    </div>
    <div class="form-group text-right">
        <input type="hidden" id="save_val" name="save" value="0"/>
        <button type="submit" class="btn btn-primary" onclick="$('#save_val').val('1');">Сохранить</button>
    </div>
<?=Html::endForm();?>
<?php Pjax::end(); ?>
<script type="text/javascript">
window.addEventListener('load', function(){
    function init()
    {
        var allowDateList = <?= json_encode($allowDateList)?>; 
        $('#donation-add-form').find('input, select').change(function(){
            $('#donation-add-form').submit();
        });
        var datePickerElem = $(<?=json_encode('#'.Html::getInputId($model, 'date'));?>);
        var date = new Date(<?= json_encode((int)date('Y'))?>, <?=json_encode((int)date('m')-1)?>, <?=json_encode((int)date('d'))?>);
        datePickerElem.datepicker({
            language: 'ru',
            minDate: date,
            changeMonth: true,
            changeYear: true,
            beforeShowDay : function(date){
                var key = date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear();
                console.log(key, allowDateList[key]);
               return [allowDateList[key] === true];
            }
        });
        datePickerElem.inputmask("99.99.9999");
        $('select.select2').select2();
    }
    $(document).on('pjax:success', function() {
        init();
    });
    init();
    $('#donation-add-form').submit();
});
</script>