<?php
/* @var $this yii\web\View */
$this->title = 'Запись на сдачу';
use yii\helpers\Html;
?>
<h1><?=$this->title;?></h1>
<h6><?=$org ? $org->name : ''?></h6>
<?php
if(\Yii::$app->session->hasFlash('enroll_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
      <?=\Yii::$app->session->getFlash('enroll_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('enroll_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
      <?=\Yii::$app->session->getFlash('enroll_error')?>
    </div>
    <?php
}
?>
<?=Html::beginForm('', 'POST', ['id' => 'enroll-form']);?>
    <?=Html::errorSummary($model);?>
    <div class="form-group">
        <?=Html::label('Доступные для записи дата и время', Html::getInputId($model, 'time_id'));?>
        <?=Html::activeDropDownList($model, 'time_id', $timeList, ['class' => 'form-control', 'options' => $selectOptions]);?>
    </div>
    <div class="form-group text-right">
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
<?=Html::endForm();?>


