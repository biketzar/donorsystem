<?php
use \yii\helpers\Html;
use app\modules\rest\components\DonationHelper;
use yii\widgets\Pjax;
$this->title = 'Список доноров';
?>
<?php Pjax::begin(['enablePushState' => false]); ?>
<?=Html::beginForm('', 'POST', ['id' => 'donorlist-form', 'data-pjax' => '']);?>
<div class="row">
    <p class="h3 mb-4">
        Список доноров организации «<?= $filterModel->org ? $filterModel->org->name : ''?>» 
        <?=$filterModel->date ? ' на '.date('d.m.Y', strtotime($filterModel->date)) : ''?>
    </p>
</div>
<div class="row choose-time-block full-list-row">
    <div class="col-xs-4 col-lg-3">Выберите организацию: </div>
    <div class="col-xs-8 col-lg-7"><?= Html::activeDropDownList($filterModel,
            'org_id', 
            $orgList, 
            ['id'=>'organization-dropdown', 'class' => 'form-control'])?></div>
</div>
<div class="row choose-time-block full-list-row">
    <div class="col-xs-4 col-lg-3">Выберите дату: </div>
    <div class="col-xs-6 col-lg-7"><?= Html::activeDropDownList($filterModel,
            'date', 
            $timeList, 
            ['id' => 'time-dropdown', 'class' => 'form-control'])?></div>
    <div class="col-xs-2"><a href="#" class="btn btn-success" id="print-table">Печать таблицы</a></div>
</div>

<table class="table table-striped table-hover" id="donor-table">
<thead>
    <tr>
        <td>ФИО</td>
        <td>Группа</td>
        <td>Резус</td>
        <td>Время</td>
    </tr>
</thead>
<tbody>
    <?php 
    $resusList = \app\modules\donor\models\Donor::getShortResusList();
    foreach($donationList as $donation)
    {
        ?>
    <tr>
        <td><?=$donation->donor->surname." ".$donation->donor->name." ".$donation->donor->patronymic;?></td>
        <td><?=$donation->donor->group;?></td>
        <td><?= array_key_exists($donation->donor->rhesus ? 1 : 0, $resusList)
                ? ($resusList[$donation->donor->rhesus ? 1 : 0])
                : '';?></td>
        <td><?=date("H:i", strtotime($donation->time->date_time));?></td>
    </tr>
        <?php
    }
    ?>
</tbody>
<tfoot>

</tfoot>
</table>
<?=Html::endForm();?>
<?php Pjax::end(); ?>
<script type="text/javascript">

    window.addEventListener('load', function(){
        function init(){
            $('#print-table').click(function(e){
                e.preventDefault();
                window.print();
                return false;
            });
            $('#organization-dropdown').change(function(e){
                $('#donorlist-form').submit();
            });
            $('#time-dropdown').change(function(e){
                $('#donorlist-form').submit();
            });
        }
        init();
        $(document).on('pjax:success', function() {init()});
    });

</script>
<style>
    .show-print{
        display:none;
    }
    .time-info-row td{
        text-align: center;
    }
    .full-list-row{
        padding: 3px 0px;
    }
    @media print {
        @page {size:landscape;}
        html, body {
            height: 99%;    
        }
        .show-print{
            display:inline-block;
        }
        select, nav, .breadcrumbs, .padtop20,
        .copy, .choose-time-block, #yii-debug-toolbar, #yii-debug-toolbar-min,
        .select2-container, #select2-drop-mask, .select2-drop{
            display:none!important;
        }
        .page-content-wrapper .page-content{
            margin-left: 0px;
        }
        table#donor-table td {
            border: 1px solid black;
            color: black;
            padding: 1px 8px;
            line-height: 24x;
            font-size: 30px;
        }
        table#donor-table{
            border: 1px solid black;
        }
        table#donor-table tr:nth-child(odd) {
            background-color: #F2F2F2;
        }
        table#donor-table tr:nth-child(event) {
            background-color: #FFF;
        }
        div.select2-container {
            display: none;
        }
        .full-list-row{
            display: none;
        }
        table.event-people {
            border: 1px solid black;
            width: 100%;
            display: table;
            margin-bottom: 5px;
        }
        table.event-people td {
            border: 1px solid black;
            padding: 2px 10px;
            color: black;
        }
        p.h3{
            font-size: 20px;
        }
        .wrap {
            min-height: 90%;
            height: auto;
        }
    }
</style>