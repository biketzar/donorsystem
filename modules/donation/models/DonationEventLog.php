<?php
namespace app\modules\donation\models;
/**
 * @property int $id идентификатор лога
 * @property int $donation_id идентификатор записи
 * @property int $status_from предыдущий статус
 * @property int $status_to новый статус
 * @property string $reason причина изменения статус
 * @property string $user_id идентификатор пользователя, сменившего статус
 * @property string $date_time Время измения статуса
 */
class DonationEventLog extends \yii\db\ActiveRecord
{
    public static function tableName() {
        return 'Donation_event_log';
    }
    
    public function attributeLabels() {
        return [
            'donation_id' => 'Идентификатор записи',
            'status_from' => 'Предыдущий статус',
            'status_to' => 'Новый статус',
            'reason' => 'Причина',
            'user_id' => 'Пользователь, сменивший статус',
            'date_time' => 'Время измения статуса'
        ];
    }
    
    
    public function beforeSave($insert) {
        //нельзя модифицировать лог
        if(!$insert)
        {
            return false;
        }
        return parent::beforeSave($insert);
    }
    
    public function beforeDelete() {
        //нельзя удалить лог
        return false;
        return parent::beforeDelete();
    }
    
    /**
     * запись измения статуса в лог
     * @param int $statusFrom
     * @param int $statusTo
     * @param int $donationId
     * @param string $reason
     * @return boolean
     */
    public static function writeLog($statusFrom, $statusTo, $donationId, $reason = '')
    {
        $model = new DonationEventLog();
        $model->donation_id = (int)$donationId;
        $model->status_from = (int)$statusFrom;
        $model->status_to = (int)$statusTo;
        $model->reason = (string)$reason;
        $model->user_id = \Yii::$app->user->getId();
        //$model->date_time = date('Y-m-d H:i:s');
        return $model->save(false);
    }
}