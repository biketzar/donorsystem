<?php
namespace app\modules\donation\models;
/**
 * @property int $id идентификатор записи
 * @property int $org_id идентификатор организации
 * @property \app\modules\org\models\Orgs $org организация
 * @property int $donor_id идентификатор донора
 * @property int $time_id идентификатор времени
 * @property int $event_id идентификатор текущего статуса
 * @property \app\modules\time\models\Time $time
 * @property \app\modules\donor\models\Donor $donor
 */
class Donation extends \yii\db\ActiveRecord
{
    /**
     * поле для фильтрации по времени
     * @var string
     */
    public $filterTime;
    
    /**
     * причина смены статуса
     * @var string
     */
    public $reason = '';
    /**
     * дата сдачи
     * @var string 
     */
    public $date = '';
    /**
     * поле для фильтрации по ФИО
     * @return string
     */
    public $filterName = '';
    
    public function getOrg()
    {
        return $this->hasOne(\app\modules\org\models\Orgs::className(), ['id' => 'org_id']);
    }
    
    public static function tableName() {
        return 'Donation';
    }
    
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'org_id' => 'Организация',
            'donor_id' => 'Донор',
            'time_id' => 'Дата и время',
            'event_id' => 'Текущий статус',
            'filterName' => 'Донор',
            'date' => 'Дата сдачи'
        ];
    }
    
    public function rules() {
        return [
            [['donor_id', 'time_id', 'event_id', 'org_id'], 'integer', 'on' => ['insert', 'update']],
            [['time_id'], 'validateTimeID', 'on' => ['insert', 'update']],
            [['time_id', 'donor_id'], 'required', 'on' => ['insert', 'update']],
            [['event_id'], 'validateEventId', 'on' => ['update']],
            [['date'], 'safe'], 
            [['id', 'donor_id', 'time_id', 'event_id', 'org_id', 'filterTime', 'date', 'filterName'], 'safe', 'on' => ['search']]
            
        ];
    }
    
    /**
     * валидация id времени
     * @param string $attribute
     * @param array $params
     */
    public function validateTimeID($attribute, $params)
    {
        
       if(\Yii::$app->user->getId() != $this->donor->user->id
               && !\app\modules\time\components\AccessHelper::canEditTime($this->org, \Yii::$app->user->getIdentity()))
       {
           $this->addError($attribute, 'Доступ ко времени выбранной организации запрещён');
       }
       if(self::find()->where([
           'donor_id' => $this->donor_id, 
           'time_id' =>$this->time_id,
           'event_id' => \app\modules\donation\components\StatusHelper::STATUS_READY_TO_DONATE
           ])->count())
       {
           $this->addError($attribute, 'Донор уже записан на указанное время');
       }
    }
    
    /**
     * валидация id устанавливаемого события
     * @param string $attribute
     * @param array $params
     */
    public function validateEventId($attribute, $params)
    {
        if($this->oldAttributes['event_id'] != $this->event_id
                && !\app\modules\donation\components\StatusHelper::canTransitions($this->oldAttributes['event_id'], $this->event_id))
        {
            $statusList = \app\modules\donation\components\StatusHelper::getStatusList();
            $this->addError($attribute, "Запрещение изменения статуса с «{$statusList[$this->oldAttributes['event_id']]}» на «{$statusList[$this->event_id]}»");
        }
    }
    
    public function getTime()
    {
        return $this->hasOne(\app\modules\time\models\Time::className(), ['id' => 'time_id']);
    }
    
    public function getDonor()
    {
         return $this->hasOne(\app\modules\donor\models\Donor::className(), ['id' => 'donor_id']);
    }
    
    public function beforeSave($insert) {
        $this->org_id = $this->time->org_id;
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes) {
        //запись изменения статуса в лог
        if($insert)
        {
            DonationEventLog::writeLog(0, $this->event_id, $this->id, $this->reason);
        }
        elseif(array_key_exists('event_id', $changedAttributes))
        {
            DonationEventLog::writeLog($changedAttributes['event_id'], $this->event_id, $this->id, $this->reason);
        }
        if($this->event_id == \app\modules\donation\components\StatusHelper::STATUS_READY_TO_DONATE)
        {
            \app\modules\time\models\Time::updateAll(['published' => false], ['id' => $this->time_id]);
        }
        elseif($this->event_id != \app\modules\donation\components\StatusHelper::STATUS_DONATED)
        {
            \app\modules\time\models\Time::updateAll(['published' => true], ['id' => $this->time_id]);
        }
        parent::afterSave($insert, $changedAttributes);
    }
    
}