<?php
namespace app\modules\donation\models;

class ExternalForm extends \yii\base\Model
{
    public $email;
    public $name;
    public $surname;
    public $patronymic;
    public $dob;
    public $time_id;
    public $date;
    public $group;
    public $rhesus;
    
    public function attributeLabels() {
        return [
            'email' => 'Email',
            'name' => 'Имя',
            'surname' => 'Фамилия',
            'patronymic' => 'Отчество',
            'dob' => 'Дата рождения',
            'group' => 'Группа крови',
            'rhesus' => 'Резус-фактор',
            'date' => 'Дата сдачи',
            'time_id' => 'Время сдачи'
        ];
    }
    
    public function rules()
    {
        return [
            [['email'], 'email'],
            [['email'], 'required'],
            [['patronymic', 'name', 'surname'], 'match', 'pattern' => '/^[a-zа-яёЁ]*$/iu', 'message' => 'Поле должно содержать только латиницу и кириллицу'],
            [['surname'], 'required', 'when' => function($model) {
                return is_string($model->email) && strlen($model->email) > 0 && !$model->hasErrors('email');
            }],
            [['name'], 'required', 'when' => function($model) {
                return is_string($model->surname) && strlen($model->surname) > 0 && !$model->hasErrors('surname');
            }],
            [['patronymic'], 'string', 'when' => function($model) {
                return is_string($model->name) && strlen($model->name) > 0 && !$model->hasErrors('name');
            }],
            [['dob'], 'date', 'format' => 'php:d.m.Y', 'on' => ['insert', 'update']],
            [['dob'], 'required', 'when' => function($model) {
                return is_string($model->name) && strlen($model->name) > 0 && !$model->hasErrors('name');
            }],
            [['date'], 'date', 'format' => 'php:d.m.Y', 'on' => ['insert', 'update']],
            [['date'], 'required', 'when' => function($model) {
                return is_string($model->dob) && strlen($model->dob) > 0 && !$model->hasErrors('dob');
            }],   
            [['time_id'], 'required', 'when' => function($model) {
                return is_string($model->dob) && strlen($model->dob) > 0 && !$model->hasErrors('dob');
            }],
            [['group', 'rhesus'], 'safe'],
            ['group', 'in', 'range' => [1, 2, 3, 4]],
            [['rhesus'], 'boolean'],
            
        ];
    }
    
    /**
     * запись на сдачу
     * @param \app\modules\org\models\Orgs $org
     * @return boolean
     */
    public function save($org)
    {
        $mutex = new \yii\mutex\FileMutex();
        $mutexKey = 'external-form-registration';
        if(!$mutex->acquire($mutexKey, 15))
        {
            $this->addError('email', 'При сохранении произошла ошибка. Попробуйте отправить данные ещё раз');
            return false;
        }
        $user = $this->upsertUser();
        if(!$user || !\Yii::$app->user->login($user))
        {
            $mutex->release($mutexKey);
            return false;
        }
        $donor = $this->upsertDonor($user);
        
        if(!$donor)
        {
            $mutex->release($mutexKey);
            return false;
        }
        $donation = $this->upsertDonation($donor, $org);
        if(!$donation)
        {
            $mutex->release($mutexKey);
            return true;
        }
        $mutex->release($mutexKey);
        return false;
    }
    
    /**
     * добавление пользователя
     * @return boolean|\app\models\User
     */
    protected function upsertUser()
    {
        $userModel = \app\models\User::find()
                ->where(['email' => $this->email])
                ->limit(1)->one();
        if(!$userModel)
        {
            $userModel = new \app\models\User();
            $userModel->email = $this->email;
            $userModel->username = $this->email;
            $userModel->password_hash = uniqid().md5(microtime());
            $userModel->created_at = date('Y-m-d H:i:s');
            $userModel->confirmed_at = date('Y-m-d H:i:s');
            if(!$userModel->save())
            {
                foreach($userModel->getFirstErrors() as $error)
                {
                     $this->addError('email', $error);
                }
                return false;
            }
        }
        return $userModel;
    }
    
    /**
     * добавление или обновление донора
     * @param \app\models\User $userModel
     * @return boolean|\app\models\User
     */
    protected function upsertDonor($userModel)
    {
        $donor = \app\modules\donor\models\Donor::find()
                ->where(['id' => $userModel->id])
                ->limit(1)
                ->one();
        if(!$donor)
        {
            $donor = new \app\modules\donor\models\Donor(['scenario' => 'insert']);
            $donor->id = $userModel->id;
        }
        else
        {
            $donor->setScenario('update');
        }
        $donor->name = $this->name;
        $donor->surname = $this->surname;
        if($this->patronymic)
        {
            $donor->patronymic = $this->patronymic;
        }
        $donor->dob = $this->dob;
        $donor->group = $this->group;
        $donor->rhesus = $this->rhesus;
        if(!$donor->save())
        {
            foreach($donor->getFirstErrors() as $attribute => $error)
            {
                if(array_key_exists($attribute, $this->attributeLabels()))
                {
                    $this->addError($attribute, $error);
                }
                else
                {
                    $this->addError('name', $error);
                }
            }
            return false;
        }
        return $donor;
    }
    
    /**
     * добавление даты сдачи
     * @param \app\modules\donor\models\Donor $donor
     * @param \app\modules\org\models\Orgs $org
     * @return boolean|Donation
     */
    protected function upsertDonation($donor, $org)
    {
        $model = new \app\modules\donation\models\Donation(['scenario' => 'insert']);
        $model->donor_id = $donor->id;
        $model->org_id = $org->id;
        $model->time_id = $this->time_id;
        $helper = new \app\modules\rest\components\DonationHelper();
        $resultArr = $helper->enroll(['time_id' => $model->time_id]);
        if($resultArr['result'])
        {
            return Donation::find()->where(['id' => $resultArr['id']])->limit(1)->one();
        }
        elseif($resultArr['code'] != \app\modules\rest\components\DonationHelper::CODE_HAS_ERROR)
        {
            $this->addError('time_id', $resultArr['msg']);
            return FALSE;
        }
        if($resultArr['code'] == \app\modules\rest\components\DonationHelper::CODE_HAS_ERROR)
        {
            foreach($resultArr['errors'] as $attr => $errors)
            {
                foreach($errors as $error)
                {
                    $this->addError('time_id', $error);
                }
            }
            return FALSE;
        }
        return FALSE;
    }
}
