<?php
namespace app\modules\donation\components;
use yii\web\ForbiddenHttpException;

class AccessControl extends \yii\filters\AccessControl
{
    protected function denyAccess($user)
    {
        if ($user !== false && $user->getIsGuest()) {
            $user->loginRequired();
        } else {
            throw new \yii\web\HttpException(403, 'Доступ к разделу запрещён. Перейдите в раздел '
                    .\yii\helpers\Html::a('"Настройки"', ['/donor/default/update'])
                    .' и заполните обязательные поля');
        }
    }
}