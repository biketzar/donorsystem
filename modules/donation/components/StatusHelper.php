<?php
namespace app\modules\donation\components;

class StatusHelper
{
    /**
     * донор готов сдать кровь
     */
    CONST STATUS_READY_TO_DONATE = 1;
    /**
     * донор отказался от выбранного времени
     */
    CONST STATUS_DONOR_CANCELED = 2;
    /**
     * менеджер отклонил выбранное донором время
     */
    CONST STATUS_MANAGER_CANCELED = 3;
    /**
     * донор сдал кровь
     */
    CONST STATUS_DONATED = 4;
    
    /**
     * список возможных переходов
     * @return array
     */
    public static function getPossibleTransitins()
    {
        return [
            self::STATUS_READY_TO_DONATE => [
                //self::STATUS_READY_TO_DONATE,
                self::STATUS_DONATED,
                self::STATUS_DONOR_CANCELED,
                self::STATUS_MANAGER_CANCELED,
                ],
            self::STATUS_DONOR_CANCELED => [
                //self::STATUS_DONOR_CANCELED
                ],
            self::STATUS_MANAGER_CANCELED => [
                //self::STATUS_MANAGER_CANCELED
                ],
            self::STATUS_DONATED => [
                //self::STATUS_DONATED
                ]
        ];
    }
    
    /**
     * возможен ли переход из статуса $statusFrom в $statusTo
     * @param int $statusFrom
     * @param int $statusTo
     * @return boolean
     */
    public static function canTransitions($statusFrom, $statusTo)
    {
        $possibleTransitions = self::getPossibleTransitins();
        if(!array_key_exists($statusFrom, $possibleTransitions))
        {
            return false;
        }
        return in_array($statusTo, $possibleTransitions[$statusFrom]);
    }
    
    public static function getStatusList()
    {
        return [
            self::STATUS_READY_TO_DONATE => 'Записан',
            self::STATUS_DONOR_CANCELED => 'Отклонено донором',
            self::STATUS_MANAGER_CANCELED => 'Отклонено менеджером',
            self::STATUS_DONATED => 'Успешная сдача',
        ];
    }
}

