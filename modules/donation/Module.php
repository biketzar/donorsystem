<?php
namespace app\modules\donation;

class Module extends \yii\base\Module
{
  public $controllerNamespace = 'app\modules\donation\controllers';
  
  public function init()
  {
      parent::init();
  }
}
