<?php
namespace app\modules\donation\controllers;
use yii\helpers\ArrayHelper;
use Yii;
use app\modules\rest\components\DonationHelper;
use app\modules\donation\models\Donation;
use app\modules\time\models\Time;
use app\modules\org\models\Orgs;

class DefaultController extends \yii\web\Controller{

    //public $layout = '/sidebar';
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \app\modules\donation\components\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['enroll', 'time-search', 'index'],
                        'roles' => ['admin', 'manager', 'donor'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['denial', 'complete', 'add', 'donorlist'],
                        'roles' => ['admin', 'manager'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['cancel'],
                        'roles' => ['donor'],
                    ],
                ],
            ],
        ];
    }
    
    public function actionEnroll()
    {
        $model = new \app\modules\donation\models\Donation(['scenario' => 'insert']);
        if(\Yii::$app->request->isPost
                && !\Yii::$app->request->isPjax)
        {
            $model->load(\Yii::$app->request->post());
            $helper = new DonationHelper();
            $resultArr = $helper->enroll(['time_id' => $model->time_id]);
            if($resultArr['result'])
            {
                \Yii::$app->session->setFlash('enroll_success', $resultArr['msg']);
                $model = new \app\modules\donation\models\Donation(['scenario' => 'insert']);
                return $this->redirect('/donation/default/index');
            }
            elseif($resultArr['code'] != DonationHelper::CODE_HAS_ERROR)
            {
                \Yii::$app->session->setFlash('enroll_error', $resultArr['msg']);
            }
            if($resultArr['code'] == DonationHelper::CODE_HAS_ERROR)
            {
                $model->addErrors($resultArr['errors']);
            }
        }
        $timeListQuery = Time::find()
                ->where(['published' => true])
                ->andWhere(['NOT IN', 'id', \app\modules\donation\models\Donation::find()
                        ->select('time_id')->where([
                            'donor_id' => \Yii::$app->user->getId(), 
                            'event_id' => \app\modules\donation\components\StatusHelper::STATUS_READY_TO_DONATE
                            ])->column()])
                ->andWhere('date_time>:date_time', [":date_time" => date('Y-m-d H:i:s')])
                ->orderBy('date_time ASC');
        $donor = \app\modules\donor\models\Donor::find()
                ->where(['id' => \Yii::$app->user->getId()])->limit(1)->one();
        $allTimeQuery = clone $timeListQuery;
        if($donor)
        {
            $timeListQuery = \app\modules\rest\components\TimeHelper::addExtendedFilter($timeListQuery, $donor);
        }
        if($donor && $donor->group)
        {
            $timeListQuery->andWhere(['IN', 'group', [NULL, 0, $donor->group]]);
        }
        $orgId = \Yii::$app->request->post('org', \Yii::$app->request->get('org'));
        $org = NULL;
        if($orgId 
                && ($org = Orgs::find()->limit(1)->where(['id' => $orgId])->one()))
        {
            $timeListQuery->andWhere(['org_id' => $org->id]);
        }
        $processedTimeList = [];
        $timeTypeList = Time::getTypeList();
        $getType = function($type)use($timeTypeList){
            return array_key_exists($type, $timeTypeList) ? " ({$timeTypeList[$type]})" : '';
        };
        $enbleTimeList = $timeListQuery->select('id')->column();
        $selectOptions = [];
        if($org)
        {
            $allTimeList = $allTimeQuery->asArray()->all();
            foreach($allTimeList as $key => $timeRow)
            {
                $time = date('d.m.Y H:i', strtotime($timeRow['date_time'])).$getType($timeRow['type']);
                $processedTimeList[$timeRow['id']] = $time;
                if(!in_array($timeRow['id'], $enbleTimeList))
                {
                    $selectOptions[$timeRow['id']] = ['disabled' => true, 'title' => 'Запись недоступна'];
                }
            }
            
        }
        else
        {
            $allTimeList = $allTimeQuery->with('org')->all();
            foreach($allTimeList as $timeModel)
            {
                $time = date('d.m.Y H:i', strtotime($timeModel->date_time)).' - '.$timeModel->org->name.$getType($timeModel->type);
                $processedTimeList[$timeModel->id] = $time;
                if(!in_array($timeModel->id, $enbleTimeList))
                {
                    $selectOptions[$timeModel->id] = ['disabled' => true, 'title' => 'Запись недоступна'];
                }
            }
        }
        return $this->render('enroll', [
            'model' =>  $model,
            'timeList' => $processedTimeList,
            'org' => $org,
            'selectOptions' => $selectOptions
            ]);
    }
    
    public function actionDenial()
    {
        $helper = new DonationHelper();
        $resultArr = $helper->denial([
            'id' => \Yii::$app->request->post('id'),
            'reason' => \Yii::$app->request->post('reason')
            ]);
        if($resultArr['result'])
        {
            \Yii::$app->session->setFlash('donation_success', $resultArr['msg']);
        }
        else
        {
            \Yii::$app->session->setFlash('donation_error', $resultArr['msg']);
        }
        return $this->redirect('/donation/default/index');
    }
    
    public function actionCancel()
    {
        $helper = new DonationHelper();
        $resultArr = $helper->cancel([
            'id' => \Yii::$app->request->post('id'),
            'reason' => \Yii::$app->request->post('reason')
            ]);
        if($resultArr['result'])
        {
            \Yii::$app->session->setFlash('donation_success', $resultArr['msg']);
        }
        else
        {
            \Yii::$app->session->setFlash('donation_error', $resultArr['msg']);
        }
        return $this->redirect('/donation/default/index');
    }
    
    public function actionComplete()
    {
        $helper = new DonationHelper();
        $resultArr = $helper->complete([
            'id' => \Yii::$app->request->post('id'),
            'reason' => \Yii::$app->request->post('reason')
            ]);
        if($resultArr['result'])
        {
            \Yii::$app->session->setFlash('donation_success', $resultArr['msg']);
        }
        else
        {
            \Yii::$app->session->setFlash('donation_error', $resultArr['msg']);
        }
        return $this->redirect('/donation/default/index');
    }
    
    public function actionIndex()
    {
        $filterModel = new Donation(['scenario' => 'search']);
        $filterModel->load(Yii::$app->request->get());
        $orderField = \Yii::$app->request->get('sort');
        $helper = new DonationHelper();
        $pagination = new \yii\data\Pagination(['defaultPageSize' => 20]);
        $limit = \Yii::$app->request->getQueryParam($pagination->pageSizeParam, $pagination->defaultPageSize);
        $offset = $limit < 1 ? 0 : $limit * (\Yii::$app->request->getQueryParam($pagination->pageParam, 1) - 1);
        $data = $helper->getList($filterModel->attributes 
                + ['filterTime' => $filterModel->filterTime]
                + ['filterName' => $filterModel->filterName]
                , $orderField, $limit, $offset);
        $pagination->totalCount = $data['total'];
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $data['donations']
        ]);
        $filterUrlArr = \Yii::$app->request->getQueryParams();
        unset($filterUrlArr[$pagination->pageParam]);
        unset($filterUrlArr[$pagination->pageSizeParam]);
        $filterUrlArr[0] = '/donation/default/index';



        return $this->render('index', [
            'data' => $data,
            'dataProvider' => $dataProvider,
            'filterModel' => $filterModel,
            'pagination' => $pagination,
            'filterUrlArr' => $filterUrlArr
            ]);
    }
    
    public function actionTimeSearch()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $query = Time::find();
        $roleList = array_keys(\Yii::$app->user->getIdentity()->getRoleList());
        if(in_array('admin', $roleList))
        {
            
        }
        elseif(in_array('manager', $roleList))
        {
            $query->andWhere(['IN', 'org_id', \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity())]);
        }
        elseif(in_array('donor', $roleList))
        {
            $query->andWhere('IN', 'id', Donation::find()->select('time_id')->where(['donor_id' => \Yii::$app->user->getId()])->column());
        }
        else
        {
            throw new \yii\web\HttpException(404);
        }
        $dateTimeArr = explode(' ', \Yii::$app->request->get('datetime'));
        $dateArr = [];
        $timeArr = [];
        if($dateTimeArr[0])
        {
            $dateArr = explode('.', $dateTimeArr[0]);
        }
        if(isset($dateTimeArr[1]))
        {
            $timeArr = explode(':', $dateTimeArr[0]);
        }
        if(isset($dateArr[0]) && ($dateArr[0] > 0 && $dateArr[0] <= 31))
        {
            $query->andWhere('EXTRACT(DAY FROM date_time) = :day', [':day' => (int)$dateArr[0]]);
        }
        if(isset($dateArr[1]) && ($dateArr[1] > 0 && $dateArr[1] <= 12))
        {
            $query->andWhere('EXTRACT(month FROM date_time) = :month', [':month' => (int)$dateArr[1]]);
        }
        if(isset($dateArr[2]) && ($dateArr[2] > 2017 && $dateArr[2] <= 9999))
        {
            $query->andWhere('EXTRACT(DAY FROM date_time) = :year', [':year' => (int)$dateArr[2]]);
        }
        if(isset($timeArr[0]) && ($timeArr[0] > 0 && $timeArr[0] <= 23))
        {
            $query->andWhere('EXTRACT(hour FROM date_time) = :hour', [':hour' => (int)$timeArr[0]]);
        }
        if(isset($timeArr[0]) && ($timeArr[0] > 0 && $timeArr[0] <= 23))
        {
            $query->andWhere('EXTRACT(minute FROM date_time) = :minute', [':minute' => (int)$timeArr[1]]);
        }
        $rows = $query->limit(100)->asArray();
        //доделать
    }

    public function actionDonorlist()
    {
        $orgList = DonationHelper::getOrgsForFilter();
        $filterModel = new Donation(['scenario' => 'search']);
        $filterModel->load(Yii::$app->request->post());
        if(!$filterModel->org_id && $orgList)
        {
            $filterModel->org_id = (int)key($orgList);
        }
        $timeRows = Time::find()
                ->select(['date_time'])
                ->where(['IN', 'id', Donation::find()
                    ->distinct()
                    ->select(['time_id'])
                    ->where(['org_id' => $filterModel->org_id])
                    ->column()])
                ->andWhere('"date_time" > NOW() - INTERVAL \'1 DAY\'')
                ->orderBy('date_time ASC')
                ->groupBy('date_time')
                ->asArray()
                ->all();
        $timeList = ['' => 'Выберите дату'];
        foreach($timeRows as $timeRow)
        {
            $t = strtotime($timeRow['date_time']);
            $timeList[date('Y-m-d', $t)] = date('d.m.Y', $t);
        }
        $timeTableName = Time::getTableSchema()->fullName;
        $donationTableName = Donation::getTableSchema()->fullName;
        $timeStamp = (int) strtotime($filterModel->date);
        $donationList = Donation::find()->with(['donor', 'time'])
            ->where("\"{$donationTableName}\".\"org_id\" = :org_id", [
                ':org_id' => (int)$filterModel->org_id
            ])
            ->innerJoin($timeTableName, "\"{$timeTableName}\".\"id\"=\"{$donationTableName}\".\"time_id\"")
            ->andWhere("\"{$timeTableName}\".\"date_time\" >= :from AND \"{$timeTableName}\".\"date_time\" <= :to", [
                ':from' => date('Y-m-d 00:00:00', $timeStamp),
                ':to' => date('Y-m-d 23:59:59', $timeStamp)
            ])
            ->andWhere(['event_id' => \app\modules\donation\components\StatusHelper::STATUS_READY_TO_DONATE])
            ->all();
        return $this->render('donorlist', [
            'filterModel' => $filterModel,
            'orgList' =>  $orgList,
            'timeList' => $timeList,
            'donationList' => $donationList
        ]);
    }

    public function actionAdd()
    {
        $model = new Donation(['scenario' => 'insert']);
        if(\Yii::$app->request->isPost)
        {
            $model->load(\Yii::$app->request->post());
            if((string)\Yii::$app->request->post('save') === '1')
            {
                if($model->save())
                {
                    \Yii::$app->session->setFlash('donation_success', 'Донация успешно добавлена');
                    return $this->redirect('/donation/default/');
                }
                else
                {
                    \Yii::$app->session->setFlash('donation_add_error', 'При добавлении произошла ошибка');
                }
            }
            else
            {
                $model->validate();
            }
        }
        $timeListQuery = Time::find()->where(['published' => true]);
        $donor = \app\modules\donor\models\Donor::find()
                ->where(['id' => $model->donor_id])->limit(1)->one();
        if($donor)
        {
            $timeListQuery = \app\modules\rest\components\TimeHelper::addExtendedFilter($timeListQuery, $donor);
            $timeListQuery->andWhere(['NOT IN', 'id', \app\modules\donation\models\Donation::find()
                        ->select('time_id')->where([
                            'donor_id' => $donor->id,
                            'event_id' => \app\modules\donation\components\StatusHelper::STATUS_READY_TO_DONATE
                            ])->column()]);
        }
        $timeListQuery->andWhere('date_time>:date_time', [":date_time" => date('Y-m-d H:i:s')])
                ->orderBy('date_time ASC');
        
        if($donor && $donor->group)
        {
            $timeListQuery->andWhere(['IN', 'group', [NULL, 0, $donor->group]]);
        }
        if($model->org)
        {
            $timeListQuery->andWhere(['org_id' => $model->org->id]);
        }
        $allowDateQuery = clone $timeListQuery;
        $allowDateQuery->distinct()->select('date_time');
        $allowDateList = [];
        foreach($allowDateQuery->column() as $dateTime)
        {
            $time = strtotime($dateTime);
            $allowDateList[date('j.n.Y', $time)] = true;
        }
        if($model->date && strtotime($model->date) >= time())
        {
            $time = strtotime($model->date);
            $timeListQuery->andWhere("date_time>=:from AND date_time<=:to", [
                ':from' => date('Y-m-d 00:00:00', $time),
                ':to' => date('Y-m-d 23:59:59', $time),
            ]);
        }
        $timeList = [];
        $list = $timeListQuery->with('org')->all();
        $timeTypeList = \app\modules\time\models\Time::getTypeList();
        $getType = function($type)use($timeTypeList){
            return array_key_exists($type, $timeTypeList) ? " ({$timeTypeList[$type]})" : '';
        };
        foreach($list as $timeModel)
        {
            if($model->date && strtotime($model->date) >= time())
            {
                $timeList[$timeModel->id] = date('d.m.Y H:i', strtotime($timeModel->date_time)).$getType($timeModel->type);
            }
            else
            {
                $timeList[$timeModel->id] = date('d.m.Y H:i', strtotime($timeModel->date_time)).' - '.$timeModel->org->name;
            }
        }
        return $this->render('add', [
            'model' => $model,
            'timeList' => $timeList,
            'allowDateList' => $allowDateList
        ]);
    }
}

