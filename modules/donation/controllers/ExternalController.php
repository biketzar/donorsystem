<?php
namespace app\modules\donation\controllers;


class ExternalController extends \yii\web\Controller
{
    public $layout = '@app/modules/donation/views/external/layout';
    
    
    /**
     * все действия без авторизации
     * @param type $action
     * @return type
     */
    public function beforeAction($action) {
        \Yii::$app->user->logout();
        return parent::beforeAction($action);
    }
    public function afterAction($action, $result) {
        \Yii::$app->user->logout();
        return parent::afterAction($action, $result);
    }
    
    public function actionEnroll()
    {
        $key = \Yii::$app->request->get('key');
        if(!is_string($key) && strlen($key) === 0)
        {
            throw new \yii\web\HttpException(404);
        }
        $org = \app\modules\org\models\Orgs::find()
                ->where(['exterrnal_form_id' => \Yii::$app->request->get('key')])
                ->limit(1)->one();
        if(!$org)
        {
            throw new \yii\web\HttpException(404);
        }
        $model = new \app\modules\donation\models\ExternalForm();
        $model->load(\Yii::$app->request->post());
        if(\Yii::$app->request->isPost)
        {
            if((int)\Yii::$app->request->post('save') === 1
                    && $model->save($org))
            {
                //вывести сообщение об успешном сохранениее
            }
            else
            {
                $model->validate();
            }
        }      
        $timeListQuery = \app\modules\time\models\Time::find()
                ->where(['published' => true])
                ->andWhere(['NOT IN', 'id', \app\modules\donation\models\Donation::find()
                        ->select('time_id')->where([ 
                            'event_id' => \app\modules\donation\components\StatusHelper::STATUS_READY_TO_DONATE
                            ])->column()])
                ->andWhere('date_time>:date_time', [":date_time" => date('Y-m-d H:i:s')])
                ->orderBy('date_time ASC');
        $timeListQuery->andWhere(['org_id' => $org->id]);
        $showTime = false;
        if($model->date && ($time = strtotime($model->date)))
        {
            $showTime = true;
            $timeListQuery->andWhere("date_time>=:from AND date_time<=:to", [
                ':from' => date('Y-m-d 00:00:00', $time),
                ':to' => date('Y-m-d 23:59:59', $time),
            ]);
        }
        $rows = $timeListQuery->asArray()->all();
        $timeList = ['' => 'Выберите время'];
        $dateList = ['' => 'Выберите дату'];
        $timeTypeList = \app\modules\time\models\Time::getTypeList();
        $getType = function($type)use($timeTypeList){
            return array_key_exists($type, $timeTypeList) ? " ({$timeTypeList[$type]})" : '';
        };
        foreach($rows as $row)
        {
            if($showTime)
            {
                $timeList[$row['id']] = date('H:i', strtotime($row['date_time'])).$getType($row['type']);
            }
            $d = date('d.m.Y', strtotime($row['date_time']));
            $dateList[$d] = $d;
        }
        return $this->render('enroll', [
            'model' => $model,
            'dateList' => $dateList,
            'timeList' => $timeList
        ]);
    }
}
