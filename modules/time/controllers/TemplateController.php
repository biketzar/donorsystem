<?php
namespace app\modules\time\controllers;
use yii\helpers\ArrayHelper;
use Yii;
class TemplateController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //'only' => ['update', 'view', 'create', 'list'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['update', 'view', 'create', 'index', 'delete', 'fill'],
                        'roles' => ['admin', 'manager'],
                    ],
                    
                ],
            ],
        ];
    }
    
    public function actionCreate()
    {
        $sessionid = \Yii::$app->request->get('session', null);
        if((string)$sessionid === '')
        {
            return $this->redirect(['/time/template/create', 'session' => uniqid('s')]);
        }
        $model = new \app\modules\time\models\TimeTemplate(['scenario' => 'insert']);
        $itemModel = new \app\modules\time\models\TimeTemplateItem(['scenario' => 'insert']);
        $itemModel->template_id = -1;
        $sessionData = \Yii::$app->session->get($sessionid, []);
        if(ArrayHelper::getValue($sessionData, 'TimeTemplate'))
        {
            $model->setAttributes(ArrayHelper::getValue(\Yii::$app->session->get($sessionid, []), 'TimeTemplate'));
        }
        if(Yii::$app->request->isPost)
        {
            $model->load(Yii::$app->request->post());
            $sessionData['TimeTemplate'] = $model->getAttributes();
            if((string)Yii::$app->request->post('save-template-item') === '1')
            {
                $itemModel->load(Yii::$app->request->post());
                if($itemModel->validate())
                {
                    if(!array_key_exists('TimeTemplateItem', $sessionData))
                    {
                        $sessionData['TimeTemplateItem'] = array();
                    }
                    $sessionData['TimeTemplateItem'][] = $itemModel->getAttributes();
                    $itemModel = new \app\modules\time\models\TimeTemplateItem(['scenario' => 'insert']);
                    $itemModel->template_id = -1;
                }
            }
            if((int)Yii::$app->request->post('delete-template-item') >= 0)
            {
                unset($sessionData['TimeTemplateItem'][Yii::$app->request->post('delete-template-item')]);
            }
            if((string)\Yii::$app->request->post('save') === '0')
            {
                $model->validate();
            }
            else
            {
                $transaction = \app\modules\time\models\TimeTemplate::getDb()->beginTransaction();
                $hasError = false;
                if($model->save())
                {
                    foreach(ArrayHelper::getValue($sessionData, 'TimeTemplateItem', []) as $item)
                    {
                        $tempItemTemplate = new \app\modules\time\models\TimeTemplateItem(['scenario' => 'insert']);
                        $tempItemTemplate->setAttributes($item);
                        $tempItemTemplate->template_id = $model->id;
                        if($tempItemTemplate->save())
                        {
                            
                        }
                        else
                        {
                            $itemModel->addErrors($tempItemTemplate->getErrors());
                            $hasError = true;
                            break;
                        }
                    }
                }
                else 
                {
                    $hasError = true;
                }
                if($hasError === false)
                {
                    $transaction->commit();
                    \Yii::$app->session->setFlash('time_template_update_success', 'Шаблон успешно сохранён');
                    return $this->redirect(['/time/template/view', 'id' => $model->id]);
                }
                else
                {
                    $transaction->rollBack();
                    \Yii::$app->session->setFlash('time_template_update_error', 'При сохранении произошла ошибка');
                }
            }
        }
        $timeTemplateItems = ArrayHelper::getValue($sessionData, 'TimeTemplateItem', []);
        usort($timeTemplateItems, function($a, $b){
            $a = explode(':', $a['time']);
            $b = explode(':', $b['time']);
            if((int)$a[0] === (int)$b[0] && (int)$a[1] === (int)$b[1])
            {
                return 0;
            }
            if((int)$a[0] > (int)$b[0] || ((int)$a[0] === (int)$b[0] && (int)$a[1] > (int)$b[1]))
            {
                return 1;
            }
            return -1;
        });
        $sessionData['TimeTemplateItem'] = array_values($timeTemplateItems);
        \yii::$app->session->set($sessionid, $sessionData);
        return $this->render('create', [
            'itemModel' =>  $itemModel,
            'model' => $model,
            'timeTemplateItems' => $timeTemplateItems
            ]);
    }
    
    public function actionUpdate()
    {
        $model = \app\modules\time\models\TimeTemplate::find()
                ->where(['id' => \Yii::$app->request->get('id')])
                ->andWhere(['IN', 'org_id', \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity())])
                ->limit(1)
                ->one();
        if(!$model)
        {
            throw new \yii\web\HttpException(404);
        }
        $model->setScenario('update');
        $itemModel = new \app\modules\time\models\TimeTemplateItem(['scenario' => 'insert']);
        $itemModel->template_id = $model->id;
        if(Yii::$app->request->isPost)
        {
            if((string)Yii::$app->request->post('save-template-item') === '1')
            {
                $itemModel->load(Yii::$app->request->post());
                $itemModel->template_id = $model->id;
                if($itemModel->save())
                {                    
                    $itemModel = new \app\modules\time\models\TimeTemplateItem(['scenario' => 'insert']);
                    $itemModel->template_id = $model->id;
                }
            }
            if((int)Yii::$app->request->post('delete-template-item') >= 0)
            {
                \app\modules\time\models\TimeTemplateItem::deleteAll([
                    'template_id' => $model->id,
                    'id' => Yii::$app->request->post('delete-template-item')
                ]);
            }
            if((string)\Yii::$app->request->post('save') === '0')
            {
                $model->validate();
            }
            else
            {
                $transaction = \app\modules\time\models\TimeTemplate::getDb()->beginTransaction();
                $hasError = false;
                if(!$model->save())
                {
                    $hasError = true;
                }
                if($hasError === false)
                {
                    $transaction->commit();
                    \Yii::$app->session->setFlash('time_template_update_success', 'Шаблон успешно сохранён');
                    return $this->redirect(['/time/template/view', 'id' => $model->id]);
                }
                else
                {
                    $transaction->rollBack();
                    \Yii::$app->session->setFlash('time_template_update_error', 'При сохранении произошла ошибка');
                }
            }
        }
        $timeTemplateItems = ArrayHelper::index(\app\modules\time\models\TimeTemplateItem::find()
                ->where(['template_id' => $model->id])
                ->asArray()
                ->all(), 'id');
        uasort($timeTemplateItems, function($a, $b){
            $a = explode(':', $a['time']);
            $b = explode(':', $b['time']);
            if((int)$a[0] === (int)$b[0] && (int)$a[1] === (int)$b[1])
            {
                return 0;
            }
            if((int)$a[0] > (int)$b[0] || ((int)$a[0] === (int)$b[0] && (int)$a[1] > (int)$b[1]))
            {
                return 1;
            }
            return -1;
        });
        return $this->render('update', [
            'itemModel' =>  $itemModel,
            'model' => $model,
            'timeTemplateItems' => $timeTemplateItems
            ]);
    }
    
    public function actionIndex()
    {
        $query = \app\modules\time\models\TimeTemplate::find();
        $query->andWhere(['IN', 'org_id', \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity())]);
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query
        ]);
        return $this->render('list', [
            'dataProvider' => $dataProvider,
            'pagination' => $dataProvider->getPagination(),
        ]);
    }
    
    public function actionView()
    {
        $id = (int)\Yii::$app->request->get('id');
        $model = \app\modules\time\models\TimeTemplate::find()
                ->where(['id' => $id])
                ->andWhere(['IN', 'org_id', \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity())])
                ->limit(1)
                ->one();
        if(!$model)
        {
            throw new \yii\web\HttpException(404);
        }
        return $this->render('view', ['model' => $model]);
    }
    
    public function actionDelete()
    {
        $id = (int)\Yii::$app->request->get('id');
        $model = \app\modules\time\models\TimeTemplate::find()
                ->where(['id' => $id])
                ->andWhere(['IN', 'org_id', \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity())])
                ->limit(1)
                ->one();
        if(!$model)
        {
            throw new \yii\web\HttpException(404);
        }
        if($model->delete())
        {
            \Yii::$app->session->setFlash('time_template_update_success', 'Шаблон удалён');
        }
        else
        {
            \Yii::$app->session->setFlash('time_template_update_error', 'При удалении шаблона произошла ошибка');
        }
        return $this->redirect('/time/template');
    }
    
    public function actionFill()
    {
        $model = new \app\modules\time\models\TimeTemplateForm();
        $templates = ArrayHelper::map(\app\modules\time\models\TimeTemplate::find()
                ->andWhere(['IN', 'org_id', \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity())])
                ->asArray()
                ->all(), 'id', 'name');
        if(Yii::$app->request->isPost)
        {
           $model->load(Yii::$app->request->post());
           if($model->save())
           {
               \Yii::$app->session->setFlash('time_update_success', 'Шаблон успешно применён');
               return $this->redirect(['/time/default/']); 
           }
        }
        return $this->render('fill', [
            'templates' => $templates,
            'model' => $model
            ]);
    }
}