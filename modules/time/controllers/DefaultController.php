<?php
namespace app\modules\time\controllers;
use yii\helpers\ArrayHelper;
use Yii;
use app\modules\rest\components\TimeHelper;
class DefaultController extends \yii\web\Controller
{
    //public $layout = '/sidebar';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //'only' => ['update', 'view', 'create', 'list'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['update', 'view', 'create', 'index', 'delete'],
                        'roles' => ['admin', 'manager'],
                    ],
                    
                ],
            ],
        ];
    }
    
    public function actionCreate()
    {
        $model = new \app\modules\time\models\Time(['scenario' => 'insert']);
        $helper = new TimeHelper();
        if(Yii::$app->request->isPost)
        {
            $postData = ArrayHelper::getValue(Yii::$app->request->post(), $model->formName());
            $model->load(Yii::$app->request->post());
            $postData['date_time'] = strtotime($postData['date_time']);
            if(\Yii::$app->request->post('save') === '0')
            {
                $model = $helper->validate($postData, $model->getScenario());
            }
            else
            {
                $createResult = $helper->create($postData);
                if($createResult['result'])
                {
                    \Yii::$app->session->setFlash('time_update_success', $createResult['msg']);
                    return $this->redirect(['/time/default/view', 'id' => $createResult['id']]);
                }
                elseif($createResult['code'] != TimeHelper::CODE_HAS_ERROR)
                {
                    \Yii::$app->session->setFlash('time_update_error', $createResult['msg']);
                }
                if($createResult['code'] == TimeHelper::CODE_HAS_ERROR)
                {
                    $model->addErrors($createResult['errors']);
                }
            }
        }
        return $this->render('create', ['model' => $model]);
    }
    
    public function actionView()
    {
        $id = (int)\Yii::$app->request->get('id');
        $helper = new \app\modules\rest\components\TimeHelper();
        $model = new \app\modules\time\models\Time();
        $data = $helper->view($id);
        if(!$data['result'])
        {
            throw new \yii\web\HttpException(404, $data['msg']);
        }
        $model->setAttributes($data, false);
        return $this->render('view', ['model' => $model]);
    }
    
    public function actionUpdate()
    {
        $id = (int)\Yii::$app->request->get('id');
        if(!TimeHelper::canUpdate($id))
        {
            throw new \yii\web\HttpException(403, 'Доступ запрещён');
        }
        $helper = new \app\modules\rest\components\TimeHelper();
        $model = new \app\modules\time\models\Time(['scenario' => 'update']);
        $data = $helper->view($id);
        if(!$data['result'])
        {
            throw new \yii\web\HttpException(404, $data['msg']);
        }
        $data['date_time'] = date('Y-m-d H:i:s', $data['date_time']);
        $model->setAttributes($data, false);
        if(Yii::$app->request->isPost)
        {
            $postData = ArrayHelper::getValue(Yii::$app->request->post(), $model->formName());
            $model->load(Yii::$app->request->post());
            $postData['id'] = $id;
            $postData['date_time'] = strtotime($postData['date_time']);
            if(\Yii::$app->request->post('save') === '0')
            {
                $model = $helper->validate($postData, $model->getScenario());
            }
            else
            {
                $createResult = $helper->update($postData);
                if($createResult['result'])
                {
                    \Yii::$app->session->setFlash('time_update_success', $createResult['msg']);
                }
                elseif($createResult['code'] != TimeHelper::CODE_HAS_ERROR)
                {
                    \Yii::$app->session->setFlash('time_update_error', $createResult['msg']);
                }
                if($createResult['code'] == TimeHelper::CODE_HAS_ERROR)
                {
                    $model->addErrors($createResult['errors']);
                }
            }
        }
        return $this->render('update', ['model' => $model]);
    }
    
    public function actionIndex()
    {


        $filterModel = new \app\modules\time\models\Time(['scenario' => 'search']);
        $filterModel->load(Yii::$app->request->get());
        if(strtotime($filterModel->date_time))
        {
            $filterModel->date_time = strtotime($filterModel->date_time);
        }
        else
        {
            $filterModel->date_time = NULL;
        }
        $orderField = \Yii::$app->request->get('sort');
        $helper = new \app\modules\rest\components\TimeHelper();
        $pagination = new \yii\data\Pagination(['defaultPageSize' => 20]);
        $limit = \Yii::$app->request->getQueryParam($pagination->pageSizeParam, $pagination->defaultPageSize);
        $offset = $limit < 1 ? 0 : $limit * (\Yii::$app->request->getQueryParam($pagination->pageParam, 1) - 1);
        $data = $helper->getList($filterModel->attributes + ($filterModel->filterTime ? ['filterTime' => $filterModel->filterTime] : []), $orderField, $limit, $offset);
        $pagination->totalCount = $data['total'];
        $dataProvider = new \yii\data\ArrayDataProvider([
            'allModels' => $data['time']
        ]);
        $filterUrlArr = \Yii::$app->request->getQueryParams();
        unset($filterUrlArr[$pagination->pageParam]);
        unset($filterUrlArr[$pagination->pageSizeParam]);
        $filterUrlArr[0] = '/time/default/index';
        return $this->render('list', [
            'data' => $data,
            'dataProvider' => $dataProvider,
            'filterModel' => $filterModel,
            'pagination' => $pagination,
            'filterUrlArr' => $filterUrlArr
            ]);
    }
    
    public function actionDelete()
    {
        $id = (int)\Yii::$app->request->get('id');
        if(!TimeHelper::canDelete($id))
        {
            throw new \yii\web\HttpException(403, 'Доступ запрещён');
        }
        $helper = new \app\modules\rest\components\TimeHelper();
        $data = $helper->delete($id);
        if(\Yii::$app->request->isAjax)
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $data['msg'] = "ID {$id}: ".$data['msg'];
            return $data;
        }
        if($data['result'])
        {
            \Yii::$app->session->setFlash('time_update_success', $data['msg']);
        }
        else
        {
            \Yii::$app->session->setFlash('time_update_error', $data['msg']);
        }
        return $this->redirect('/time/default/index');
    }
}

