<?php
namespace app\modules\time\models;

class TimeTemplateForm extends \yii\base\Model
{
    public $times;
    public $template_id;
    
    public function rules() {
        return [
            [['times', 'template_id'], 'required']
        ];
    }
    public function attributeLabels() {
        return [
            'times' => 'Время',
            'template_id' => 'Шаблон'
        ];
    }
    
    public function save()
    {
        $template = TimeTemplate::find()
                ->where(['id' => $this->template_id])
                ->andWhere(['IN', 'org_id', \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity())])
                ->limit(1)
                ->one();
        if(!$template)
        {
            $this->addError('template_id', 'Шаблон не найден или недоступен');
            return false;
        }
        $dates = explode(', ', $this->times);
        foreach($dates as $date)
        {
            foreach(TimeTemplateItem::find()->where(['template_id' => $template->id])->all() as $templateItem)
            {
                $fullTime = $date.' '.$templateItem->time;
                if(strtotime($fullTime) > 0)
                {
                    $timeModel = new Time(['scenario' => 'insert']);
                    $timeModel->published = 1;
                    $timeModel->date_time = date('Y-m-d H:i:s', strtotime($fullTime));
                    $timeModel->org_id = (int)$template->org_id;
                    $timeModel->type = $templateItem->type;
                    $timeModel->group = $templateItem->group;
                    $timeModel->reg_stop_hours = $templateItem->reg_stop_hours;
                    if($timeModel->save())
                    {
                        $this->addError('times', 'Не удалось сохранить '.$timeModel->date_time);
                    }
                }
                else
                {
                    $this->addError('times', 'Не удалось сохранить '.$fullTime);
                }
            }
        }
        return $this->hasErrors();
    }
    
}