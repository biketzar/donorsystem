<?php
namespace app\modules\time\models;
use app\modules\time\components\AccessHelper;

/**
 * @property int $id идентифкатор времени
 * @property int $org_id идентификатор организации
 * @property string $date_time время в формате Y-m-d H:i:s
 * @property int|boolean $published опубликовано
 * @property int $type идентификатор типа времени
 * @property int $group группа крови
 * @property \app\modules\org\models\Orgs $org
 */
class Time extends \yii\db\ActiveRecord
{
    public $filterTime;
    
    /**
     * для фильтра, только время старше текущего
     * @var int
     */
    public $onlyActual = 1;
    
    public static function tableName() {
        return 'Time';
    }
    
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'org_id' => 'Организация',
            'date_time' => 'Дата и время',
            'published' => 'Опубликовано (доступна регистрация)',
            'type' => 'Тип времени',
            'group' => 'Группа крови',
            'reg_stop_hours' => 'Закрыть регистрацию за ... (в часах)'
        ];
    }
    
    public function rules() {
        return [
            [['org_id', 'date_time', 'type'], 'required', 'on' => ['insert', 'update']],
            [['published'], 'boolean', 'on' => ['insert', 'update']],
            [['org_id',], 'integer', 'on' => ['insert', 'update']],
            [['reg_stop_hours',], 'integer', 'min' => 0, 'on' => ['insert', 'update']],
            [['reg_stop_hours',], 'default', 'value' => 0, 'on' => ['insert', 'update']],
            ['group', 'in', 'range' => [0, 1, 2, 3, 4], 'on' => ['insert', 'update']],
            [['org_id'], 'validateOrgID', 'on' => ['insert', 'update']],
            [['date_time'], 'date', 'format' => 'php:Y-m-d H:i:s'],
            [['org_id', 'date_time', 'published', 'type', 'id', 'group', 'filterTime', 'onlyActual'], 'safe', 'on' => 'search']
        ];
    }
    
    public static function getGroupList()
    {
        return [0 => 'Любая', 1 => 1, 2 => 2, 3 => 3, 4 => 4];
    }
    
    /**
     * валидация id организации
     * @param string $attribute
     * @param array $params
     */
    public function validateOrgID($attribute, $params)
    {
       if(!AccessHelper::canEditTime($this->org, \Yii::$app->user->getIdentity()))
       {
           $this->addError($attribute, 'Доступ к выбранной организации запрещён');
       }
    }
    
    /**
     * 
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert) {
        if(!AccessHelper::canEditTime($this->org, \Yii::$app->user->getIdentity()))
        {//если пользователь не может редактировать время, то правка отклоняется
            return false;
        }
        return parent::beforeSave($insert);
    }
    
    /**
     * тип времени
     * @return array
     */
    public static function getTypeList()
    {
        return [
            1 => 'Цельная кровь',
            2 => 'Компоненты',
            3 => 'Компоненты: плазмаферез',
            4 => 'Компоненты: эритроцитаферез',
            5 => 'Компоненты: тромбоцитаферез'
        ];
    }
    
    public static function getTypeListForOrg($orgId)
    {
        $org = \app\modules\org\models\Orgs::find()->where(['id' => (int)$orgId])->limit(1)->one();
        if($org && $org->enable_donation_of_blood_components == 1)
        {
            return [
                1 => 'Цельная кровь',
                2 => 'Компоненты',
                3 => 'Компоненты: плазмаферез',
                4 => 'Компоненты: эритроцитаферез',
                5 => 'Компоненты: тромбоцитаферез'
            ];
        }
        return [
            1 => 'Цельная кровь',
        ];
    }
    
    /**
     * список доступных пользователю организаций
     * @return array
     */
    public static function getOrgList()
    {
        static $list;
        if(is_array($list))
        {
            return $list;
        }
        $allowOrgs = AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity());
        return $list = \yii\helpers\ArrayHelper::map(\app\modules\org\models\Orgs::find()
                ->select(['id', 'name'])
                ->where(['IN', 'id', $allowOrgs])
                ->asArray()->all(), 'id', 'name');
    }
    
    public function getOrg()
    {
        return $this->hasOne(\app\modules\org\models\Orgs::className(), ['id' => 'org_id']);
    }
    
    public function beforeDelete() {
        if(\app\modules\donation\models\Donation::find()->where(['time_id' => $this->id])->limit(1)->count() > 0)
        {
            return false;
        }
        return parent::beforeDelete();
    }
}