<?php
namespace app\modules\time\models;
/**
 * @property int $template_id идентификатор шаблона
 * @property string $name название шаблона
 * @property \app\modules\org\models\Orgs $org модель организхации
 * @property string $time время в формате H:i:s
 * @property int $type идентификатор типа времени
 * @property int $group группа крови
 */
class TimeTemplateItem extends \yii\db\ActiveRecord
{
    public static function tableName() {
        return 'TimeTemplateItem';
    }
    
    public function rules()
    {
        return [
            ['group', 'in', 'range' => [0, 1, 2, 3, 4], 'on' => ['insert', 'update']],
            [['time', 'type'], 'required', 'on' => ['insert', 'update']],
            [['reg_stop_hours',], 'integer', 'min' => 0, 'on' => ['insert', 'update']],
            [['reg_stop_hours',], 'default', 'value' => 0, 'on' => ['insert', 'update']],
            [['time'], 'date', 'format' => 'php:H:i:s'],
        ];
    }
    
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'time' => 'Время',
            'type' => 'Тип времени',
            'group' => 'Группа крови',
            'reg_stop_hours' => 'Закрыть регистрацию за ... (в часах)'
        ];
    }
}

