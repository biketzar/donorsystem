<?php
namespace app\modules\time\models;
/**
 * @property int $org_id идентификатор организации
 * @property string $name название шаблона
 * @property \app\modules\org\models\Orgs $org модель организхации
 */
class TimeTemplate extends \yii\db\ActiveRecord
{
    public static function tableName() {
        return 'TimeTemplate';
    }
    
    public function attributeLabels() {
        return [
            'name' => 'Название',
            'org_id' => 'Организация'
        ];
    }
    
    public function getOrg()
    {
        return $this->hasOne(\app\modules\org\models\Orgs::className(), ['id' => 'org_id']);
    }
    
    public function rules() {
        return [
            [['org_id', 'name'], 'required', 'on' => ['insert', 'update']],
            [['org_id'], 'validateOrg', 'on' => ['insert', 'update']]
        ];
    }
    
    public function validateOrg($attrubute, $params)
    {
        $identity = \Yii::$app->user->getIdentity();
        if(!\Yii::$app->user->isGuest 
                && in_array('admin', array_keys($identity->getRoleList())))
        {
            return true;
        }
        if(!\Yii::$app->user->isGuest 
                && (in_array('manager', array_keys(\Yii::$app->user->getIdentity()->getRoleList()))))
        {
            if(!in_array($this->org_id, \app\modules\time\components\AccessHelper::allowOrgsByUser($identity)))
            {
                $this->addError($attrubute, 'Вы не можете добавить шаблон для указанной организации');
            }  
            return true;
        }
        $this->addError($attrubute, 'Вы не можете добавить шаблон для указанной организации');
    }
}