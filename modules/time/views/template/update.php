<?php
/* @var $this yii\web\View */
/* @var $model app\modules\donor\models\Time */
use yii\helpers\Html;
$this->title = 'Редактирование шаблона';
?>
<h1><?=$this->title;?></h1>
<?=$this->render('_form', ['model' => $model, 'itemModel' =>  $itemModel,'timeTemplateItems' => $timeTemplateItems]);?>
