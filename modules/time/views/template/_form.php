<?php
/* @var $this yii\web\View */
/* @var $model app\modules\time\models\Time */
use yii\helpers\Html;
use app\modules\time\models\Time;
use yii\widgets\Pjax;
$this->registerCssFile('@web2/datetimepicker/jquery.datetimepicker.css');
$this->registerJsFile('@web2/datetimepicker/jquery.datetimepicker.full.min.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web2/datetimepicker/date.format.js', ['depends' => 'yii\web\YiiAsset']);
?>
<?php Pjax::begin(['enablePushState' => false]); ?>
<?php
if(\Yii::$app->session->hasFlash('time_template_update_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
      <?=\Yii::$app->session->getFlash('time_template_update_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('time_template_update_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
      <?=\Yii::$app->session->getFlash('time_template_update_error')?>
    </div>
    <?php
}
?>
<?=Html::beginForm('', 'POST', ['id' => 'time-template-form', 'data-pjax' => '']);?>
<?=Html::errorSummary($model);?>
<div class="form-group">
    <?=Html::activeLabel($model, 'name');?>
    <?=Html::activeTextInput($model, 'name', ['class' => 'form-control', 'onchange' => "$('#time-template-form').submit();"]);?>
</div>
<?php
if($model->getScenario() === 'insert')
{
?>
<div class="form-group">
    <?=Html::activeLabel($model, 'org_id');?>
    <?=Html::activeDropDownList($model, 'org_id', ['' => '---'] + Time::getOrgList(), [
        'class' => 'form-control',
        'onchange' => "$('#time-template-form').submit();"
        ]);?>
</div>
<?php
}
?>
<?php
if($timeTemplateItems)
{
    ?>
<div class="form-group">
    <h3>Время</h3>
    <div>
        <table class="table table-striped table-bordered">
            <thead>
                <th>Время</th>
                <th>Тип времени</th>
                <th>Группа крови</th>
                <?php
                if($model->org && $model->org->enable_stop_registration)
                {
                ?>
                    <th>Закрыть регистрацию за ... (в часах)</th>
                <?php
                }
                ?>
                <th></th>
            </thead>
            <tbody>
                <?php
                $typeList = Time::getTypeList();
                $groupList = Time::getGroupList();
                foreach($timeTemplateItems as $id => $timeTemplateItem)
                {
                    
                    $groupId = (int)$timeTemplateItem['group'];
                    ?>
                    <tr>
                        <td><?=$timeTemplateItem['time'];?></td>
                        <td><?=array_key_exists($timeTemplateItem['type'], $typeList) ? $typeList[$timeTemplateItem['type']] : NULL;?></td>
                        <td><?=array_key_exists($groupId, $groupList) ? $groupList[$groupId] : '';?></td>
                        <?php
                        if($model->org && $model->org->enable_stop_registration)
                        {
                        ?>
                            <td><?=$timeTemplateItem['reg_stop_hours'];?></td>
                        <?php
                        }
                        ?>
                        <td>
                            <div>
                                <input type="hidden" name="template-item" value="<?=$id?>" />
                                <a href="#" 
                                   onclick="if(confirm('Потвердите удаление времени <?= $timeTemplateItem['time']?>')){$(this).parent().find('input').attr('name', 'delete-template-item');$('#time-template-form').submit();};return false;">Удалить</a>
                            </div>
                        </td>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<?php
}
?>
<div class="form-group">
    <h3>Добавить время</h3>
    <?=Html::errorSummary($itemModel);?>
</div>
<?php
$widthArr = $model->org && $model->org->enable_stop_registration ? [2,2,2,4,2] : [3,3,3,3];
?>
<div class="form-group row">
    <div class="col-md-<?= array_shift($widthArr);?>">
        <?=Html::activeLabel($itemModel, 'type');?>
        <?=Html::activeDropDownList($itemModel, 'type', Time::getTypeListForOrg($model->org_id), [
            'class' => 'form-control'
            ]);?>
    </div>
    <div class="col-md-<?= array_shift($widthArr);?>">
        <?=Html::activeLabel($itemModel, 'group');?>
        <?=Html::activeDropDownList($itemModel, 'group', Time::getGroupList(), [
            'class' => 'form-control'
            ]);?>
    </div>
    <div class="col-md-<?= array_shift($widthArr);?>">
        <?=Html::activeLabel($itemModel, 'time');?>
        <?=Html::activeTextInput($itemModel, 'time', ['class' => 'form-control']);?>
    </div>
    <?php
    if($model->org && $model->org->enable_stop_registration)
    {
    ?>
    <div class="col-md-<?= array_shift($widthArr);?>">
        <?=Html::activeLabel($itemModel, 'reg_stop_hours');?>
        <?=Html::activeTextInput($itemModel, 'reg_stop_hours', ['class' => 'form-control']);?>
    </div>
    <?php
    }
    ?>
    <div class="col-md-<?= array_shift($widthArr);?>">
        <label style="display: block;">&nbsp;</label>
        <input type="hidden" id="save_template_item" name="save-template-item" value="0"/>
        <button type="submit" class="btn btn-green" name="save" value="1" onclick="$('#save_template_item').val('1');">Добавить</button>
    </div>
</div>
<div class="form-group text-right">
    <input type="hidden" id="save_val" name="save" value="0"/>
    <button type="submit" class="btn btn-primary" name="save" value="1" onclick="$('#save_val').val('1');">Сохранить</button>
</div>
<?=Html::endForm();?>
<?php Pjax::end(); ?>
<script type="text/javascript">
    window.addEventListener('load', function(){
        function initDateTimeInput()
        {
            //использование для ввода даты плагина
           var dateTimeInput = $(<?= \yii\helpers\Json::encode('#'.Html::getInputId($itemModel, 'time'))?>);
           var newDateTimeInput = dateTimeInput.clone();
           dateTimeInput.prop('type', 'hidden');
           newDateTimeInput.prop('name', '');
           var dateTime = new Date(dateTimeInput.val());
           if(Object.prototype.toString.call(dateTime) === "[object Date]" && !isNaN( dateTime.getTime() ))
           {
               newDateTimeInput.val(dateTime.format('HH:MM'));
           }
           newDateTimeInput.prop('id', 'input_' + Math.random().toString(36).substring(7));
           dateTimeInput.after(newDateTimeInput);
           newDateTimeInput.datetimepicker({
                datepicker:false,
                format:'H:i',
                lang:'ru',
                mask:true,
                minTime:'9:00',
                onChangeDateTime: function(dp, $input){
                    dateTimeInput.val(dp.format('HH:MM:00'));
                }
              });
        }
       initDateTimeInput();
       $(document).on('pjax:success', function() {initDateTimeInput()});
    });
</script>