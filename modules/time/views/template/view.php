<?php
/* @var $this yii\web\View */
/* @var $model app\modules\time\models\TimeTemplate */
use app\modules\time\models\Time;
$this->title = 'Информация о шаблоне';
?>
<?php
if(\Yii::$app->session->hasFlash('time_template_update_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
      <?=\Yii::$app->session->getFlash('time_template_update_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('time_template_update_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
      <?=\Yii::$app->session->getFlash('time_template_update_error')?>
    </div>
    <?php
}
?>
<h1><?=$this->title;?></h1>
<?php
echo \yii\widgets\DetailView::widget([
    'model' => $model,
    'attributes' => [
        'name', 
        'org_id' => [
            'attribute' => 'org_id',
            'value' => $model->org->name
        ],
]]);
?>
<div>
        <table class="table table-striped table-bordered">
            <thead>
                <th>Время</th>
                <th>Тип времени</th>
                <th>Группа крови</th>
                <?php
                if($model->org && $model->org->enable_stop_registration)
                {
                ?>
                    <th>Закрыть регистрацию за ... (в часах)</th>
                <?php
                }
                ?>
            </thead>
            <tbody>
                <?php
                $typeList = Time::getTypeList();
                $groupList = Time::getGroupList();
                foreach(\app\modules\time\models\TimeTemplateItem::find()->where(['template_id' => $model->id])->asArray()->all() as $id => $timeTemplateItem)
                {
                    $groupId = (int)$timeTemplateItem['group'];
                    ?>
                    <tr>
                        <td><?=$timeTemplateItem['time'];?></td>
                        <td><?=array_key_exists($timeTemplateItem['type'], $typeList) ? $typeList[$timeTemplateItem['type']] : NULL;?></td>
                        <td><?=array_key_exists($groupId, $groupList) ? $groupList[$groupId] : '';?></td>
                        <?php
                        if($model->org && $model->org->enable_stop_registration)
                        {
                        ?>
                            <td><?=$timeTemplateItem['reg_stop_hours'];?></td>
                        <?php
                        }
                        ?>
                    </tr>
                    <?php
                }
                ?>
            </tbody>
        </table>
    </div>