<?php
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $pagination \yii\data\Pagination */
/* @var $filterModel app\modules\time\models\Time */
use app\modules\time\models\Time;
use yii\helpers\Html;
$this->title = 'Список шаблонов';
?>
<h1><?=$this->title?></h1>
<div class="text-right">
    <?php
    if(\Yii::$app->user->isManager() || \Yii::$app->user->isAdmin())
    {
        ?>
        <a href="<?= yii\helpers\Url::to(['/time/template/create'])?>" class="btn btn-primary">
            <i class="fa fa-plus"></i> Добавить шаблон
        </a>
        <?php
    }
    ?>
</div>
<?php
if(\Yii::$app->session->hasFlash('time_template_update_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
      <?=\Yii::$app->session->getFlash('time_template_update_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('time_template_update_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
      <?=\Yii::$app->session->getFlash('time_template_update_error')?>
    </div>
    <?php
}
?>
<?php
echo app\widgets\GridView::widget([
    'dataProvider' => $dataProvider,
    'pagination' => $pagination,
    'columns' => [
        [
            'attribute' => 'org_id',
            'value' => function($data){
                $orgList = Time::getOrgList();
                return array_key_exists($data['org_id'], $orgList) ? $orgList[$data['org_id']] : NULL;
            },
            'filter' => Time::getOrgList()
        ],
        'name',
                    [
            'class' => 'yii\grid\ActionColumn',
            'template' => '<div class="grid-action-buttons">{view} {update} {delete}</div>',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    $url = yii\helpers\Url::to(['/time/template/view', 'id' => $model['id']]);
                    return \yii\helpers\Html::a('<i class="fa fa-eye"></i> ', $url, [
                        'class' => 'btn btn-info btn-sm',
                        'title' => 'Просмотр'
                        ]);
                },
                'update' => function ($url, $model, $key) {
                    $url = yii\helpers\Url::to(['/time/template/update', 'id' => $model['id']]);
                    return \yii\helpers\Html::a('<i class="fa fa-edit"></i> ', $url, [
                        'class' => 'btn btn-success btn-sm',
                        'title' => 'Редактировать'
                        ]);
                },
                'delete' => function ($url, $model, $key) {
                    $url = yii\helpers\Url::to([
                        '/time/template/delete', 
                        'id' => $model['id']
                        ]);
                    return \yii\helpers\Html::a('<i class="fa fa-trash-o"></i> ', $url, [
                        'class' => 'btn btn-danger btn-sm',
                        'title' => 'Удалить'
                        ]);
                },
            ]
        ],
    ],
]);