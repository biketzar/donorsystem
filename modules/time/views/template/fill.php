<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use app\modules\time\models\Time;
$this->title = 'Заполнение с помощью шаблона';
$this->registerCssFile('@web2/jquery-ui/jquery-ui.min.css');
$this->registerJsFile('@web2/jquery-ui/jquery-ui.min.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerCssFile('@web2/multidatespicker/jquery-ui.multidatespicker.css');
$this->registerJsFile('@web2/multidatespicker/jquery-ui.multidatespicker.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web2/multidatespicker/datepicker-ru.js', ['depends' => 'yii\web\YiiAsset']);

?>
<?=Html::beginForm('', 'POST', ['id' => 'time-template-fill-form']);?>
<?=Html::errorSummary($model);?>
<div class="form-group">
    <label>Дата</label>
    <div id="datepicker"></div>
    <?=Html::activeTextArea($model, 'times', [
        'row' => 5,
        'class' => 'form-control',
        'id' => 'mdp-datepicker',
        'autocomplete' => 'off',
        'readonly' => TRUE,
        'required' => true])?>
</div>
<div class="form-group">
    <label>Шаблон</label>
    <?=Html::activeDropDownList($model, 'template_id', $templates, ['class' => 'form-control', 'required' => true])?>
</div>
<div class="form-group text-right">
    <button type="submit" class="btn btn-primary" name="save" value="1">Сохранить</button>
</div>
<?=Html::endForm();?>

<script type="text/javascript">
window.addEventListener('load', function(){
    var dateList = [];
    var input = $('#mdp-datepicker');
    input.val().split(', ').forEach(function(elem){
        if(elem.match(/^\d\d\.\d\d\.\d\d\d\d$/))
        {
            dateList.push(elem);
        }
    });
    console.log(dateList);
    function getDate(text)
    {
        var arr = text.split('.');
        return new Date(parseInt(arr[2], 10), parseInt(arr[1], 10)-1 , parseInt(arr[0], 10));
    }
    
    function selectHandler(dateText)
    {
        var index = dateList.indexOf(dateText);
        if(index === -1 )
        {
            dateList.push(dateText);
        }
        else
        {
            dateList.splice(index, 1);
        }
        dateList.forEach(function(elem, i){
            if(!elem.match(/^\d\d\.\d\d\.\d\d\d\d$/))
            {
                dateList.splice(index, 1);
            }
        });
        dateList.sort(function(a, b){ 
            return getDate(a).getTime() > getDate(b).getTime() ? 1 : -1;
        });
        input.val(dateList.join(', '));
    }
    
    function initPicker()
    {
        $( "#datepicker" ).datepicker( "destroy" );
        $( "#datepicker" ).datepicker({
            lang:'ru',
            minDate: 0,
            numberOfMonths: Math.floor($( "#datepicker" ).width() / 270),
            onSelect: function(dateText, obj)
            {
                selectHandler(dateText);
            },
            beforeShowDay: function(date){
                var day = date.getDate();
                var month = date.getMonth() + 1;
                var year = date.getFullYear();
                var str = (day > 9 ? day : '0' + day.toString()) + '.' 
                    + (month > 9 ? month : '0' + month.toString()) + '.' 
                    + year.toString();
                var className = dateList.indexOf(str) > -1 ? 'selected-day-ceil' : '';
                return [true, className];
            }
        });
    }
    initPicker();
    window.addEventListener('resize', function(){initPicker();});
});
</script>
<style>
    .ui-state-highlight, .ui-widget-content .ui-state-highlight, .ui-widget-header .ui-state-highlight,
    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
        border: 1px solid #c5c5c5;
        background: #f6f6f6;
        font-weight: normal;
        color: #454545;
    }
    .selected-day-ceil a{
        border: 1px solid #003eff!important;
        background: #007fff!important;
        font-weight: normal!important;
        color: #fff!important;
    }
</style>