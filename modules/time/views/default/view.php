<?php
/* @var $this yii\web\View */
/* @var $model app\modules\time\models\Time */
$this->title = 'Информация о времени';
?>
<h1><?=$this->title;?></h1>
<?php
echo \yii\widgets\DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id', 
        'org_id' => [
            'attribute' => 'org_id',
            'value' => $model->org->name
        ],
        'date_time' => [
            'attribute' => 'date_time',
            'value' => date('d.m.Y H:i', $model->date_time)
        ],
        'published' => [
            'attribute' => 'published',
            'value' => ($model->published ? 'Да' : 'Нет')
        ],
        'type' => [
            'attribute' => 'type',
            'value' => function($model, $detailView){
                $valueList = $model->getTypeList();
                return array_key_exists($model->type, $valueList) ? $valueList[$model->type] : $model->type;
            }
        ],
]
                + ($model->org->enable_stop_registration ? ['reg_stop_hours' => 'reg_stop_hours'] : [])
                
                ]);