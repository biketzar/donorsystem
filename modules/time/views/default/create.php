<?php
/* @var $this yii\web\View */
/* @var $model app\modules\donor\models\Time */
use yii\helpers\Html;
use app\modules\time\models\Time;

$this->title = 'Добавление времени сдачи';
?>
<h1><?=$this->title;?></h1>
<?=$this->render('_form', ['model' => $model]);?>


