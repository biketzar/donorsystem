<?php
/* @var $this yii\web\View */
/* @var $model app\modules\time\models\Time */
use yii\helpers\Html;
use app\modules\time\models\Time;
use yii\widgets\Pjax;
$this->registerCssFile('@web2/datetimepicker/jquery.datetimepicker.css');
$this->registerJsFile('@web2/datetimepicker/jquery.datetimepicker.full.min.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web2/datetimepicker/date.format.js', ['depends' => 'yii\web\YiiAsset']);
?>
<?php Pjax::begin(['enablePushState' => false]); ?>
<?php
if(\Yii::$app->session->hasFlash('time_update_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
      <?=\Yii::$app->session->getFlash('time_update_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('time_update_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
      <?=\Yii::$app->session->getFlash('time_update_error')?>
    </div>
    <?php
}
?>
<?=Html::beginForm('', 'POST', ['id' => 'time-form', 'data-pjax' => '']);?>
    <?=Html::errorSummary($model);?>
    <div class="form-group">
        <?=Html::activeLabel($model, 'org_id');?>
        <?=Html::activeDropDownList($model, 'org_id', ['' => '---'] + Time::getOrgList(), [
            'class' => 'form-control',
            'onchange' => "$('#time-form').submit();"
            ]);?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'type');?>
        <?=Html::activeDropDownList($model, 'type', Time::getTypeListForOrg($model->org_id), [
            'class' => 'form-control'
            ]);?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'group');?>
        <?=Html::activeDropDownList($model, 'group', Time::getGroupList(), [
            'class' => 'form-control'
            ]);?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'published');?>
        <?=Html::activeDropDownList($model, 'published', [1 => 'Да', 0 => 'Нет'], [
            'class' => 'form-control',
            'value' => (int)$model->published
            ]);?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'date_time');?>
        <?=Html::activeTextInput($model, 'date_time', ['class' => 'form-control']);?>
    </div>
    <?php 
    if($model->org && $model->org->enable_stop_registration == 1)
    {
        ?>
        <div class="form-group">
            <?=Html::activeLabel($model, 'reg_stop_hours');?>
            <?=Html::activeTextInput($model, 'reg_stop_hours', ['class' => 'form-control']);?>
        </div>
        <?php
    }
    ?>
    <div class="form-group text-right">
        <input type="hidden" id="save_val" name="save" value="0"/>
        <button type="submit" class="btn btn-primary" name="save" value="1" onclick="$('#save_val').val('1');">Сохранить</button>
    </div>
<?=Html::endForm();?>
<?php Pjax::end(); ?>
<script type="text/javascript">
    window.addEventListener('load', function(){
        function initDateTimeInput()
        {
            //использование для ввода даты плагина
           var dateTimeInput = $(<?= \yii\helpers\Json::encode('#'.Html::getInputId($model, 'date_time'))?>);
           var newDateTimeInput = dateTimeInput.clone();
           dateTimeInput.prop('type', 'hidden');
           newDateTimeInput.prop('name', '');
           var dateTime = new Date(dateTimeInput.val());
           if(Object.prototype.toString.call(dateTime) === "[object Date]" && !isNaN( dateTime.getTime() ))
           {
               newDateTimeInput.val(dateTime.format('dd.mm.yyyy HH:MM'));
           }
           newDateTimeInput.prop('id', 'input_' + Math.random().toString(36).substring(7));
           dateTimeInput.after(newDateTimeInput);
           newDateTimeInput.datetimepicker({
                format:'d.m.Y H:i',
                lang:'ru',
                mask:true,
                onChangeDateTime: function(dp, $input){
                    dateTimeInput.val(dp.format('yyyy-mm-dd HH:MM:00'));
                }
              });
        }
       initDateTimeInput();
       $(document).on('pjax:success', function() {initDateTimeInput()});
    });
</script>