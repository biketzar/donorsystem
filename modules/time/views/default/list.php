<?php
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $pagination \yii\data\Pagination */
/* @var $filterModel app\modules\time\models\Time */
use app\modules\time\models\Time;
use yii\helpers\Html;
$this->title = 'Список со временем';
$this->registerCssFile('@web2/datetimepicker/jquery.datetimepicker.css');
$this->registerJsFile('@web2/datetimepicker/jquery.datetimepicker.full.min.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerJsFile('@web2/datetimepicker/date.format.js', ['depends' => 'yii\web\YiiAsset']);
?>
<h1><?=$this->title?></h1>
<div class="text-right">
    <?php
    if(\Yii::$app->user->isManager() || \Yii::$app->user->isAdmin())
    {
        ?>
        <a href="<?= yii\helpers\Url::to(['/time/default/create'])?>" class="btn btn-primary">
            <i class="fa fa-plus"></i> Добавить
        </a>
        <a href="<?= yii\helpers\Url::to(['/time/template/fill'])?>" class="btn btn-info">
            <i class="fa fa-plus"></i> Добавить с помощью шаблона
        </a>
        <a href="<?= yii\helpers\Url::to(['/time/template/'])?>" class="btn btn-warning">
            <i class="fa fa-list"></i> Шаблоны
        </a>
        <?php
    }
    ?>
</div>
<div class="alerts">
<?php
if(\Yii::$app->session->hasFlash('time_update_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
      <?=\Yii::$app->session->getFlash('time_update_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('time_update_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
      <?=\Yii::$app->session->getFlash('time_update_error')?>
    </div>
    <?php
}
?>
</div>
<?php
yii\grid\ActionColumn::class;
echo app\widgets\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'pagination' => $pagination,
    'filterUrl' => $filterUrlArr,
    'columns' => [
        [
            'label' => '',
            'format' => 'raw',
            'filter' => Html::checkbox('flag-for-action', false, [
                   'class' => 'all-flags-for-action',
                    'title' => 'Выделить/сбросить все'
                   ]),
            'value' => function($data){
                if(!app\modules\rest\components\TimeHelper::canUpdate($data['id']))
                {
                    return ' ';
                }
                $url = yii\helpers\Url::to([
                        '/time/default/delete', 
                        'id' => $data['id']
                        ]);
                return Html::checkbox('action', false, [
                   'class' => 'flag-for-action',
                   'value' => $url
                   ]);
            }
        ],
        'id', 
        [
            'attribute' => 'org_id',
            'value' => function($data){
                $orgList = Time::getOrgList();
                return array_key_exists($data['org_id'], $orgList) ? $orgList[$data['org_id']] : NULL;
            },
            'filter' => Time::getOrgList()
        ],
        [
            'attribute' => 'date_time',
            'label' => 'Дата и время',
            'format' => 'raw',
            'value' => function($data){
                return date('d.m.Y H:i', $data['date_time']);
            },
            'filterInputOptions' => ['class' => 'form-control'],
            'filter' => trntv\yii\datetime\DateTimeWidget::widget([
                            'model' => $filterModel,
                            'attribute' => 'filterTime',
                            'phpDatetimeFormat' => 'dd.MM.yyyy',
                            'phpMomentMapping' => ['dd.MM.yyyy' => 'DD.MM.YYYY'],
                            'momentDatetimeFormat' => null,
                            'clientOptions' => [
                                'useCurrent' => false
                            ],
                            'clientEvents' => [
                                    'dp.change' => 'function(date, oldDate ){$(".trntv").val(date.toString()).change();}'
                           ]
                        ]). Html::hiddenInput('', '', ['class' => 'trntv'])
        ],
        [
            'attribute' => 'type',
            'filter' => Time::getTypeList(),
            'value' => function($data){
                $list = Time::getTypeList();
                return array_key_exists($data['type'], $list) ? $list[$data['type']] : NULL;
            },
        ],
        [
            'attribute' => 'published',
            'label' => "Опубликовано",
            'value' => function($data){ return $data['published'] ? 'Да' : 'Нет';},
            'filter' => ['1' => 'Да', '0' => 'Нет']
        ],
                    [
            'attribute' => 'group',
            'value' => function($data){ 
                $groupList = Time::getGroupList();
                $id = (int)$data['group'];
                return array_key_exists($id, $groupList) ? $groupList[$id] : '';                
            },
            'filter' => Time::getGroupList()
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '<div class="grid-action-buttons">{view} {update} {delete}</div>',
            'buttons' => [
                'view' => function ($url, $model, $key) {
                    if(!app\modules\rest\components\TimeHelper::canView($model['id']))
                    {
                        return ' ';
                    }
                    $url = yii\helpers\Url::to(['/time/default/view', 'id' => $model['id']]);
                    return \yii\helpers\Html::a('<i class="fa fa-eye"></i> ', $url, [
                        'class' => 'btn btn-info btn-sm',
                        'title' => 'Просмотр'
                        ]);
                },
                'update' => function ($url, $model, $key) {
                    if(!app\modules\rest\components\TimeHelper::canUpdate($model['id']))
                    {
                        return ' ';
                    }
                    $url = yii\helpers\Url::to(['/time/default/update', 'id' => $model['id']]);
                    return \yii\helpers\Html::a('<i class="fa fa-edit"></i> ', $url, [
                        'class' => 'btn btn-success btn-sm',
                        'title' => 'Редактировать'
                        ]);
                },
                'delete' => function ($url, $model, $key) {
                    if(!app\modules\rest\components\TimeHelper::canDelete($model['id']))
                    {
                        return ' ';
                    }
                    $url = yii\helpers\Url::to([
                        '/time/default/delete', 
                        'id' => $model['id']
                        ]);
                    return \yii\helpers\Html::a('<i class="fa fa-trash-o"></i> ', $url, [
                        'class' => 'btn btn-danger btn-sm',
                        'title' => 'Удалить'
                        ]);
                },
            ]
        ],
    ],
]);
?>
<div class="col-md-12">
        Выберите действие: 
        <?= Html::dropDownList('', '', [
            '' => '---',
            'delete' => 'Удалить'
        ], [
            'id' => 'batch-action-dropdown'
        ])?>
        <button type="button" id="batch-action-btn" class="btn btn-danger">Выполнить</button>
</div>
<script type="text/javascript">
    window.addEventListener('load', function(){
        var dateTimeInput = $(<?= \yii\helpers\Json::encode('#'.Html::getInputId($filterModel, 'time'))?>);
        dateTimeInput.datetimepicker({
            format: 'H:i',
            lang: 'ru',
            mask: true,
            datepicker: false,
            timepicker: true,
        });
    });
    window.addEventListener('load', function(){
        $('.all-flags-for-action').change(function(){
            $('.flag-for-action').prop('checked', $(this).prop('checked'));
        });
        
        function deleteBatch()
        {
            if($('.flag-for-action:checked').length === 0)
            {
                alert('Выберите элементы для удаления');
                return;
            }
            if(confirm('Потвердите удаление выбранных элементов'))
            {
                $('.flag-for-action:checked').each(function(index, domElement){
                    var url = $(this).val();
                    $.get(url, {}, function(data){
                        var msg;
                        if(data.result)
                        {
                            msg = $('<div class="alert alert-success" role="alert"></div>').html(data.msg);
                        }
                        else
                        {
                            msg = $('<div class="alert alert-danger" role="alert"></div>').html(data.msg);
                        }
                        $('.alerts').append(msg);
                    });
                });
                setTimeout(function(){
                    if(confirm("Запросы на удаление отправлены. Обновить страницу?"))
                    {
                        location.reload();
                    }  
                }, 2500);
            }
        }
        
        $('#batch-action-btn').click(function(e){
            e.preventDefault();
            var action = $('#batch-action-dropdown').val();
            switch(action)
            {
                case 'delete':
                    deleteBatch();
                    break;
                default:
                    alert('Выберите действие');
            }
            return false;
        })
    });
</script>