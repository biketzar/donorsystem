<?php
namespace app\modules\time;

class Module extends \yii\base\Module
{
  public $controllerNamespace = 'app\modules\time\controllers';
  
  public function init()
  {
      parent::init();
  }
}
