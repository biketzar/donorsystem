<?php
namespace app\modules\time\components;

class AccessHelper
{
    /**
     * может ли пользователь $userIdentity редактировать время организации
     * @param type $org
     * @param \app\models\User $userIdentity
     * @return boolean
     */
    public static function canEditTime($org, $userIdentity)
    {
        $allowOrgs = self::allowOrgsByUser($userIdentity);
        if(in_array($org->id, $allowOrgs))
        {
            return true;
        }
        return false;
    }
    
    /**
     * возвращает список id организаций, которые доступны для $userIdentity для редактирования времени
     * @param \app\models\User $userIdentity
     * @return array
     */
    public static function allowOrgsByUser($userIdentity)
    {
        if(!$userIdentity)
        {
            return [];
        }
        $roleList = array_keys($userIdentity->getRoleList());
        if(in_array('admin', $roleList))
        {
            return \app\modules\org\models\Orgs::find()->select(['id'])->column();
        }
        if(in_array('manager', $roleList))
        {
            return \app\modules\org\models\Staff::find()->select('org_id')->where(['user_id' => $userIdentity->id])->column();
        }
        return [];
    }
}

