<?php
namespace app\modules\notifications\models;
/**
 * Уведомления
 * @property int $id
 * @property int $user_id
 * @property int $creator_id
 * @property int $status
 * @property string $text
 * @property string $title
 * @property string $add_date
 * @property \app\models\User $user
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * новое уведомление
     */
    CONST STATUS_NEW = 1;
    /**
     * просмотренное уведомление
     */
    CONST STATUS_SEEN = 2;
    
    public static function tableName() {
        return 'Notifications';
    }
    
    /**
     * связь с пользователем
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }
    
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'user_id' => 'Получатель',
            'creator_id' => 'Отправитель',
            'title' => 'Тема',
            'text' => 'Сообщение',
            'add_date' => 'Дата и время добавления'
        ];
    }
    
    /**
     * добавление уведомления
     * @param string $title
     * @param string $text
     * @param int $userId
     * @return boolean|Notification
     */
    public static function addNotification($title, $text, $userId)
    {
        $model = new Notification();
        $model->user_id = (int)$userId;
        $model->creator_id = (int)\Yii::$app->user->getId();
        $model->title = strip_tags($title);
        $model->text = \yii\helpers\HtmlPurifier::process($text);
        $model->status = self::STATUS_NEW;
        $model->add_date = date('Y-m-d H:i:s');
        if(!$model->save())
        {
            return false;
        }
        return $model;
    }
    
    /**
     * изменение статуса уведомления
     * @param int $id
     */
    public static function setSeen($id)
    {
        self::updateAll(['status' => self::STATUS_SEEN], ['id' => (int)$id]);
    }
}