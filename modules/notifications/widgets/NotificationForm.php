<?php
namespace app\modules\notifications\widgets;

class NotificationForm extends \yii\base\Widget
{
    public function run() {
        if(!\app\modules\rest\components\NotificationHelper::canSendNotification())
        {
            return '';
        }
        $showForm = true;
        $helper = new \app\modules\rest\components\NotificationHelper;
        $message = '';
        if(\Yii::$app->request->post('form') === 'NotificationForm')
        {
            $result = $helper->add(
                    \yii\helpers\ArrayHelper::getValue($_POST, 'title', ''), 
                    \yii\helpers\ArrayHelper::getValue($_POST, 'text', ''), 
                    \Yii::$app->request->post('recepient'));
            $showForm = !$result['result'];
            $message = $result['msg'];
        }
        return $this->render('notification-form', [
            'showForm' => $showForm,
            'message' => $message
            ]);
    }
}