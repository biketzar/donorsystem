<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
?>
<div id="send-notification" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
        <?php
        $pjax = Pjax::begin();
        ?>
      <?=Html::beginForm('', 'POST', ['data-pjax' => '']);?>  
      <?=Html::hiddenInput('recepient', \Yii::$app->request->post('recepient', ''), ['id' => 'notification-recepient-id']);?>
      <?=Html::hiddenInput('form', 'NotificationForm');?>
      <div class="modal-header">
        <h4 class="modal-title">Отправка уведомления</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
          <div class="notification-form-msg"><?=$message;?></div>
          <div class="form-group" style="display: <?=$showForm ? 'block' : 'none'?>">
            <?=Html::label('Тема*');?>
            <?=Html::textInput('title', \Yii::$app->request->post('title', ''), ['class' => 'form-control', 'required' => true]);?>
          </div>
          <div class="form-group" style="display: <?=$showForm ? 'block' : 'none'?>">
            <?=Html::label('Сообщение*');?>
            <?=Html::textarea('text', \Yii::$app->request->post('text', ''), ['class' => 'form-control', 'required' => true]);?>
          </div>
      </div>
      <div class="modal-footer">
          <button type="submit" class="btn btn-primary" style="display: <?=$showForm ? 'block' : 'none'?>">Отправить</button> 
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button> 
      </div>
      <?=Html::endForm();?>
        <?php
        Pjax::end();
        ?>
    </div>
  </div>
</div>
<script type="text/javascript">
window.addEventListener('load', function(){
   $(document).on('click', '.send-notification-btn', function(e){
      e.preventDefault();
      var form = $('#send-notification form');
      form.find('[name=title]').val('');
      form.find('[name=text]').val('');
      form.find('.form-group').show();
      form.find('button.btn-primary').show();
      form.find('.notification-form-msg').html('');
      form.find('#notification-recepient-id').val($(this).data('recepient'));
      $('#send-notification').modal('show');
      return false;
   });
});
</script>

