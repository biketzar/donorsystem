<?php

?>
<div id="show-notification" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"></h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button> 
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
window.addEventListener('load', function(){
    function showNotification(url)
    {
        var modal = $('#show-notification');
        $.get(url, function(data){
            modal.find('.modal-title').html(data.title);
            modal.find('.modal-body').html(data.text);
            modal.modal('show');
            $.get('/notifications/default/not-seen-count', function(data){
                $('.notification-count').html(data);
            });
        });
    }
    
    $(document).on('click', '.show-notifiction', function(e){
        e.preventDefault();
        showNotification($(this).data('url'));
        $(this).parents('tr').removeClass('new-notification');
        return false;
    });
});
</script>