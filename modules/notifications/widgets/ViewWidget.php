<?php
namespace app\modules\notifications\widgets;

class ViewWidget extends \yii\base\Widget
{
    public function run()
    {
        return $this->render('view');
    }
}
