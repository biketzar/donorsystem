<?php
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
$this->title = 'Уведомления';
?>
<h4>Уведомления</h4>
<?php
if(\Yii::$app->session->hasFlash('notification_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
      <?=\Yii::$app->session->getFlash('notification_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('notification_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
      <?=\Yii::$app->session->getFlash('notification_error')?>
    </div>
    <?php
}
?>
<?php
echo yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'rowOptions' => function ($model, $key, $index, $grid){
        if($model->status == app\modules\notifications\models\Notification::STATUS_NEW)
        {
            return ['class' => 'new-notification'];
        }
        return [];
    },
    'columns' => [
        [
            'attribute' => 'title',
        ],
        [
            'attribute' => 'text',
            'value' => function($model){
                if(mb_strlen($model->text, 'UTF-8') > 90)
                {
                    return trim(mb_substr(strip_tags($model->text), 0, 90, 'UTF-8')).'...';
                }
                return $model->text;
            }
        ],
        [
            'attribute' => 'add_date',
            'value' => function($model){
                return date('d.m.Y H:i', strtotime($model->add_date));
            }
        ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '<div class="grid-action-buttons">{view} {delete}</div>',
                'buttons' => [
                    'view' => function ($url, $model, $key){
                        return \yii\helpers\Html::a('<i class="fa fa-eye"></i> ', '#', [
                            'class' => 'btn btn-info btn-sm show-notifiction',
                            'title' => 'Просмотр',
                            'data-url' => yii\helpers\Url::to($url)
                        ]);
                    },
                    'delete' => function ($url, $model, $key) {
                        $url = yii\helpers\Url::to([
                            '/notifications/default/delete', 
                            'id' => $model->id, 
                            'return' => yii\helpers\Url::current()]);
                        return \yii\helpers\Html::a('<i class="fa fa-trash"></i> ', $url, [
                            'class' => 'btn btn-danger btn-sm',
                            'title' => 'Удалить'
                        ]);
                    },
                ],
            ]
    ]
    ]);
?>
<?=
app\modules\notifications\widgets\ViewWidget::widget();?>