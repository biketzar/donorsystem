<?php
namespace app\modules\notifications\controllers;
use yii\helpers\ArrayHelper;
use Yii;
use app\modules\notifications\models\Notification;
class DefaultController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'not-seen-count', 'delete'],
                        'roles' => ['admin', 'manager', 'donor'],
                    ]
                ],
            ],
        ];
    }
    
    public function actionIndex()
    {
         $query = Notification::find()
                ->andWhere(['user_id' => \Yii::$app->user->getId()]);
         if(\Yii::$app->request->get('id'))
         {
             $query->andWhere(['id' => \Yii::$app->request->get('id')]);
         }
         $dataProvider = new \yii\data\ActiveDataProvider([
             'query' => $query,
             'sort'=> ['defaultOrder' => ['id' => SORT_DESC]],
             'pagination' => [
                'pageSize' => 10,
            ],
         ]);
         return $this->render('list', [
            'dataProvider' => $dataProvider
         ]);
    }
    
    public function actionDelete()
    {
        $this->redirect(\Yii::$app->request->get('return'));
        $helper = new \app\modules\rest\components\NotificationHelper();
        $result = $helper->delete(\Yii::$app->request->get('id'));
        \Yii::$app->session->setFlash($result['result'] ? 'notification_success' : 'notification_error', $result['msg']);
        \Yii::$app->getResponse()->send();
    }
    
    public function actionView()
    {
        \yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $helper = new \app\modules\rest\components\NotificationHelper();
        $data = $helper->view(\Yii::$app->request->get('id'));
        if(!$data['result'])
        {
            throw new \yii\web\HttpException(404);
        }
        return $data;
    }
    
    public function actionNotSeenCount()
    {
        return \app\modules\rest\components\NotificationHelper::getNotSeenCount();
    }
}