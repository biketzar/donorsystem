<?php
namespace app\modules\notifications;

class Module extends \yii\base\Module
{
  public $controllerNamespace = 'app\modules\notifications\controllers';
  
  public function init()
  {
      parent::init();
  }
}
