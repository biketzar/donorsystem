<?php
namespace app\modules\rest\components;
use \app\modules\org\models\Orgs;
/**
 *
 */
class OrgHelper extends \yii\base\Component
{
    /**
     * нет доступа
     */
    CONST CODE_NOT_ALLOWED = 1;
    /**
     * код успешной операции
     */
    CONST CODE_SUCCESS = 2;
    /**
     * оргаанизация не найдена
     */
    CONST CODE_NOT_FOUND = 3;
    /**
     * оргаанизация найдена
     */
    CONST CODE_FOUND = 4;
    /**
     * не удалось  удалить организацию
     */
    CONST CODE_ERROR = 5;
    /**
     * при сохранении произошла ошибка
     */
    CONST CODE_HAS_ERROR = 6;
    /**
     * количество максимально выдаваемых строк не может быть больше этого значения
     */
    CONST MAX_LIMIT = 100;

    public function create($data)
    {
        if(!self::canCreate())
        {
            return [
                'result' => false,
                'msg' => 'Нет доступа',
                'code' => self::CODE_NOT_ALLOWED];
        }
        $org = new Orgs(['scenario' => 'insert']);
        $org->setAttributes($data);
        if($org->save())
        {
            return [
                    'result' => true,
                    'id' => $org->id,
                    'msg' => 'Организация успешно добавлена',
                    'code' => self::CODE_SUCCESS];
        }
        else
        {
            return [
                    'result' => false,
                    'msg' => 'Необходимо исправить следующие ошибки',
                    'errors' => $org->getErrors(),
                    'code' => self::CODE_HAS_ERROR
                ];
        }
    }

    public static function canCreate()
    {
        if(\Yii::$app->user->isAdmin())
        {
            return true;
        }
        return false;
    }

    public function view($id)
    {
        /** @var $org Orgs */
        $org = Orgs::find()->where(['id' => $id])->limit(1)->one();
        if(!$org)
        {
            return [
                    'result' => false,
                    'msg' => 'Организация не найдена',
                    'code' => self::CODE_NOT_FOUND
                ];
        }
        return [
           'result' => true,
           'code' => self::CODE_FOUND
        ] + $org->getAttributes();
    }

    public function update($data)
    {
        /** @var $org Orgs */
        $id = (int) \yii\helpers\ArrayHelper::getValue($data, 'id');
        if(!self::canUpdate($id))
        {
            return [
                'result' => false,
                'msg' => 'Нет доступа',
                'code' => self::CODE_NOT_ALLOWED];
        }
        $org = Orgs::find()->where(['id' => $id])->limit(1)->one();
        if(!$org)
        {
            return [
                    'result' => false,
                    'msg' => 'Организация не найдена',
                    'code' => self::CODE_NOT_FOUND
                ];
        }
        $org->setScenario('update');
        $org->setAttributes($data);
        if($org->save())
        {
            return [
                    'result' => true,
                    'id' => $org->id,
                    'msg' => 'Организация успешно обновлена',
                    'code' => self::CODE_SUCCESS];
        }
        else
        {
            return [
                    'result' => false,
                    'msg' => 'Необходимо исправить следующие ошибки',
                    'errors' => $org->getErrors(),
                    'code' => self::CODE_HAS_ERROR
                ];
        }
    }

    /**
     * 
     * @param int $id идентификатор организации
     * @return boolean
     */
    public static function canUpdate($id)
    {
        $identity = \Yii::$app->user->getIdentity();
        if(!\Yii::$app->user->isGuest
                && in_array('admin', array_keys($identity->getRoleList())))
        {
            return true;
        }
        if(!\Yii::$app->user->isGuest
                && (in_array('manager', array_keys(\Yii::$app->user->getIdentity()->getRoleList()))))
        {
            return in_array($id, \app\modules\time\components\AccessHelper::allowOrgsByUser($identity));
        }
        return false;
    }

    public function delete($id)
    {
        if(!self::canDelete())
        {
            return [
                'result' => false,
                'msg' => 'Нет доступа',
                'code' => self::CODE_NOT_ALLOWED];
        }
        $org = Orgs::find()->where(['id' => $id])->limit(1)->one();
        if(!$org)
        {
            return [
                    'result' => false,
                    'msg' => 'Организация не найдена',
                    'code' => self::CODE_NOT_FOUND
                ];
        }
        if($org->delete())
        {
            return [
                'result' => true,
                'msg' => 'Организация удалена',
                'code' => self::CODE_SUCCESS
            ];
        }
        else
        {
            return [
                'result' => false,
                'msg' => 'Организация не удалена',
                'code' => self::CODE_ERROR
            ];
        }
    }

    public static function canDelete()
    {
        if(!\Yii::$app->user->isGuest
                && in_array('admin', array_keys(\Yii::$app->user->getIdentity()->getRoleList())))
        {
            return true;
        }
        return false;
    }

    public static function canView(){
        if(!\Yii::$app->user->isGuest){
            return true;
        }
        return false;
    }

    public function getList($filter, $orderField, $limit = 0, $offset = 0)
    {
        $data = array('result' => true);
        $limit = (($limit > 0 && $limit < self::MAX_LIMIT) ? (int)$limit : self::MAX_LIMIT);
        $query = Orgs::find();
        $filterModel = new Orgs(['scenario' => 'search']);
        $filterModel->setAttributes($filter);
        $query = self::addFilter($filterModel, $query);
        $query = self::addOrder($query, $orderField);
        $countQuery = clone $query;
        $data['total'] = $countQuery->count();
        $query->limit($limit);
        $data['limit'] = $limit;
        if($offset > 0)
        {
            $query->offset($offset);
            $data['offset'] = $offset;
        }
        else
        {
            $data['offset'] = 0;
        }
        $models = $query->all();
        $rows = [];
        foreach($models as $model)
        {
            $rows[] = $model->getAttributes();
        }
        $data['organizations'] = $rows;
        return $data;
    }

    /**
     * добавление в запрос $query фильтрации по данным из $filterModel
     * @param Orgs $filterModel
     * @param \yii\db\ActiveQuery $query
     * @return \yii\db\ActiveQuery
     */
    public static function addFilter($filterModel, $query)
    {
        if($filterModel->id)
        {
            $query->andWhere(['id' => $filterModel->id]);
        }
        if($filterModel->name)
        {
            $query->andWhere(['LIKE', 'name', $filterModel->name]);
        }
        if($filterModel->address)
        {
            $query->andWhere(['LIKE', 'address', $filterModel->name]);
        }
        if($filterModel->description)
        {
            $query->andWhere(['LIKE', 'description', $filterModel->name]);
        }
        if($filterModel->link)
        {
            $query->andWhere(['LIKE', 'link', $filterModel->name]);
        }
        if($filterModel->phone)
        {
            $query->andWhere(['LIKE', 'phone', $filterModel->name]);
        }
        if($filterModel->status)
        {
            $query->andWhere(['status' => $filterModel->id]);
        }
        return $query;
    }

    /**
     * добавление сортировки
     * @param \app\modules\donor\models\DonorQuery $query
     * @param string $field
     * @return \app\modules\donor\models\DonorQuery
     */
    public static function addOrder($query, $field)
    {
        if(!$field)
        {
            return $query;
        }
        $dir = SORT_ASC;
        if(strpos($field, '-') === 0)
        {
            $dir = SORT_DESC;
            $field = substr($field, 1);
        }
        if(!in_array($field, array_keys(Orgs::getTableSchema()->columns)))
        {
            return $query;
        }
        $query->orderBy([$field => $dir]);
        return $query;
    }
}