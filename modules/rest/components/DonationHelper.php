<?php
namespace app\modules\rest\components;
use app\modules\donation\models\Donation;
use app\modules\donation\components\StatusHelper;
use app\modules\donor\models\Donor;
use app\modules\time\models\Time;
/**
 * 
 */
class DonationHelper extends \yii\base\Component
{
    /**
     * Донор не найден
     */
    CONST CODE_DONOR_NOT_FOUND = 1;
    /**
     * Время не найдено или запись на указанное время остановлена
     */
    CONST CODE_TIME_NOT_FOUND = 2;
    /**
     * успешная операция
     */
    CONST CODE_SUCCESS = 3;
    /**
     * Необходимо исправить ошибки
     */
    CONST CODE_HAS_ERROR = 4;
    /**
     * нет доступа
     */
    CONST CODE_NOT_ALLOWED = 5;
    /**
     * запись на сдачу не найдена
     */
    CONST CODE_NOT_FOUND= 6;
    /**
     * невозможно изменить статус
     */
    CONST CODE_TRANSITIONS_ERROR = 7;
    /**
     * ошибка при измении статуса
     */
    CONST CODE_STATUS_ERROR = 8;
    /**
     * ресурс заблокирован
     */
    CONST CODE_MUTEX = 9;
    /**
     * количество максимально выдаваемых строк не может быть больше этого значения
     */
    CONST MAX_LIMIT = 1000;
    
    /**
     * запись на сдачу
     * @param array $data
     * @return array
     */
    public function enroll($data)
    {
        $mutex = new \yii\mutex\FileMutex();
        if(!$mutex->acquire(self::class, 30))
        {
            return [
                    'result' => false,
                    'msg' => 'Ресурс заблокирован',
                    'code' => self::CODE_MUTEX
                    ];
        }
        if(!array_key_exists('donor_id', $data))
        {
            $data['donor_id'] = \Yii::$app->user->getId();
        }
        $donor = Donor::find()->where(['id' => (int)$data['donor_id']])->limit(1)->one();
        if(!$donor)
        {
            $mutex->release(self::class);
            return [
                    'result' => false,
                    'msg' => 'Донор не найден',
                    'code' => self::CODE_DONOR_NOT_FOUND
                    ];
        }
        $time = Time::find()
                ->where(['id' => (int)$data['time_id'], 'published' => true])
                ->limit(1)->one();
        if(!$time)
        {
            $mutex->release(self::class);
            return [
                    'result' => false,
                    'msg' => 'Время не найдено или запись на указанное время остановлена',
                    'code' => self::CODE_TIME_NOT_FOUND
                    ];
        }
        if(Donation::find()
                ->where(['time_id' => $time->id])
                ->andWhere(['IN', 'event_id', [StatusHelper::STATUS_READY_TO_DONATE, StatusHelper::STATUS_DONATED]])->count() > 0)
        {
            $mutex->release(self::class);
            return [
                    'result' => false,
                    'msg' => 'Время не найдено или запись на указанное время остановлена',
                    'code' => self::CODE_TIME_NOT_FOUND
                    ];
        }
        $model = new Donation(['scenario' => 'insert']);
        $model->donor_id = $donor->id;
        $model->time_id = $time->id;
        $model->event_id = StatusHelper::STATUS_READY_TO_DONATE;
        if($model->save())
        {
            $mutex->release(self::class);
            \app\components\NotificationHelper::enroll($model);
            return [
                    'result' => true,
                    'msg' => 'Донор успешно записан на указанное время',
                    'code' => self::CODE_SUCCESS,
                    'id' => $model->id
                    ];
        }
        else
        {
            $mutex->release(self::class);
             return [
                    'result' => false,
                    'msg' => 'Необходимо исправить следующие ошибки',
                    'errors' => $model->getErrors(),
                    'code' => self::CODE_HAS_ERROR
                ];
        }
    }
    
    /**
     * отмена записи на сдачу по медицинским причинам со стороны станции
     * @param array $data
     * @return array
     */
    public function denial($data)
    {
        /* @var $donation Donation */
        $id = (int)$data['id'];
        if(!self::canDenial($id))
        {
            return [
                    'result' => false,
                    'msg' => 'Нет доступа',
                    'code' => self::CODE_NOT_ALLOWED
                    ];
        }
        $donation = Donation::find()->where(['id' => $id])->limit(1)->one();
        if(!$donation)
        {
            return [
                    'result' => false,
                    'msg' => 'Запись на сдачу не найдена',
                    'code' => self::CODE_NOT_ALLOWED
                    ];
        }
        if(!StatusHelper::canTransitions($donation->event_id, StatusHelper::STATUS_MANAGER_CANCELED))
        {
            return [
                'result' => false,
                'msg' => 'Из текущего статуса запрещено отмена записи на сдачу по медицинским причинам со стороны станции',
                'code' => self::CODE_TRANSITIONS_ERROR
            ];
        }
        $donation->event_id = StatusHelper::STATUS_MANAGER_CANCELED;
        if(array_key_exists('reason', $data))
        {
            $donation->reason = strip_tags($data['reason']);
        }
        if($donation->save())
        {
            \app\components\NotificationHelper::denial($donation);
            return [
                    'result' => true,
                    'msg' => 'Запись на сдачу отменена',
                    'code' => self::CODE_SUCCESS
                    ];
        }
        else
        {
            return [
                    'result' => false,
                    'msg' => 'Запись на сдачу не отклонена',
                    'code' => self::CODE_STATUS_ERROR
                    ];
        }
        
    }
    
    /**
     * может ли текущий пользователь отменить запись на сдачу по медицинским причинам со стороны станции
     * @param int|string $id
     * @return boolean
     */
    public static function canDenial($id)
    {
        $roleList = array_keys(\Yii::$app->user->getIdentity()->getRoleList());
        if(!\Yii::$app->user->isGuest 
                && in_array('admin', $roleList))
        {
            return true;
        }
        if(!\Yii::$app->user->isGuest 
                && in_array('manager', $roleList))
        {
            $donationOrgId = Donation::find()->select(['org_id'])->where(['id' => $id])->limit(1)->scalar();
            return in_array($donationOrgId, \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity()));
        }
        return false;
    }
    
    /**
     * может ли текущий пользователь отменить свою запись на сдачу
     * @param int|string $id
     * @return boolean
     */
    public static function canCancel($id)
    {
        $donationDonorId = Donation::find()->select(['donor_id'])->where(['id' => $id])->limit(1)->scalar();
        return $donationDonorId == \Yii::$app->user->getId();
    }
    
    /**
     * отмена записи на сдачу со стороны донора
     * @param array $data
     * @return array
     */
    public static function cancel($data)
    {
        /* @var $donation Donation */
        $id = (int)$data['id'];
        if(!self::canCancel($id))
        {
            return [
                    'result' => false,
                    'msg' => 'Нет доступа',
                    'code' => self::CODE_NOT_ALLOWED
                    ];
        }
        $donation = Donation::find()->where(['id' => $id])->limit(1)->one();
        if(!$donation)
        {
            return [
                    'result' => false,
                    'msg' => 'Запись на сдачу не найдена',
                    'code' => self::CODE_NOT_ALLOWED
                    ];
        }
        if(!StatusHelper::canTransitions($donation->event_id, StatusHelper::STATUS_DONOR_CANCELED))
        {
            return [
                'result' => false,
                'msg' => 'Из текущего статуса запрещено отмена записи',
                'code' => self::CODE_TRANSITIONS_ERROR
            ];
        }
        $donation->event_id = StatusHelper::STATUS_DONOR_CANCELED;
        if(array_key_exists('reason', $data))
        {
            $donation->reason = strip_tags($data['reason']);
        }
        if($donation->save())
        {
            \app\components\NotificationHelper::cancel($donation);
            return [
                    'result' => true,
                    'msg' => 'Запись на сдачу отменена',
                    'code' => self::CODE_SUCCESS
                    ];
        }
        else
        {
            return [
                    'result' => false,
                    'msg' => 'Запись на сдачу не отменена',
                    'code' => self::CODE_STATUS_ERROR
                    ];
        }
    }
    
    /**
     * может ли текущий пользователь завершить сдачу
     * @param int|string $id
     * @return boolean
     */
    public static function canComplete($id)
    {
        $roleList = \Yii::$app->user->isGuest ? [] : array_keys(\Yii::$app->user->getIdentity()->getRoleList());
        if(!\Yii::$app->user->isGuest 
                && in_array('admin', $roleList))
        {
            return true;
        }
        if(!\Yii::$app->user->isGuest 
                && in_array('manager', $roleList))
        {
            $donationOrgId = Donation::find()->select(['org_id'])->where(['id' => $id])->limit(1)->scalar();
            return in_array($donationOrgId, \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity()));
        }
        return false;
    }
    
    /**
     * может ли текущий посмотреть лог сдачи крови
     * @param int|string $id
     * @return boolean
     */
    public static function canViewLog($id)
    {
        $roleList = array_keys(\Yii::$app->user->getIdentity()->getRoleList());
        if(\Yii::$app->user->isAdmin())
        {
            return true;
        }
        if(\Yii::$app->user->isManager())
        {
            $donationOrgId = Donation::find()->select(['org_id'])->where(['id' => $id])->limit(1)->scalar();
            return in_array($donationOrgId, \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity()));
        }
        return false;
    }
    
    /**
     * завершение сдачи
     * @param array $data
     * @return array
     */
    public static function complete($data)
    {
        /* @var $donation Donation */
        $id = (int)$data['id'];
        if(!self::canComplete($id))
        {
            return [
                    'result' => false,
                    'msg' => 'Нет доступа',
                    'code' => self::CODE_NOT_ALLOWED
                    ];
        }
        $donation = Donation::find()->where(['id' => $id])->limit(1)->one();
        if(!$donation)
        {
            return [
                    'result' => false,
                    'msg' => 'Запись на сдачу не найдена',
                    'code' => self::CODE_NOT_ALLOWED
                    ];
        }
        if(!StatusHelper::canTransitions($donation->event_id, StatusHelper::STATUS_DONATED))
        {
            return [
                'result' => false,
                'msg' => 'Из текущего статуса запрещено перевод в статус "Завершение сдачи"',
                'code' => self::CODE_TRANSITIONS_ERROR
            ];
        }
        $donation->event_id = StatusHelper::STATUS_DONATED;
        if(array_key_exists('reason', $data))
        {
            $donation->reason = strip_tags($data['reason']);
        }
        if($donation->save())
        {
            return [
                    'result' => true,
                    'msg' => 'Сдача успешно завершена',
                    'code' => self::CODE_SUCCESS
                    ];
        }
        else
        {
            return [
                    'result' => false,
                    'msg' => 'Не удалось изменить статус',
                    'code' => self::CODE_STATUS_ERROR
                    ];
        }
    }
    
    /**
     * отфильтрованный список со временем
     * @param array $filter
     * @param string $orderField например 'id' - для сортировки по возрастанию, '-id' - для сортировки по убыванию
     * @param string|int $limit
     * @param string|int $offset
     */
    public function getList($filter, $orderField, $limit = 0, $offset = 0)
    {
        /* @var $donation Donation */
        $data = array();
        $limit = (($limit > 0 && $limit < self::MAX_LIMIT) ? (int)$limit : self::MAX_LIMIT);
        $donationTableName = Donation::getTableSchema()->fullName;
        $timeTableName = Time::getTableSchema()->fullName;
        $donorTableName = Donor::getTableSchema()->fullName;
        $query = Donation::find();
        if(\Yii::$app->user->isAdmin())
        {
            
        }
        elseif(\Yii::$app->user->isManager())
        {
            $query->where(['IN', "\"{$donationTableName}\".\"org_id\"", \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity())]);
        }
        elseif(\Yii::$app->user->isDonor())
        {
            $query->where("\"{$donationTableName}\".\"donor_id\"=:donor_id", [':donor_id' => \Yii::$app->user->getId()]);
        }
        $query->innerJoin($timeTableName, "\"{$timeTableName}\".\"id\"=\"{$donationTableName}\".\"time_id\"");
        $query->innerJoin($donorTableName, "\"{$donorTableName}\".\"id\"=\"{$donationTableName}\".\"donor_id\"");
        $query->orderBy(["{$timeTableName}.date_time" =>SORT_ASC]);
        $filterModel = new Donation(['scenario' => 'search']);
        $filterModel->setAttributes($filter);
        $query = self::addFilter($filterModel, $query);
        $query = self::addOrder($query, $orderField);
        $countQuery = clone $query;
        $data['total'] = $countQuery->count();
        $query->limit($limit);
        $data['limit'] = $limit;
        if($offset > 0)
        {
            $query->offset($offset);
            $data['offset'] = $offset;
        }
        else
        {
            $data['offset'] = 0;
        }
        $models = $query->with(['time', 'donor', 'org'])->all();
        $rows = [];
        foreach($models as $donation)
        {
            $rows[] = [
                'id' => $donation->id,
                'org_id' => $donation->org->id,
                'donor_id' => $donation->donor_id,
                'time_id' => $donation->time_id,
                'type' => $donation->event_id,
                'event_id' => $donation->event_id,
                'organisation' => $donation->org->name,
                'date_time' => strtotime($donation->time->date_time),
                'date_time_string' => $donation->time->date_time,
                'donor_name' => $donation->donor->getFullName(),
                'donor_rhesus' => $donation->donor->rhesus,
                'donor_group' => $donation->donor->group
            ];
        }
        $data['donations'] = $rows;
        $data['result'] = true;
        return $data;
    }
    
    /**
     * добавление в запрос $query фильтрации по данным из $filterModel
     * @param Donation $filterModel
     * @param \yii\db\ActiveQuery $query
     * @return \yii\db\ActiveQuery
     */
    public static function addFilter($filterModel, $query)
    {
	$donationTableName = Donation::getTableSchema()->fullName;
        $donorTableName = Donor::getTableSchema()->fullName;
        if($filterModel->id > 0)
        {
            $query->andWhere(["\"{$donationTableName}\".\"id\"" => (int)$filterModel->id]);
        }
        if($filterModel->org_id > 0)
        {
            $query->andWhere(["\"{$donationTableName}\".\"org_id\"" => (int)$filterModel->org_id]);
        }
        if($filterModel->time_id > 0)
        {
            $query->andWhere(["\"{$donationTableName}\".\"time_id\"" => (int)$filterModel->time_id]);
        }
        if($filterModel->event_id > 0)
        {
            $query->andWhere(["\"{$donationTableName}\".\"event_id\"" => (int)$filterModel->event_id]);
        }
        if($filterModel->donor_id > 0)
        {
            $query->andWhere(["\"{$donationTableName}\".\"donor_id\"" => (int)$filterModel->donor_id]);
        }
        if(strtotime($filterModel->filterTime) > 0)
        {
            $timeIds = Time::find()
                    ->select(['id'])
                    ->where(['date("date_time")' => date('Y-m-d', strtotime($filterModel->filterTime))])
                    ->column();
            $query->andWhere(['IN', "\"{$donationTableName}\".\"time_id\"", $timeIds]);
        }
        if(trim($filterModel->filterName))
        {
            $names = explode(' ', trim($filterModel->filterName));
            foreach($names as $key => &$name)
            {
                $name = trim($name);
                if(strlen(trim($name)) === 0)
                {
                    unset($names[$key]);
                }
            }
            $nameFilter = ['AND'];
            foreach($names as $name)
            {
                $nameFilter[] = ['OR', 
                    ['LIKE', "\"{$donorTableName}\".\"name\"", $name],
                    ['LIKE', "\"{$donorTableName}\".\"surname\"", $name],
                    ['LIKE', "\"{$donorTableName}\".\"patronymic\"", $name],
                ];
            }
            $query->andWhere($nameFilter);
        }
        return $query;
    }
    
    /**
     * добавление сортировки
     * @param \app\modules\donor\models\DonorQuery $query
     * @param string $field
     * @return \app\modules\donor\models\DonorQuery
     */
    public static function addOrder($query, $field)
    {
        if(!$field)
        {
            return $query;
        }
        $dir = SORT_ASC;
        if(strpos($field, '-') === 0)
        {
            $dir = SORT_DESC;
            $field = substr($field, 1);
        }
        if(!in_array($field, array_keys(Donation::getTableSchema()->columns)))
        {
            return $query;
        }
        $query->orderBy([Donation::tableName().'.'.$field => $dir]);
        return $query;
    }
    
    /**
     * список организаций для фильтра
     * @return array
     */
    public static function getOrgsForFilter()
    {
        $donationTable = Donation::getTableSchema()->fullName;
        $orgTable = \app\modules\org\models\Orgs::getTableSchema()->fullName;
        $query = \app\modules\org\models\Orgs::find()->select(["{$orgTable}.id", "{$orgTable}.name"]);
        if(\Yii::$app->user->isAdmin())
        {
            $rows = $query->asArray()->all();
            return \yii\helpers\ArrayHelper::map($rows, 'id', 'name');
        }
        elseif(\Yii::$app->user->isManager())
        {
            $rows = $query
                    ->innerJoin($donationTable, "\"{$donationTable}\".\"org_id\"=\"{$orgTable}\".\"id\"")
                    ->where(['IN', "\"{$donationTable}\".\"org_id\"", \Yii::$app->user->getOrgIDs()])
                    ->asArray()
                    ->all();
            return \yii\helpers\ArrayHelper::map($rows, 'id', 'name');
        }
        elseif(\Yii::$app->user->isDonor())
        {
            $rows = $query
                    ->innerJoin($donationTable, "\"{$donationTable}\".\"org_id\"=\"{$orgTable}\".\"id\"")
                    ->where(["\"{$donationTable}\".\"donor_id\"" => \Yii::$app->user->getId()])
                    ->asArray()
                    ->all();
            return \yii\helpers\ArrayHelper::map($rows, 'id', 'name');
        }
        return [];
    }

    public static function getDonorsForDay($day){

        $data = array();
        $donationTableName = Donation::getTableSchema()->fullName;
        $timeTableName = Time::getTableSchema()->fullName;
        $query = Donation::find();
        if(\Yii::$app->user->isAdmin())
        {

        }
        elseif(\Yii::$app->user->isManager())
        {
            $query->where(['IN', "\"{$donationTableName}\".\"org_id\"", \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity())]);
        }
        elseif(\Yii::$app->user->isDonor())
        {
            return 0;
        }
        $query->innerJoin($timeTableName, "\"{$timeTableName}\".\"id\"=\"{$donationTableName}\".\"time_id\"");
        $query->orderBy(["{$timeTableName}.date_time" =>SORT_ASC]);
        $models = $query->with(['time', 'donor', 'org'])->all();
        $rows = [];

        foreach($models as $donation)
        {
            $rows[] = [
                'donor_name' => $donation->donor->getFullName(),
                'donor_group' => $donation->donor->group,
                'donor_rhesus' => intval($donation->donor->rhesus),
                'date_time' => date('d.m.Y H:i' ,strtotime($donation->time->date_time)),
            ];
        }

        foreach ($rows as $donation){
            if (substr($donation['date_time'], 0, 10) == $day){
                $data[] = $donation;
            }
        }

        return $data;
    }
    
    /**
     * список доноров для фильтра
     * @return array
     */
    public static function getDonorsForFilter()
    {
        static $list;
        if(is_array($list))
        {
            return $list;
        }
        $list = array();
        $identity = \Yii::$app->user->getIdentity();
        if(!$identity)
        {
            return [];
        }
        $roleList = array_keys($identity->getRoleList());
        $query = Donor::find();
        $donorTable = Donor::getTableSchema()->fullName;
        $donationTable = Donation::getTableSchema()->fullName;
        if(\Yii::$app->user->isAdmin())
        {
        }
        elseif(\Yii::$app->user->isManager())
        {
            $query->andWhere(['IN', "{$donationTable}.org_id", \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity())]);
        }
        else
        {
            $query->andWhere(["{$donationTable}.donor_id" => \Yii::$app->user->getId()]);
        }
        $query->select([$donorTable.'.id', $donorTable.'.surname', $donorTable.'.name', $donorTable.'.patronymic']);
        $query->innerJoin($donationTable, "\"{$donationTable}\".\"donor_id\"=\"{$donorTable}\".\"id\"");
        $query->orderBy([
            "{$donorTable}.surname" => SORT_ASC,
            "{$donorTable}.name" => SORT_ASC,
            "{$donorTable}.patronymic" => SORT_ASC,
                    ]);
        $rows = $query->asArray()->all();
        foreach($rows as $row)
        {
            $list[$row['id']] = "{$row['surname']} {$row['name']} {$row['patronymic']}";
        }
        return $list;
    }

    // Написать функцию получения пользователей на определенный день
}