<?php
namespace app\modules\rest\components;
use conquer\oauth2\models\AccessToken;
use conquer\oauth2\models\RefreshToken;
use conquer\oauth2\BaseModel;
use conquer\oauth2\OAuth2IdentityInterface;
use Yii;
use yii\web\IdentityInterface;
/**
 * пример запроса
 * http://localhost/rest/auth/token
 * ?grant_type=vk&client_secret=a4966dmk63gk3eshg4ddnshhc35fwdgxrtkps34x
 * &client_id=1225
 * &email=w1121@yandex.ru
 * &vk_access_token=0c219ddfda66744c8bd2e5f85ac965965992ec7380bc89f7c9015569fef63cf91605c8fe6039c2aceda12
 */
class VkCredentials extends \conquer\oauth2\granttypes\ClientCredentials
{
    private $_user;

    /**
     * Value MUST be set to "password"
     * @var string
     */
    public $grant_type;

    /**
     * @var string
     */
    public $email;

    /**
     * @var string
     */
    public $vk_access_token;

    /**
     * Access Token Scope
     * @link https://tools.ietf.org/html/rfc6749#section-3.3
     * @var string
     */
    public $scope;

    /**
     * @var string
     */
    public $client_id;

    /**
     * @var string
     */
    public $client_secret;
    
    public function rules() {
        return [
            [['grant_type', 'client_id', 'email', 'vk_access_token'], 'required'],
            ['grant_type', 'required', 'requiredValue' => 'vk'],
            [['client_id'], 'string', 'max' => 80],
            [['client_id'], 'validateClientId'],
            [['client_secret'], 'validateClientSecret'],
            [['scope'], 'validateScope'],
            ['vk_access_token', 'validateVk']
        ];
    }
    
    /**
     * валидация access_token от вк и создание пользователя в системе при необходимости
     * @param string $attribute
     * @return type
     */
    public function validateVk($attribute)
    {
        $vkClient = \Yii::$app->authClientCollection->getClients()['vkontakte'];
        $tokenObj = new \yii\authclient\OAuthToken(['tokenParamKey' => 'access_token']);
        $tokenObj->setParam('access_token', $this->vk_access_token);
        $tokenObj->setParam('email', $this->email);
        $vkClient->setAccessToken($tokenObj);
        $result = $vkClient->reInitUserAttributes();
        if($result !== TRUE)
        {
            $this->addError($attribute, 'Error VK access token');
            return;
        }
        $account = \dektrium\user\models\Account::find()
                    ->where([
                        'provider' => $vkClient->getId(),
                        'client_id' => $vkClient->getUserAttributes()['id']])
                    ->one();
        if(!$account)
        {
            \dektrium\user\models\Account::create($vkClient);
            $account = \dektrium\user\models\Account::find()
                    ->where([
                        'provider' => $vkClient->getId(),
                        'client_id' => $vkClient->getUserAttributes()['id']])
                    ->one();
        }
        if ($account && $account->user instanceof \dektrium\user\models\User) 
        {
            if ($account->user->isBlocked) 
            {
                $this->addError($attribute, 'User is blocked');
                return;
            }
        }
        else
        {
            $this->addError($attribute, 'Account create error');
            return;
        }
        $this->_user = $account->user;
    }
    
    public function getResponseData()
    {
        /** @var IdentityInterface $identity */
        $identity = $this->getUser();

        $accessToken = AccessToken::createAccessToken([
            'client_id' => $this->client_id,
            'user_id' => $identity->getId(),
            'expires' => $this->accessTokenLifetime + time(),
            'scope' => $this->scope,
        ]);

        $refreshToken = RefreshToken::createRefreshToken([
            'client_id' => $this->client_id,
            'user_id' => $identity->getId(),
            'expires' => $this->refreshTokenLifetime + time(),
            'scope' => $this->scope,
        ]);

        return [
            'access_token' => $accessToken->access_token,
            'expires_in' => $this->accessTokenLifetime,
            'token_type' => $this->tokenType,
            'scope' => $this->scope,
            'refresh_token' => $refreshToken->refresh_token,
        ];
    }

    /**
     * Finds user by [[email]]
     * @return IdentityInterface|null
     * @throws \yii\base\InvalidConfigException
     * @throws \conquer\oauth2\Exception
     */
    protected function getUser()
    {
        return $this->_user;
    }
}

