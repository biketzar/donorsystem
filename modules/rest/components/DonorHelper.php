<?php
namespace app\modules\rest\components;
use \app\modules\donor\models\Donor;
/**
 * 
 */
class DonorHelper extends \yii\base\Component
{
    /**
     * донор создан/обновлён, есть поле id с ИД донора
     */
    CONST CODE_SUCCESS = 1;
    /**
     * донор не создан/обновлён, есть поле errors с ошибками, которые надо исправить
     */
    CONST CODE_HAS_ERROR = 2;
    /**
     * невозможно создать/обновить донора - не найден пользователь с указанным id
     */
    CONST CODE_USER_NOT_FOUND = 3;
    /**
     * нет доступа к редактированию данных пользователя с указанным id
     */
    CONST CODE_NOT_ALLOWED_EDIT = 4;
    /**
     * донор с указаынным id не найден
     */
    CONST CODE_DONOR_NOT_FOUND = 5;
    /**
     * донор с указаынным id найден
     */
    CONST CODE_DONOR_FOUND = 6;
    /**
     * нет доступа к просмтору данных донора
     */
    CONST CODE_NOT_ALLOWED_VIEW = 7;
    /**
     * добавление/удалении данных о доноре
     * @param array $data
     * @param string|int $donorId
     * @return array
     */
    public function update($data, $donorId)
    {
        $user = \app\models\User::find()->where(['id' => (int)$donorId])->limit(1)->one();
        if($user)
        {
            if(!self::canUpdate($user->id))
            {
                return [
                    'result' => false,
                    'msg' => 'Нет доступа',
                    'code' => self::CODE_NOT_ALLOWED_EDIT];
            }
            $donor = \app\modules\donor\models\Donor::find()->where(['id' => $user->id])->limit(1)->one();
            if($donor)
            {
                $donor->setScenario('update');
            }
            else
            {
                $donor = new \app\modules\donor\models\Donor(['scenario' => 'insert']);
            }
            $donor->setAttributes($data, true);
            $donor->id = $user->id;
            if($donor->save())
            {
                return [
                    'result' => true,
                    'id' => $donor->id,
                    'msg' => 'Данные успешно обновлены',
                    'code' => self::CODE_SUCCESS
                ];
            }
            else
            {
                return [
                    'result' => false,
                    'msg' => 'Необходимо исправить следующие ошибки',
                    'errors' => $donor->getErrors(),
                    'code' => self::CODE_HAS_ERROR
                ];
            }
        }
        else
        {
            return [
                'result' => false,
                'msg' => 'Пользователь c "'.$donorId.'" не найден',
                'code' => self::CODE_USER_NOT_FOUND
                ];
        }
    }
    
    /**
     * может ли текущий пользователь редактировать данные донора с указанным $id
     * только пользователь с ролью admin может управлять всеми пользователями, 
     * остальные пользователи могут редактировать только донора, связанного с текущим пользователем
     * @param string|integer $id
     * @return boolean
     */
    public static function canUpdate($id)
    {
        if(!\Yii::$app->user->isGuest 
                && in_array('admin', array_keys(\Yii::$app->user->getIdentity()->getRoleList())))
        {
            return true;
        }
        return $id == \Yii::$app->user->getId();
    }
    
    /**
     * может ли текущий пользователь получить данные донора с указанным $id
     * только пользователь с ролью admin или manager может видеть всех пользователей, 
     * остальные пользователи могут видеть только донора, связанного с текущим пользователем
     * @param string|integer $id
     * @return boolean
     */
    public static function canView($id)
    {
        if(!\Yii::$app->user->isGuest 
                && (in_array('admin', array_keys(\Yii::$app->user->getIdentity()->getRoleList()))
                || in_array('manager', array_keys(\Yii::$app->user->getIdentity()->getRoleList()))))
        {
            return true;
        }
        return $id == \Yii::$app->user->getId();
    }

    public static function canViewExtensionSetting($id) {
        if(!\Yii::$app->user->isGuest && (in_array('admin', array_keys(\Yii::$app->user->getIdentity()->getRoleList())) || in_array('manager', array_keys(\Yii::$app->user->getIdentity()->getRoleList())))) {
            return true;
        }
    }
    
    /**
     * получение данных о доноре по id
     * @param string|int $id
     * @return array
     */
    public function view($id)
    {
        if(self::canView($id))
        {
            /* @var $donor \app\modules\donor\models\Donor */
            $donor = \app\modules\donor\models\Donor::find()->where(['id' => $id])->limit(1)->one();
            if(!$donor)
            {
               return [
                    'result' => false,
                    'msg' => 'Донор не найден',
                    'code' => self::CODE_DONOR_NOT_FOUND
                ]; 
            }
            $data =  [
                'result' => true,
                'code' => self::CODE_DONOR_FOUND
            ];
            $data['name'] = $donor->name;
            $data['surname'] = $donor->surname;
            $data['patronymic'] = $donor->patronymic;
            $data['group'] = $donor->group;
            $data['rhesus'] = $donor->rhesus;
            $data['id'] = $donor->id;
            $data['dob'] = $donor->dob;
            return $data;
        }
        return [
                'result' => false,
                'msg' => 'Нет доступа',
                'code' => self::CODE_NOT_ALLOWED_VIEW
            ];
    }
    
    /**
     * добавление в запрос $query фильтрации по данным из $filterModel
     * @param \app\modules\donor\models\Donor $filterModel
     * @param \app\modules\donor\models\DonorQuery $query
     * @return \app\modules\donor\models\DonorQuery
     */
    public static function addFiter($filterModel, $query)
    {
        if($filterModel->id > 0)
        {
            $query->andWhere(['id' => (int)$filterModel->id]);
        }
        if(in_array((int)$filterModel->group, Donor::getGroupList()))
        {
            $query->andWhere(['group' => (int)$filterModel->id]);
        }
        if($filterModel->rhesus === '0' || $filterModel->rhesus === '1')
        {
            $query->andWhere(['rhesus' => (bool)$filterModel->rhesus]);
        }
        if($filterModel->name)
        {
            $query->andWhere(['LIKE', 'name', $filterModel->name]);
        }
        if($filterModel->surname)
        {
            $query->andWhere(['LIKE', 'surname', $filterModel->surname]);
        }
        if($filterModel->patronymic)
        {
            $query->andWhere(['LIKE', 'patronymic', $filterModel->patronymic]);
        }
        return $query;
    }
    
    /**
     * добавление сортировки
     * @param \app\modules\donor\models\DonorQuery $query
     * @param string $field
     * @return \app\modules\donor\models\DonorQuery
     */
    public static function addOrder($query, $field)
    {
        if(!$field)
        {
            return $query;
        }
        $dir = SORT_ASC;
        if(strpos($field, '-') === 0)
        {
            $dir = SORT_DESC;
            $field = substr($field, 1);
        }
        if(!in_array($field, array_keys(Donor::getTableSchema()->columns)))
        {
            return $query;
        }
        $query->orderBy([$field => $dir]);
        return $query;
    }
    
    /**
     * разрешено ли текушему пользователю менять настройки донора
     * @param type $donorId
     * @return type
     */
    public function canSaveSettings($donorId)
    {
        return \Yii::$app->user->isManager() || \Yii::$app->user->isAdmin();
    }
    
    /**
     * сохранение дополнительных настроек для донора
     * @param integer $donorId
     * @param array $settings
     * @return array
     */
    public function saveSettings($donorId, $settings)
    {
        if(!$this->canSaveSettings($donorId))
        {
            return [
                    'result' => false,
                    'msg' => 'Нет доступа',
                    'code' => self::CODE_NOT_ALLOWED_EDIT];
        }
        $donor = \app\modules\donor\models\Donor::find()->where(['id' => (int)$donorId])->limit(1)->one();
        if(!$donor)
        {
            return [
                    'result' => false,
                    'msg' => 'Донор не найден',
                    'code' => self::CODE_DONOR_NOT_FOUND
                ];
        }
        $donor->extraSettings = $settings;
        if($donor->saveExtraSettings())
        {
            return  [
                'result' => true,
                'msg' => 'Настройки удачно сохранены',
                'code' => self::CODE_SUCCESS
            ];
        }
        return [
                    'result' => false,
                    'msg' => 'Не удалось сохранить настройки',
                    'code' => self::CODE_HAS_ERROR,
                    'errors' => []
                ];
    }
    
    public function canGetSettings($donorId)
    {
        return ((int)$donorId === (int)\Yii::$app->user->getId()) || $this->canSaveSettings($donorId);
    }
    
    /**
     * получение настроек пользователя
     * @param integer $donorId
     * @return array
     */
    public function getSettings($donorId)
    {
        if(!$this->canGetSettings($donorId))
        {
            return [
                    'result' => false,
                    'msg' => 'Нет доступа',
                    'code' => self::CODE_NOT_ALLOWED_EDIT];
        }
        $donor = \app\modules\donor\models\Donor::find()->where(['id' => (int)$donorId])->limit(1)->one();
        if(!$donor)
        {
            return [
                    'result' => false,
                    'msg' => 'Донор не найден',
                    'code' => self::CODE_DONOR_NOT_FOUND
                ];
        }
        $settings = \app\modules\donor\models\ExtensionSettings::find()
                ->where(['id_donor' => $donorId])
                ->asArray()
                ->all();
        return [
                'result' => true,
                'code' => self::CODE_SUCCESS,
                'settings' => $settings
            ];
    }
}