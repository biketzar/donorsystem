<?php
namespace app\modules\rest\components;
/**
 * 
 */
class StaffHelper extends \yii\base\Component
{
    CONST MAX_LIMIT = 100;
    /**
     * нет доступа
     */
    CONST CODE_NOT_ALLOW = 1;
    /**
     * успешно сохранено
     */
    CONST CODE_SUCCESS = 2;
    /**
     * ошибка при сохранении
     */
    CONST CODE_ERROR = 3;
    
    public function getList($filter, $orderField, $limit = 0, $offset = 0)
    {
        $data = array(
            'staff' => []
        );
        $query = \app\modules\org\models\Staff::find();
        $limit = (($limit > 0 && $limit < self::MAX_LIMIT) ? (int)$limit : self::MAX_LIMIT);
        $query->limit($limit);
        $filterModel = new \app\modules\org\models\Staff(['scenario' => 'search']);
        $filterModel->setAttributes($filter);
        $query = self::addFiter($filterModel, $query);
        $data['limit'] = $limit;
        if($offset > 0)
        {
            $query->offset($offset);
            $data['offset'] = $offset;
        }
        else
        {
            $data['offset'] = 0;
        }
        $models = $query->all();
        foreach ($models as $model)
        {
            $staff = $model->attributes;
            $data['staff'][] = $staff;
        }
        return $data;
    }
    
    /**
     * 
     * @param \app\modules\org\models\Staff $filterModel
     * @param \yii\db\ActiveQuery $query
     * @return \yii\db\ActiveQuery
     */
    public static function addFiter($filterModel, $query)
    {
        if($filterModel->user_id > 0)
        {
            $query->andWhere(['user_id' => (int)$filterModel->user_id]);
        }
        if($filterModel->org_id > 0)
        {
            $query->andWhere(['org_id' => (int)$filterModel->org_id]);
        }
        return $query;
    }
    
    public function create($data)
    {
        if(!self::canCreate())
        {
            return [
                'result' => false,
                'msg' => 'Нет доступа',
                'code' => self::CODE_NOT_ALLOW];
        }
        $model = \app\modules\org\models\Staff::find()
                ->where(['user_id' => $data['user_id'], 'org_id' => $data['org_id']])
                ->limit(1)->one();
        if(!$model)
        {
            $model = new \app\modules\org\models\Staff();
        }
        $model->user_id = (int)$data['user_id'];
        $model->org_id = (int)$data['org_id'];
        if($model->save())
        {
            return [
                    'result' => true,
                    'id' => $model->id,
                    'msg' => 'Успешно сохранено',
                    'code' => self::CODE_SUCCESS
                ];
        }
        return [
                    'result' => false,
                    'msg' => 'Ошибка при сохранении',
                    'code' => self::CODE_ERROR
                ];
        
    }
    
    public static function canCreate()
    {
        return \Yii::$app->user->isAdmin();
    }
    
    public function delete($data)
    {
        if(!self::canCreate())
        {
            return [
                'result' => false,
                'msg' => 'Нет доступа',
                'code' => self::CODE_NOT_ALLOW];
        }
        $model = \app\modules\org\models\Staff::find()
                ->where(['user_id' => $data['user_id'], 'org_id' => $data['org_id']])
                ->limit(1)->one();
        if($model && $model->delete())
        {
            return [
                    'result' => true,
                    'msg' => 'Успешно сохранено',
                    'code' => self::CODE_SUCCESS
                ];
        }
        return [
                    'result' => false,
                    'msg' => 'Ошибка при сохранении',
                    'code' => self::CODE_ERROR
                ];
        
    }
}