<?php
namespace app\modules\rest\components;
use app\modules\notifications\models\Notification;
/**
 * 
 */
class NotificationHelper extends \yii\base\Component
{
    /**
     * успешное добавление уведомления
     */
    CONST CODE_SUCCESS = 1;
    /**
     * ошибка при добавлении уведомления
     */
    CONST CODE_ERROR = 2;
    /**
     * не найдено
     */
    CONST CODE_NOT_FOUND = 3;
    /**
     * нет доступа
     */
    CONST CODE_ACCESS_ERROR = 4;
    
    /**
     * добавление уведомления
     * @param string $title
     * @param string $text
     * @param int $userId
     * @return array
     */
    public function add($title, $text, $userId)
    {
        $notification = Notification::addNotification($title, $text, $userId);
        if($notification)
        {
            $this->sendMail($notification);
            if(is_string($title) && is_string($text))
            {
                \Yii::$app->appNotification->sendNotification($notification->user_id, $title, $text);
            }
            return [
                    'result' => true,
                    'msg' => 'Уведомление успешно добавлено',
                    'code' => self::CODE_SUCCESS,
                    ];
        }
        return [
            'result' => false,
            'msg' => 'Не удалось добавить уведомление',
            'code' => self::CODE_ERROR,
            ]; 
    }
    
    /**
     * отправка уведомлений на почту
     * @param Notification $notification
     * @return null
     */
    protected function sendMail($notification)
    {
        if(!$notification || !$notification->user || !$notification->user->email)
        {
            return;
        }
        $params = [
            'title' => $notification->title,
            'text' => $notification->text
        ];
        \Yii::$app->mailer->compose('@app/mail/notifications/simple', $params)
                ->setFrom('no-reply@donorsystem.org')
                ->setTo(trim($notification->user->email))
                ->setSubject($notification->title)
                ->send();
    }
    
    /**
     * удаление уведомления
     * @param int $id
     * @return array
     */
    public function delete($id)
    {
        /* @var $model Notification */
        $model = Notification::find()->where(['id' => (int)$id])->limit(1)->one();
        if(!$model)
        {
            return [
                'result' => false,
                'msg' => 'Уведомление не найдено',
                'code' => self::CODE_NOT_FOUND,
            ]; 
        }
        if((int)$model->user_id !== (int)\Yii::$app->user->getId())
        {
            return [
                'result' => false,
                'msg' => 'Нет прав',
                'code' => self::CODE_ACCESS_ERROR,
            ]; 
        }
        if($model->delete())
        {
            return [
                'result' => false,
                'msg' => 'Успешно удалено',
                'code' => self::CODE_SUCCESS,
            ]; 
        }
        return [
            'result' => true,
            'msg' => 'Уведомление не удалено',
            'code' => self::CODE_ERROR,
            ];  
    }
    
    /**
     * установка статуса 'просмотрено' 
     * @param int $id
     * @return array
     */
    public function seen($id)
    {
        /* @var $model Notification */
        $model = Notification::find()->where(['id' => (int)$id])->limit(1)->one();
        if(!$model)
        {
            return [
                'result' => false,
                'msg' => 'Уведомление не найдено',
                'code' => self::CODE_NOT_FOUND,
            ]; 
        }
        if((int)$model->user_id !== (int)\Yii::$app->user->getId())
        {
            return [
                'result' => false,
                'msg' => 'Нет прав',
                'code' => self::CODE_ACCESS_ERROR,
            ]; 
        }
        Notification::setSeen($id);
        return [
                'result' => true,
                'msg' => 'Статус уведомление изменён',
                'code' => self::CODE_SUCCESS,
            ]; 
    }
    
    /**
     * получение информации об уведомлении
     * @param int $id
     * @return array
     */
    public function view($id)
    {
        /* @var $model Notification */
        $model = Notification::find()->where(['id' => (int)$id])->limit(1)->one();
        if(!$model)
        {
            return [
                'result' => false,
                'msg' => 'Уведомление не найдено',
                'code' => self::CODE_NOT_FOUND,
            ]; 
        }
        if((int)$model->user_id !== (int)\Yii::$app->user->getId())
        {
            return [
                'result' => false,
                'msg' => 'Нет прав',
                'code' => self::CODE_ACCESS_ERROR,
            ]; 
        }
        Notification::setSeen($id);
        return [
                'result' => true,
                'id' => $model->id, 
                'code' => self::CODE_SUCCESS,
                'title' => $model->title,
                'text' => $model->text,
                'status' => $model->status,
                'add_date' => $model->add_date
            ]; 
    }
    
    /**
     * список уведомлений
     * @param array $filter
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function getListUser($filter, $limit = 0, $offset = 0)
    {
        $query = Notification::find()
                ->andWhere(['user_id' => \Yii::$app->user->getId()]);
        $query->orderBy(['id' => SORT_DESC]);
        if(array_key_exists('status', $filter))
        {
            $query->andWhere(['status' => $filter['status']]);
        }
        if($limit > 0)
        {
            if($limit > 100)
            {
                $limit = 100;
            }
            $query->limit($limit);
        }
        if($offset > 0)
        {
            $query->offset($offset);
        }
        return [
                'result' => true,
                'notifications' => $query->asArray()->all()
            ];
    }
    
    /**
     * может ли пользователь отправлять уведомления
     * @return boolean
     */
    public static function canSendNotification()
    {
        return \Yii::$app->user->isManager() || \Yii::$app->user->isAdmin();
    }
    
    public static function getNotSeenCount()
    {
        $count = Notification::find()
                ->where(['status' => Notification::STATUS_NEW])
                ->andWhere(['user_id' => \Yii::$app->user->getId()])
                ->count();
        return $count > 0 ? "({$count})" : '';
    }
}