<?php
namespace app\modules\rest\components;
/**
 * 
 */
class UserHelper extends \yii\base\Component
{
    /**
     * пользователь создан
     */
    CONST CODE_SUCCESS = 1;
    /**
     * пользователь не создан, есть поле errors с ошибками, которые надо исправить
     */
    CONST CODE_HAS_ERROR = 2;
    /**
     * 
     * @param array $data
     * @return array
     */
    public function createUser($data)
    {
        $user = new \app\models\User(['scenario' => 'register']);
        $user->setAttributes($data, true);
        if($user->save())
        {
            return [
                'result' => true,
                'id' => $user->id,
                'msg' => 'Данные успешно обновлены',
                'code' => self::CODE_SUCCESS
                ];
        }
        return [
                'result' => false,
                'msg' => 'Необходимо исправить следующие ошибки',
                'errors' => $user->getErrors(),
                'code' => self::CODE_HAS_ERROR
                ];
    }
}