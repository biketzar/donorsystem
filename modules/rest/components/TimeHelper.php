<?php
namespace app\modules\rest\components;
use app\modules\time\models\Time;

class TimeHelper extends \yii\base\Component
{
    /**
     * время создано/обновлено, есть поле id с идентификатором времени
     */
    CONST CODE_SUCCESS = 1;
    /**
     * время не создано/обновлено, есть поле errors с ошибками, которые надо исправить
     */
    CONST CODE_HAS_ERROR = 2;
    /**
     * время не найдено
     */
    CONST CODE_NOT_FOUND = 3;
    /**
     * время найдено
     */
    CONST CODE_FOUND = 4;
    /**
     * нет доступа
     */
    CONST CODE_NOT_ALLOW = 5;
    /**
     * Время удалено
     */
    CONST CODE_DELETED = 6;
    /**
     * Невозможно удалить время
     */
    CONST CODE_NOT_DELETED = 7;
    /**
     * количество максимально выдаваемых строк не может быть больше этого значения
     */
    CONST MAX_LIMIT = 1000;
    
    /**
     * добавление времени
     * @param array $data
     * @return array
     */
    public function create($data)
    {
        $model = new Time(['scenario' => 'insert']);
        unset($data['id']);
        return $this->save($data, $model);
    }
    
    /**
     * валидация данных
     * @param array $data
     * @param string $scenario
     * @return Time
     */
    public function validate($data, $scenario)
    {
        $model = new Time(['scenario' => $scenario]);
        $model->setAttributes($data, true);
        if($data['date_time'] > 0)
        {
            $model->date_time = date('Y-m-d H:i:00', $data['date_time']);
        }
        $model->validate();
        return $model;
    }
    
    /**
     * удаление времени
     * @param array $data
     * @return array
     */
    public function update($data)
    {
        if(!self::canUpdate((int)$data['id']))
        {
            return [
                'result' => false,
                'msg' => 'Нет доступа',
                'code' => self::CODE_NOT_ALLOW];
        }
        $model = Time::find()
                ->where(['id' => (int)$data['id']])->limit(1)->one();
        if(!$model)
        {
            return [
                    'result' => false,
                    'msg' => 'Время не найдено или нет доступа',
                    'code' => self::CODE_NOT_FOUND
                ]; 
        }
        $model->setScenario('update');
        return $this->save($data, $model);
    }
    
    /**
     * сохранение данных о времени
     * @param array $data
     * @param app\modules\time\models\Time $model
     * @return array
     */
    protected function save($data, $model)
    {
        $data['date_time'] = date('Y-m-d H:i:00', $data['date_time']);
        $model->setAttributes($data, true);
        if($model->save())
        {
            return [
                    'result' => true,
                    'id' => $model->id,
                    'msg' => 'Время успешно сохранено',
                    'code' => self::CODE_SUCCESS
                ];
        }
        else
        {
            return [
                    'result' => false,
                    'msg' => 'Необходимо исправить следующие ошибки',
                    'errors' => $model->getErrors(),
                    'code' => self::CODE_HAS_ERROR
                ];
        }
    }
    
    /**
     * получение данных о времени
     * @param string|int $id
     * @return array
     */
    public function view($id)
    {
        if(!self::canView($id))
        {
            return [
                'result' => false,
                'msg' => 'Нет доступа',
                'code' => self::CODE_NOT_ALLOW];
        }
        $model = Time::find()->where(['id' => $id])->limit(1)->one();
        if(!$model)
        {
           return [
                'result' => false,
                'msg' => 'Время не найдено',
                'code' => self::CODE_NOT_FOUND
            ]; 
        }
        $data =  [
            'result' => true,
            'code' => self::CODE_FOUND
        ];
        $data['org_id'] = $model->org_id;
        $data['date_time'] = strtotime($model->date_time);
        $data['date_time_string'] = $model->date_time;
        $data['published'] = $model->published;
        $data['type'] = $model->type;        
        $data['id'] = $model->id;
        $data['reg_stop_hours'] = $model->reg_stop_hours;
        return $data;
    }
    
    /**
     * удаление времени
     * @param string|int $id
     * @return array
     */
    public function delete($id)
    {
        if(!self::canDelete((int)$id))
        {
            return [
                'result' => false,
                'msg' => 'Нет доступа',
                'code' => self::CODE_NOT_ALLOW];
        }
        $model = Time::find()
                ->where(['id' => (int)$id])->limit(1)->one();
        if(!$model)
        {
            return [
                    'result' => false,
                    'msg' => 'Время не найдено или нет доступа',
                    'code' => self::CODE_NOT_FOUND
                ]; 
        }
        if($model->delete())
        {
            return [
                    'result' => true,
                    'msg' => 'Время удалено',
                    'code' => self::CODE_DELETED
                ]; 
        }
        else
        {
            return [
                    'result' => true,
                    'msg' => 'Невозможно удалить время',
                    'code' => self::CODE_NOT_DELETED
                ]; 
        }
    }
    
    /**
     * 
     * @param \yii\db\ActiveQuery $query
     * @param \app\modules\donor\models\Donor $donor
     * @return \yii\db\ActiveQuery
     */
    public static function addExtendedFilter($query, $donor)
    {
        if(\Yii::$app->user->isDonor()
                && !\Yii::$app->user->isAdmin()
                && !\Yii::$app->user->isManager())
        {
            $settings = \app\modules\donor\models\ExtensionSettings::find()
                    ->where(['id_settings' => \app\modules\donor\models\ExtensionSettings::ENABLE_BLOOD_COMPONENT_DONATION,
                        'id_donor' => $donor->id,
                        'val' => '1'
                        ])->asArray()->all();
            if(count($settings))
            {//цельняя кровь и компоненты
                $condition = ['OR', ['IN', 'type', [NULL, 0, 1]]];
                foreach($settings as $oneSettings)
                {
                    $condition[] = ['type' => 2, 'org_id' => $oneSettings['id_org']];
                }
                $query->andWhere($condition);
            }
            else
            {//только цельняя кровь
                $query->andWhere(['IN', 'type', [NULL, 0, 1]]);
            }
        }
        return $query;
    }
    
    /**
     * отфильтрованный список со временем
     * @param array $filter
     * @param string $orderField например 'id' - для сортировки по возрастанию, '-id' - для сортировки по убыванию
     * @param string|int $limit
     * @param string|int $offset
     */
    public function getList($filter, $orderField, $limit = 0, $offset = 0)
    {
        $data = array();
        $limit = (($limit > 0 && $limit < self::MAX_LIMIT) ? (int)$limit : self::MAX_LIMIT);
        $query = Time::find();
        if(\Yii::$app->user->isManager()
                && !\Yii::$app->user->isAdmin()
                && !\Yii::$app->user->isDonor())
        {
            $query->where(['IN', 'org_id', \app\modules\time\components\AccessHelper::allowOrgsByUser(\Yii::$app->user->getIdentity())]);
        }
        $donor = \app\modules\donor\models\Donor::find()
                ->where(['id' => \Yii::$app->user->getId()])->limit(1)->one();
        if($donor)
        {
            $query = self::addExtendedFilter($query, $donor);
        }
        if(\Yii::$app->user->isDonor()
                && !\Yii::$app->user->isAdmin()
                && !\Yii::$app->user->isManager())
        {
            
            if($donor && $donor->group)
            {
                $query->andWhere(['IN', 'group', [NULL, 0, $donor->group]]);
            }
            $query->andWhere('id NOT IN (SELECT time_id FROM "Donation" WHERE event_id=:exclude_status)', [':exclude_status' => \app\modules\donation\components\StatusHelper::STATUS_READY_TO_DONATE]);
            $query->andWhere(['published' => true]);
        }
        $query->orderBy('date_time');
        $filterModel = new Time(['scenario' => 'search']);
        $filterModel->setAttributes($filter);
        $query = self::addFilter($filterModel, $query);
        $query = self::addOrder($query, $orderField);
        $countQuery = clone $query;
        $data['total'] = $countQuery->count();
        $query->limit($limit);
        $data['limit'] = $limit;
        if($offset > 0)
        {
            $query->offset($offset);
            $data['offset'] = $offset;
        }
        else
        {
            $data['offset'] = 0;
        }
        $rows = $query->asArray()->all();
        foreach($rows as &$row)
        {
            $row['date_time_string'] = $row['date_time'];
            $row['date_time'] = strtotime($row['date_time']);
        }
        $data['time'] = $rows;
        $data['result'] = true;
        return $data;
    }
    
    /**
     * добавление в запрос $query фильтрации по данным из $filterModel
     * @param \app\modules\time\models\Time $filterModel
     * @param \yii\db\ActiveQuery $query
     * @return \yii\db\ActiveQuery
     */
    public static function addFilter($filterModel, $query)
    {
        if($filterModel->id > 0)
        {
            $query->andWhere(['id' => (int)$filterModel->id]);
        }
        if($filterModel->org_id > 0)
        {
            $query->andWhere(['org_id' => (int)$filterModel->org_id]);
        }
        if($filterModel->published === TRUE 
                || $filterModel->published === FALSE
                || $filterModel->published === 0
                || $filterModel->published === 1
                || $filterModel->published === '0'
                || $filterModel->published === '1'
                )
        {
            
            $query->andWhere(['published' => (bool)$filterModel->published]);
        }
        if($filterModel->type > 0 || $filterModel->type === '0')
        {
            $query->andWhere(['type' => (int)$filterModel->type]);
        }
        if($filterModel->group >= 1 && $filterModel->group <= 4)
        {
            $query->andWhere(['group' => (int)$filterModel->group]);
        }
        if($filterModel->filterTime && ($filterTime = strtotime($filterModel->filterTime)))
        {
            $query->andWhere("date_time>=:from_date_time AND date_time<=:to_date_time", [
                ':from_date_time' => date("Y-m-d 00:00:00", $filterTime),
                ':to_date_time' => date("Y-m-d 23:59:59", $filterTime),
                ]);
        }
        if($filterModel->date_time > 0)
        {
            $query->andWhere('date_time >= :date_time_from AND date_time <= :date_time_to', [
                ':date_time_from' => date('Y-m-d 00:00:00', $filterModel->date_time),
                ':date_time_to' => date('Y-m-d 23:59:59', $filterModel->date_time),
                ]);
        }
        if((int)$filterModel->onlyActual === 1)
        {
            $query->andWhere('date_time>:date_time', [":date_time" => date('Y-m-d H:i:s')]);
        }
        return $query;
    }
    
    /**
     * добавление сортировки
     * @param \app\modules\donor\models\DonorQuery $query
     * @param string $field
     * @return \app\modules\donor\models\DonorQuery
     */
    public static function addOrder($query, $field)
    {
        if(!$field)
        {
            return $query;
        }
        $dir = SORT_ASC;
        if(strpos($field, '-') === 0)
        {
            $dir = SORT_DESC;
            $field = substr($field, 1);
        }
        if(!in_array($field, array_keys(Time::getTableSchema()->columns)))
        {
            return $query;
        }
        $query->orderBy([$field => $dir]);
        return $query;
    }
    
    /**
     * может ли текущий пользователь редактировать время
     * @param string|int $id
     * @return boolean
     */
    public static function canUpdate($id)
    {
        $identity = \Yii::$app->user->getIdentity();
        if(!\Yii::$app->user->isGuest 
                && in_array('admin', array_keys($identity->getRoleList())))
        {
            return true;
        }
        if(!\Yii::$app->user->isGuest 
                && (in_array('manager', array_keys(\Yii::$app->user->getIdentity()->getRoleList()))))
        {
            $orgId = Time::find()->select(['org_id'])->where(['id' => $id])->limit(1)->scalar();
            return in_array($orgId, \app\modules\time\components\AccessHelper::allowOrgsByUser($identity));
        }
        return false;
    }
    
    /**
     * может ли текущий пользователь удалить время
     * @param string|int $id
     * @return boolean
     */
    public static function canDelete($id)
    {
        return self::canUpdate($id);
    }
    
    /**
     * может ли текущий пользователь просматривать время
     * @param string|int $id
     * @return boolean
     */
    public static function canView($id)
    {
        return true;
    }
}