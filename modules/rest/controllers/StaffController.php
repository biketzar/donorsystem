<?php
namespace app\modules\rest\controllers;
use yii\helpers\ArrayHelper;

class StaffController extends \app\modules\rest\components\RestController
{
    public function actionIndex() {
        $helper = new \app\modules\rest\components\StaffHelper();
        return $helper->getList([], null, \Yii::$app->request->get('limit', 0), \Yii::$app->request->get('offset', 0));
    }
    
    public function actionCreate()
    {
        $helper = new \app\modules\rest\components\StaffHelper();
        $data = \Yii::$app->request->post();
        return $helper->create($data);
    }
    
    public function actionView()
    {
        
    }
    
    public function actionDelete()
    {
        $helper = new \app\modules\rest\components\StaffHelper();
        $data = \Yii::$app->request->post();
        return $helper->create($data);
    }
}