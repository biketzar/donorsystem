<?php
namespace app\modules\rest\controllers;
use yii\helpers\ArrayHelper;
class DonorController extends \app\modules\rest\components\RestController
{
    public function actionUpdate()
    {
        $helper = new \app\modules\rest\components\DonorHelper();
        $data = \Yii::$app->request->post();
        return $helper->update(\Yii::$app->request->post(), ArrayHelper::getValue($data, 'id', \Yii::$app->user->getId()));
    }
    
    public function actionView()
    {
        $helper = new \app\modules\rest\components\DonorHelper();
        $data = \Yii::$app->request->get();
        return $helper->view(ArrayHelper::getValue($data, 'id', \Yii::$app->user->getId()));
    }
    
    public function actionSettings()
    {
        $helper = new \app\modules\rest\components\DonorHelper();
        if(\Yii::$app->request->isPost)
        {
            $settings = \Yii::$app->request->post('settings');
            $donorId = \Yii::$app->request->post('donor_id');
            return $helper->saveSettings($donorId, $settings);
        }
        return $helper->getSettings(\Yii::$app->request->get('id'));
    }
}

