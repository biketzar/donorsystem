<?php
namespace app\modules\rest\controllers;

use \yii\rest\Controller;

class UserController extends Controller
{
    public function actionCreate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $helper = new \app\modules\rest\components\UserHelper();
        return $helper->createUser(\Yii::$app->request->post());
    }
    
    public function actionLoginToken()
  {
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    $accessToken = \Yii::$app->request->get('accessToken');
    $email = mb_strtolower(trim(\Yii::$app->request->get('email')), \Yii::$app->charset);
    if(!$accessToken)
    {
        return [
            "result" => FALSE,
            "error" => "Empty token",
            "code" => \app\components\ErrorCode::EMPTY_TOKEN
        ];
    }
    if(!$email)
    {
        return [
            "result" => FALSE,
            "error" => "Empty email",
            "code" => \app\components\ErrorCode::EMPTY_EMAIL
        ];
    }
    /* @var $vkClient \app\modules\rest\components\VKontakte */
    $vkClient = \Yii::$app->authClientCollection->getClients()['vkontakte'];
    $tokenObj = new \yii\authclient\OAuthToken(['tokenParamKey' => 'access_token']);
    $tokenObj->setParam('access_token', $accessToken);
    $tokenObj->setParam('email', $email);
    $vkClient->setAccessToken($tokenObj);
    $result = $vkClient->reInitUserAttributes();
    if($result !== TRUE)
    {
        return [
            'result' => FALSE,
            'error' => $result,
            'code' => \app\components\ErrorCode::VK_AUTH_ERROR
        ];
    }
    $account = \dektrium\user\models\Account::find()
                ->where([
                    'provider' => $vkClient->getId(),
                    'client_id' => $vkClient->getUserAttributes()['id']])
                ->one();
    if(!$account)
    {
        \dektrium\user\models\Account::create($vkClient);
        $account = \dektrium\user\models\Account::find()
                ->where([
                    'provider' => $vkClient->getId(),
                    'client_id' => $vkClient->getUserAttributes()['id']])
                ->one();
    }
    if ($account && $account->user instanceof \dektrium\user\models\User) {
        if (!$account->user->isBlocked) {
            if(\Yii::$app->user->login($account->user))
            {
                return [
                    'result' => TRUE
                ];
            }
            else
            {
                return [
                    'result' => FALSE,
                    'error' => 'Login error',
                    'code' => \app\components\ErrorCode::LOGIN_ERROR
                ];
            }
        }
    }
    else
    {
        return [
            'result' => FALSE,
            'error' => 'User not created',
            'code' => \app\components\ErrorCode::USER_CREATE_ERROR
        ];
    }
  }
}
