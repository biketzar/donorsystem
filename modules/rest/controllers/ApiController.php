<?php
namespace app\modules\rest\controllers;
use app\components\Notification;
use app\models\User;

/**
 * контроллер с общими методами API
 */
class ApiController extends \app\modules\rest\components\RestController
{
    public function actionSendTest() {
        $notification = new Notification(\Yii::$app->user->getId());
        $notification->test();

        return [
            'result' => true
        ];
    }

    public function actionDeviceToken() {
        $iosToken = \Yii::$app->request->get('iosToken', \Yii::$app->request->post('iosToken'));
        $androidToken = \Yii::$app->request->get('android_token', \Yii::$app->request->post('android_token'));
        $user = User::findOne(['id' => (int) \Yii::$app->user->getId()]);

        if ($user) {
            if ($iosToken) {
                $user->ios_token = $iosToken;
            }

            if ($androidToken) {
                $user->android_token = $androidToken;
            }

            if ($user->save()) {
                return [
                    'result' => true
                ];
            }
        }

        return [
            'result' => false
        ];
    }
}
