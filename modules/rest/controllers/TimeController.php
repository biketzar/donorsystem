<?php
namespace app\modules\rest\controllers;

use \app\modules\rest\components\RestController;

class TimeController extends RestController {

    public function actionIndex() 
    {
        $helper = new \app\modules\rest\components\TimeHelper();
        $request = \Yii::$app->request;
        $filter = $request->get('filter', $request->post('filter'));
        if(!is_array($filter))
        {
            $filter = [];
        }
		$orgId = $request->get('id', $request->post('id'));
        if($orgId)
        {
            $filter = [
                'org_id' => (int)$orgId
            ];
        }
        $orderField = $request->get('sort');
        $limit = $request->get('limit');
        $offset = $request->get('offset');
        return $helper->getList($filter, $orderField, $limit, $offset);
    }

    public function actionView() {
        $helper = new \app\modules\rest\components\TimeHelper();
        return $helper->view(\Yii::$app->request->get('id'));
    }
    
    public function actionTypes()
    {
        $data = ['result' => TRUE];
        $data['dictionary'] = \app\modules\time\models\Time::getTypeList();
        return $data;
    }
    
    public function actionCreate()
    {
        $helper = new \app\modules\rest\components\TimeHelper();
        $data = \Yii::$app->request->post();
        return $helper->create($data);
    }
    
    public function actionUpdate()
    {
        $helper = new \app\modules\rest\components\TimeHelper();
        $data = \Yii::$app->request->post();
        return $helper->update($data);
    }
    
    public function actionDelete()
    {
        $helper = new \app\modules\rest\components\TimeHelper();
        return $helper->delete(\Yii::$app->request->get('id'));
    }
}

