<?php
namespace app\modules\rest\controllers;

use app\modules\rest\components\NotificationHelper;
use \app\modules\rest\components\RestController;

class NotificationController extends RestController {
    public function actionIndex() {
        $helper = new NotificationHelper();
        $request = \Yii::$app->request;
        $filter = $request->get('filter', $request->post('filter', []));
        if(!is_array($filter))
        {
            $filter = [];
        }
        $limit = $request->get('limit', $request->post('limit'));
        $offset = $request->get('offset', $request->post('offset'));
        return $helper->getListUser($filter, $limit, $offset);
    }
    
    public function actionView()
    {
        $helper = new NotificationHelper();
        $request = \Yii::$app->request;
        return $helper->view($request->get('filter', $request->post('id', 0)));
    }
    
    public function actionDelete()
    {
        $helper = new NotificationHelper();
        $request = \Yii::$app->request;
        return $helper->delete($request->get('filter', $request->post('id', 0)));
    }
    
    public function actionSeen()
    {
        $helper = new NotificationHelper();
        $request = \Yii::$app->request;
        return $helper->seen($request->get('filter', $request->post('id', 0)));
    }
    
    public function actionAdd()
    {
        $helper = new NotificationHelper();
        $request = \Yii::$app->request;
        return $helper->add($request->post('title'), $request->post('text'), $request->post('user_id'));
    }
}