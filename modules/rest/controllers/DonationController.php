<?php
namespace app\modules\rest\controllers;

use app\modules\rest\components\DonationHelper;
use \app\modules\rest\components\RestController;

class DonationController extends RestController {

    public function actionIndex() {
        $helper = new DonationHelper();
        $request = \Yii::$app->request;
        $filter = $request->get('filter', $request->post('filter', []));
        if(!is_array($filter))
        {
            $filter = [];
        }
        $orderField = $request->get('sort', $request->post('sort'));
        $limit = $request->get('limit', $request->post('limit'));
        $offset = $request->get('offset', $request->post('offset'));
        return $helper->getList($filter, $orderField, $limit, $offset);
    }

    public function actionEnroll() {
        $helper = new \app\modules\rest\components\DonationHelper();
        return $helper->enroll(\yii::$app->request->post());
    }
    
    public function actionDenial() {
        $helper = new \app\modules\rest\components\DonationHelper();
        return $helper->enroll(\yii::$app->request->post());
    }

    public function actionCancel() {
        $helper = new \app\modules\rest\components\DonationHelper();
        return $helper->cancel(\yii::$app->request->post());
    }
    
     public function actionComplete() {
        $helper = new \app\modules\rest\components\DonationHelper();
        return $helper->complete(\yii::$app->request->post());
    }
}

