<?php
namespace app\modules\rest\controllers;

use app\models\LoginForm;
/**
 * контроллер с методами авторизации
 */
class AuthController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            /** 
             * checks oauth2 credentions
             * and performs OAuth2 authorization, if user is logged on
             */
            'oauth2Auth' => [
                'class' => \conquer\oauth2\AuthorizeFilter::className(),
                'only' => ['index', 'json'],
            ],
            'rateLimiter' => [
                'class' => \app\components\RateLimiter::className(),
                'enableRateLimitHeaders' => FALSE,
                'user' => new \app\components\IpLimiter()
            ],
        ];
    }
    
    /**
     * обработка запросов на получение access_token
     * @return array
     */
    public function actions()
    {
        return [
            // returns access token
            'token' => [
                'class' => \conquer\oauth2\TokenAction::classname(),
                'grantTypes' => [
                    'authorization_code' => 'conquer\oauth2\granttypes\Authorization',
                    'refresh_token' => 'conquer\oauth2\granttypes\RefreshToken',
                    //'client_credentials' => 'conquer\oauth2\granttypes\ClientCredentials',
                    'vk' => 'app\modules\rest\components\VkCredentials',
                    'password' => 'conquer\oauth2\granttypes\UserCredentials',
                ]
            ],
        ];
    }
    /**
     * Display login form to authorize user
     */
    public function actionIndex()
    {
        throw new Exception('Метод недоступен');
        $model = new LoginForm();
        $model->load(\Yii::$app->request->get());
        if ($model->setAttributes(\Yii::$app->request->get()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }
    }
}