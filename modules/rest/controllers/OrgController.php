<?php
namespace app\modules\rest\controllers;
use yii\helpers\ArrayHelper;
class OrgController extends \app\modules\rest\components\RestController
{
    public function actionIndex() {
        $helper = new \app\modules\rest\components\OrgHelper();
        $request = \Yii::$app->request;
        $filter = $request->get('filter', $request->post('filter', []));
        if(!is_array($filter))
        {
            $filter = [];
        }
        $orderField = $request->get('sort', $request->post('sort'));
        $limit = $request->get('limit', $request->post('limit'));
        $offset = $request->get('offset', $request->post('offset'));
        return $helper->getList($filter, $orderField, $limit, $offset);
    }

    public function actionView() {
       $helper = new \app\modules\rest\components\OrgHelper();
       $data = \Yii::$app->request->get();
       return $helper->view(ArrayHelper::getValue($data, 'id'));
    }
    
    public function actionCreate()
    {
        $helper = new \app\modules\rest\components\OrgHelper();
        $data = \Yii::$app->request->post();
        return $helper->create($data);
    }
    
    public function actionUpdate()
    {
        $helper = new \app\modules\rest\components\OrgHelper();
        $data = \Yii::$app->request->post();
        return $helper->update($data);
    }
    
    public function actionDelete() {
       $helper = new \app\modules\rest\components\OrgHelper();
       $data = \Yii::$app->request->get();
       return $helper->delete(ArrayHelper::getValue($data, 'id'));
    }
}

