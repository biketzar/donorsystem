<?php
namespace app\modules\org;

class Module extends \yii\base\Module
{
  public $controllerNamespace = 'app\modules\org\controllers';
  
  public function init()
  {
      parent::init();
  }
}
