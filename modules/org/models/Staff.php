<?php
namespace app\modules\org\models;

/**
 * @property int $id
 * @property int $user_id
 * @property int $org_id
 * @property \app\modules\org\models\Orgs модель организации
 * @property \app\models\User модель пользователя
 */
class Staff extends \yii\db\ActiveRecord
{
    public static function tableName(){
        return 'Staff';
    }
    
    public function rules() {
        return [
            [['user_id', 'org_id'], 'safe', 'on' => 'search']
        ];
    }

    public function atributeLabels(){
        return [
            'id' => 'ID',
            'user_id' => 'Пользователь',
            'org_id' => 'Организация',
        ];
    }
    
    /**
     * связь с пользователем
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\app\models\User::className(), ['id' => 'user_id']);
    }
    
    public function getOrg()
    {
        return $this->hasOne(\app\modules\org\models\Orgs::className(), ['id' => 'org_id']);
    }
}