<?php
namespace app\modules\org\models;
/**
 * модель для работы с данными организации
 * доступные сценарии: insert, update
 * @property int $id
 * @property string $name Название организации
 * @property string $description Описание
 * @property point(double, double) $point 
 * @property int $status Статус
 * @property string $address Адрес
 * @property string $link Регистрационный номер
 * @property string $phone Номер телефона
 * @property Array $users модели сотрудников
 * @property integer $enable_donation_of_blood_components Доступно донорство компонентов крови
 * @property string $notification_emails Email для уведомлений
 */
class Orgs extends \yii\db\ActiveRecord
{
    CONST STATUS_ACTIVE = 1;
    
    public static function tableName(){
        return 'Organisation';
    }

    public function attributeLabels(){
        return [
            'id' => 'ID',
            'name' => 'Название организации',
            'description' => 'Описание',
            'point' => 'Координаты',
            'status' => 'Статус',
            'address' => 'Адрес',
            'link' => 'Ссылка',
            'phone' => 'Номер телефона',
            'enable_donation_of_blood_components' => 'Доступно донорство компонентов крови',
            'enable_stop_registration' => 'Доступно автоматическое закрытие регистрации',
            'exterrnal_form_id' => 'Идентификатор для внешней формы',
            'notification_emails' => 'Email для уведомлений'
        ];
    }

    public function rules(){
        return [
            [['name', 'description'], 'required', 'on' => ['insert', 'update']],
            [['description', 'link', 'phone', 'address'], 'string', 'on' => ['insert', 'update']],
            [['status', 'enable_donation_of_blood_components', 'enable_stop_registration'], 'integer', 'on' => ['insert', 'update']],
            [['point'], 'pointValidator', 'on' => ['insert', 'update']],
            [['exterrnal_form_id'], 'unique', 'on' => ['insert', 'update']],
            [['name', 'description'], 'purifier', 'on' => ['insert', 'update']],
            [['notification_emails'], 'string', 'max' => 255, 'on' => ['insert', 'update']],
            [['name', 'description', 'link', 'phone', 'address', 'id', 'status', 'enable_donation_of_blood_components', 'enable_stop_registration'], 'safe', 'on' => ['search']],
        ];
    }

    public function pointValidator($attribite, $params)
    {

    }
     
    /**
     * защита от xss
     * @param string $attribite
     * @param array $params
     */
    public function purifier($attribite, $params)
    {
        $this->{$attribite} = \yii\helpers\HtmlPurifier::process($this->{$attribite});
    }
    
    public static function pointToArray($str)
    {
        $arr = explode(',', trim($str, ' ()'));
        return array(
            (float) \yii\helpers\ArrayHelper::getValue($arr, 0, 0),
            (float) \yii\helpers\ArrayHelper::getValue($arr, 1, 0)
        );
    }
    
    public static function arrayToPoint($arr)
    {
        $x = (float) \yii\helpers\ArrayHelper::getValue($arr, 0, 0);
        $y = (float) \yii\helpers\ArrayHelper::getValue($arr, 1, 0);
        return "({$x}, {$y})";
    }
    
    public function beforeSave($insert) {
        $this->point = self::arrayToPoint($this->point);
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes) {
        $this->point = self::pointToArray($this->point);
        return parent::afterSave($insert, $changedAttributes);
    }
    
    public function afterFind() {
        $this->point = self::pointToArray($this->point);
        return parent::afterFind();
    }
    
    public function getUsers()
    {
        return $this->hasMany(\app\models\User::className(), ['id' => 'user_id'])
                ->viaTable(Staff::tableName(), ['org_id' => 'id']);
    }
    
    public function beforeDelete() {
        if(\app\modules\time\models\Time::find()->where(['org_id' => $this->id])->limit(1)->count() > 0)
        {
            return FALSE;
        }
        return parent::beforeDelete();
    }
    
    public static function getStatusList()
    {
        return [
            self::STATUS_ACTIVE => 'Активный'
        ];
    }
}