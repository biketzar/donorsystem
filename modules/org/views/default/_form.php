<?php
/* @var $this yii\web\View */
/* @var $model app\modules\org\models\Orgs */
use yii\helpers\Html;
$this->registerJsFile('//maps.googleapis.com/maps/api/js?key=AIzaSyB0UVyJGCyfHktevDxKHqFOPT50NsqzrPc');
if(\Yii::$app->session->hasFlash('org_update_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
        <?=\Yii::$app->session->getFlash('org_update_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('org_update_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
        <?=\Yii::$app->session->getFlash('org_update_error')?>
    </div>
    <?php
}
?>
<?=Html::beginForm('', 'POST', ['id' => 'org-form']);?>
<?=Html::errorSummary($model);?>
    <div class="form-group">
        <?=Html::activeLabel($model, 'name');?>
        <?=Html::activeTextInput($model, 'name', ['class' => 'form-control', 'required' => true])?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'description');?>
        <?=Html::activeTextArea($model, 'description', ['class' => 'form-control'])?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'status');?>
        <?=Html::activeDropDownList($model, 'status', app\modules\org\models\Orgs::getStatusList(), ['class' => 'form-control', 'required' => true])?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'address');?>
        <?=Html::activeTextInput($model, 'address',['class' => 'form-control']);?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'link');?>
        <?=Html::activeTextInput($model, 'link',['class' => 'form-control']);?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'phone');?>
        <?=Html::activeTextInput($model, 'phone',['class' => 'form-control']);?>
    </div>
    <div class="form-group">
        <?=Html::activeLabel($model, 'point');?>
        <div class="row">
            <div class="col-md-6">
                <Label for="latitude">Широта:</Label>
                <?=Html::textInput(Html::getInputName($model, 'point').'[0]', $model->point[0], ['id' => 'latitude', 'class' => 'latitude form-control']);?>
            </div>
            <div class="col-md-6">
                <Label for="longitude">Долгота:</Label>
                <?=Html::textInput(Html::getInputName($model, 'point').'[1]', $model->point[1], ['id' => 'longitude', 'class' => 'longitude form-control']);?>
            </div>
        </div>
        <div class="row pt-2 pb-2">
            <div class="col-md-12">
                <Label for="address-search">Поиск по адресу:</Label>
            </div>
            <div class="col-md-12">
                <?=Html::textInput('', '', ['class' => 'form-control', 'id' => 'address-search', 'style' => 'width:90%;float:left;']);?>
                <button type="button" class="btn btn-md" id="address-search-button">Поиск</button>
            </div>
        </div>
        <?= Html::tag('DIV', '', [
            'id' => 'coord-input', 
            'style' => "width:100%; height:300px;"
            ])?>
        <div class="form-group">
            <?=Html::activeLabel($model, 'enable_donation_of_blood_components');?>
            <?=Html::activeDropDownList($model, 'enable_donation_of_blood_components', [0 => 'Нет', 1 => 'Да'], ['class' => 'form-control', 'required' => true])?>
        </div>
        <div class="form-group">
            <?=Html::activeLabel($model, 'enable_stop_registration');?>
            <?=Html::activeDropDownList($model, 'enable_stop_registration', [0 => 'Нет', 1 => 'Да'], ['class' => 'form-control', 'required' => true])?>
        </div>
        <div class="form-group">
            <?=Html::activeLabel($model, 'exterrnal_form_id');?>
            <?=Html::activeTextInput($model, 'exterrnal_form_id',['class' => 'form-control']);?>
        </div>
        <div class="form-group">
            <?=Html::activeLabel($model, 'notification_emails');?>
            <?=Html::activeTextArea($model, 'notification_emails',['class' => 'form-control']);?>
        </div>
    </div>
    <div class="form-group text-right">
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </div>
<?=Html::endForm();?>

<script type="text/javascript">
    window.addEventListener('load', function(){
       var mapid = 'coord-input';
       var map;
       var marker = new Array();
       var latitude = $('#latitude').val();
       var longitude = $('#longitude').val();
       initialize();
       function initialize() {
            var mapOptions = {
              zoom: 8,
              center: new google.maps.LatLng(latitude, longitude),
              mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById(mapid),
                mapOptions);
            
            var myLatlng = new google.maps.LatLng(latitude, longitude);
            marker = new google.maps.Marker({
              position: myLatlng,
              map: map,
              draggable:true,

              title: 'Здесь!'
          });

               google.maps.event.addListener(marker, 'dragend', function(event){
                   var point = marker.getPosition();
                   $('#latitude').val(point.lat());
                   $('#longitude').val(point.lng());
               });
               
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    if(latitude == '' || longitude == '')
                    {
                        var p = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        $('#latitude').val(position.coords.latitude);
                        $('#longitude').val(position.coords.longitude);
                        map.setCenter(p);
                        marker.setPosition(p);
                    }
                }, function() {
                });
            }
          }
          
       var geocoder = new google.maps.Geocoder();
       $('#address-search-button').click(function(e){
           e.preventDefault();
           var address = $('#address-search').val();
           geocoder.geocode( { 'address': address}, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    map.setCenter(results[0].geometry.location);
                    marker.setPosition(results[0].geometry.location);
                    $('#latitude').val(results[0].geometry.location.lat());
                    $('#longitude').val(results[0].geometry.location.lng());
                } else {
                  alert('Geocode was not successful for the following reason: ' + status);
                }
              });
           
           return false;
       });
    });
</script>