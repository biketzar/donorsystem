<?php
/* @var $this yii\web\View */
/* @var $dataProvider \yii\data\ActiveDataProvider */
/* @var $filterModel app\modules\org\models\Orgs */
$this->title = 'Список организаций';
?>
    <h1>Список организаций</h1>
    <div class="text-right">
        <?php
        if(app\modules\rest\components\OrgHelper::canCreate())
        {
            ?>
            <a href="<?= yii\helpers\Url::to(['/org/default/create'])?>" class="btn btn-primary">
                <i class="fa fa-plus"></i> Добавить
            </a>
            <?php
        }
        ?>
    </div>
<?php

echo yii\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'columns' => [
        'id', 'name', 'description', 'status', 'phone', 'link', 'address',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '<div class="grid-action-buttons">{view} {update} {staff}</div>',
            'buttons' => [
                    'view' => function ($url, $model, $key){
                        if(!app\modules\rest\components\OrgHelper::canView()){
                            return ' ';
                        } else {
                            return \yii\helpers\Html::a('<i class="fa fa-eye"></i> ', $url, [
                                'class' => 'btn btn-info btn-sm',
                                'title' => 'Просмотр'
                            ]);
                        }
                    },
                'delete' => function ($url, $model, $key) {
                    if(!app\modules\rest\components\OrgHelper::canDelete())
                    {
                        return ' ';
                    }
                    return \yii\helpers\Html::a('<i class="fa fa-trash"></i> ', $url, [
                        'class' => 'btn btn-danger btn-sm',
                        'title' => 'Удалить'
                    ]);
                },
                'update' => function ($url, $model, $key) {
                    if(!app\modules\rest\components\OrgHelper::canUpdate($model->id))
                    {
                        return ' ';
                    }
                    return \yii\helpers\Html::a('<i class="fa fa-edit"></i> ', $url, [
                        'class' => 'btn btn-success btn-sm',
                        'title' => 'Редактировать'
                    ]);
                },
                'staff' => function ($url, $model, $key) {
                    if(!app\modules\rest\components\StaffHelper::canCreate())
                    {
                        return ' ';
                    }
                    return \yii\helpers\Html::a('<i class="fa fa-users"></i> ', $url, [
                        'class' => 'btn btn-dark btn-sm',
                        'title' => 'Сотрудники'
                    ]);
                },
            ]
        ],
    ]
]);

