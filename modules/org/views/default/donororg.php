<?php

$this->title = 'Пункты сдачи';
$this->registerJsFile('//developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js');
$this->registerJsFile('//maps.googleapis.com/maps/api/js?key=AIzaSyB0UVyJGCyfHktevDxKHqFOPT50NsqzrPc');

?>

<div class="site-orgs">

    <h1><?=$this->title?></h1>
    <p class="lead">Здесь вы можете выбрать организацию, в которой желаете сдать кровь</p>

    <div>
        <input type="text" id="listbox-input" placeholder="Поиск" class="form-control">

        <div class="mt-3" id="org-cards"">
                <?php
                foreach($orgList as $org)
                {
                    ?>
                    <div class="card org-card" id="<?=$org['name']?>">
                        <div class="card-header">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?=$org['id']?>" aria-expanded="true" aria-controls="collapse<?=$org['id']?>">
                                    <span class="h5"><?=$org['name']?></span>
                                </button>
                            </h5>
                        </div>
                        <div id="collapse<?=$org['id']?>"class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <h6 class="card-subtitle mb-2 text-muted">
                                    <?=$org['address'];?> <br />
                                    <span><?=$org['phone'];?></span>
                                </h6>
                                <p class="card-text"><p class="lead"><?=$org['description']?></p></p>
                                <!--<a href="#" class="btn btn-default btn-success float-right">Узнать больше</a>-->
                                <a href="/donation/default/enroll?org=<?=$org['id']?>" class="btn btn-danger float-right">Записаться</a>
                                <a href= "<?=$org['link']?>" target="_blank" class="btn btn-success mr-1 float-right">Узнать больше</a>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
        </div>
    </div>
    <div class="map-container mt-3">
        <h3 class="">Организации на карте</h3>
        <div id="map" style="width: 100%;height:350px;"></div>
    </div>
</div>

<script type="text/javascript">
    window.addEventListener('load', function(){
        $('#listbox-input').keyup(function(e){
            search(this);
        });
        function search(input) {
            input = input.value.toUpperCase();
            console.log(input);

            var main = document.getElementById('org-cards');
            var orgs = main.getElementsByClassName('org-card');
            for (var i =0; i < orgs.length; i++){
                console.log(orgs[i].id.toUpperCase());
                if (orgs[i].id.toUpperCase().indexOf(input) > -1) {
                    orgs[i].style.display = "";
                } else {
                    orgs[i].style.display = "none";
                }
            }
        }
    });
</script>



<script type="text/javascript">
    window.addEventListener('load', function(){
        var map;
        var orgList = <?= \yii\helpers\Json::encode(is_array($orgList) ? $orgList : []);?>;
        var infowindows = [];
        initialize();
        function initialize() {
            var mapOptions = {
                zoom: 10,
                center: new google.maps.LatLng(60.007743, 30.373234),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById('map'), mapOptions);

            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var p = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                    map.setCenter(p);
                }, function() {
                });
            }
            var markers = orgList.map(function(org, i) {
                var infowindow = new google.maps.InfoWindow({
                    content: org.name
                });
                infowindows.push(infowindow);
                var marker = new google.maps.Marker({
                    position: {lat: org.point[0], lng: org.point[1]},
                });
                marker.addListener('click', function() {
                    infowindows.forEach(function(iw){
                        iw.close()
                    });
                    infowindow.open(map, marker);
                });
                return marker;
            });
            var markerCluster = new MarkerClusterer(map, markers,{imagePath: '//developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
        }
    });
</script>

