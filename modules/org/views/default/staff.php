<?php
/* @var $this yii\web\View */
/* @var $items Array */
use yii\helpers\Html;
$this->title = 'Сотрудники организации';
if(\Yii::$app->session->hasFlash('staff_update_success'))
{
    ?>
    <div class="alert alert-success" role="alert">
        <?=\Yii::$app->session->getFlash('staff_update_success')?>
    </div>
    <?php
}
if(\Yii::$app->session->hasFlash('staff_update_error'))
{
    ?>
    <div class="alert alert-danger" role="alert">
        <?=\Yii::$app->session->getFlash('staff_update_error')?>
    </div>
    <?php
}
$this->registerCssFile('@web2/select2/css/select2.min.css');
$this->registerjsFile('@web2/select2/js/select2.min.js', ['depends' => 'yii\web\YiiAsset']);
?>
<?=Html::beginForm('', 'POST', ['id' => 'org-form']);?>
<div class="form-group">
    <?=Html::label('Сотрудники');?>
    <?=Html::dropDownList('users', $users, $allUsers, ['id' => 'org-staff-select', 'class' => 'form-control', 'multiple' => true])?>
</div>
<div class="form-group text-right">
    <button type="submit" class="btn btn-primary">Сохранить</button>
</div>
<?=Html::endForm();?>
<script type="text/javascript">
document.addEventListener("DOMContentLoaded", function(event) {
    $('#org-staff-select').select2();
  });
</script>

