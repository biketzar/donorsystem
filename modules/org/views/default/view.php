<?php
/* @var $this yii\web\View */
/* @var $model app\modules\org\models\Orgs */

$this->title = $model->name;
use yii\helpers\Html;
?>

<h1>Организация <?=$model->name?></h1>
<?php

echo \yii\widgets\DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id', 'name', 'description', 'address', 'link', 'phone', 'status', 
        'enable_donation_of_blood_components' => [
            'attribute' => 'enable_donation_of_blood_components',
            'value' => (string)$model->enable_donation_of_blood_components === '0' ? 'Нет' : 'Да'
        ],
        'enable_stop_registration' => [
            'attribute' => 'enable_stop_registration',
            'value' => (string)$model->enable_stop_registration === '0' ? 'Нет' : 'Да'
        ],
        'point' => [
            'attribute' => 'point',
            'value' => $model->point[0] . ', ' . $model->point[1]
        ]
    ]]);

?>
<div class="text-right">
    <?php
    if(app\modules\rest\components\OrgHelper::canUpdate($model->id))
    {
        ?>
        <a href="<?= yii\helpers\Url::to(['/org/default/update', 'id' => $model->id]);?>" class="btn btn-md btn-success">
            <i class="fa fa-edit"></i> Редактировать
        </a>    
        <?php
    }
    ?>
    <?=    app\modules\rest\components\OrgHelper::canDelete() ?  Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
            'confirm' => 'Вы уверены, что хотите удалить организацию?',
            'method' => 'post',
        ],
    ]) : '' ?>
</div>

