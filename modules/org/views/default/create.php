<?php
/* @var $this yii\web\View */
/* @var $model app\modules\org\models\Orgs */


$this->title = 'Добавление организации';
?>
<h1><?=$this->title;?></h1>
<?=$this->render('_form', ['model' => $model]);?>