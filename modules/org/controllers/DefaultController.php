<?php
namespace app\modules\org\controllers;

use app\modules\rest\components\OrgHelper;
use yii\helpers\ArrayHelper;
use Yii;
use app\modules\rest\components\DonorHelper;

class DefaultController extends \yii\web\Controller{

    //public $layout = '/sidebar';

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                //'only' => ['update', 'view', 'admin'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['update', 'view', 'index'],
                        'roles' => ['admin', 'manager'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'delete', 'staff'],
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['donororg'],
                        'roles' => ['@']
                    ]
                ],
            ],
            [
                'class' => \app\modules\donation\components\AccessControl::className(),
                //'only' => ['update', 'view', 'admin'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['update', 'view', 'index', 'create', 'delete', 'staff'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['donororg'],
                        'roles' => ['donor', 'admin', 'manager']
                    ]
                ],
            ],
        ];
    }

    public function actionView(){
        $id = (int)\Yii::$app->request->get('id');
        if(!OrgHelper::canView())
        {
            throw new \yii\web\HttpException(403, 'Доступ запрещён');
        }
        $helper = new \app\modules\rest\components\OrgHelper();
        $model = new \app\modules\org\models\Orgs(['scenario' => 'update']);
        $data = $helper->view($id);

        if(!$data['result'])
        {
            throw new \yii\web\HttpException(404, $data['msg']);
        }

        $model->setAttributes($data, false);
        return $this->render('view', ['model' => $model]);
    }

    public function actionCreate(){
        //$id = (int)\Yii::$app->request->get('id', 1);
        if(!OrgHelper::canCreate())
        {
            throw new \yii\web\HttpException(403, 'Доступ запрещён');
        }
        $helper = new OrgHelper();
        $model = new \app\modules\org\models\Orgs(['scenario' => 'insert']);
        if(Yii::$app->request->isPost)
        {
            $postData = ArrayHelper::getValue(Yii::$app->request->post(), $model->formName());
            $model->load(Yii::$app->request->post());
            $createResult = $helper->create($postData);
            if($createResult['result'])
            {
                \Yii::$app->session->setFlash('org_update_success', $createResult['msg']);
                return $this->redirect(['/org/default/view', 'id' => $createResult['id']]);
            }
            elseif($createResult['code'] != OrgHelper::CODE_HAS_ERROR)
            {
                \Yii::$app->session->setFlash('org_update_error', $createResult['msg']);
            }
            if($createResult['code'] == OrgHelper::CODE_HAS_ERROR)
            {
                $model->addErrors($createResult['errors']);
            }
        }
        return $this->render('create', ['model' => $model]);
    }

    public function actionDelete(){
        $id = (int) Yii::$app->request->get('id', -1);
        $helper = new \app\modules\rest\components\OrgHelper();
        if(!$helper->canUpdate($id))
        {
            throw new \yii\web\HttpException(403, 'Доступ запрещён');
        }
        $model = new \app\modules\org\models\Orgs(['scenario' => 'delete']);
        $data = $helper->delete($id);

        if($data['code'] == OrgHelper::CODE_NOT_FOUND){
           echo 'Пожалуйста, укажите верную организацию!';
        }
        else{
            return $this->render('delete', ['model' => $model]);
        }

    }


    public function actionUpdate(){

        $id = (int)\Yii::$app->request->get('id');
        $helper = new OrgHelper();
        if(!$helper->canUpdate($id))
        {
            throw new \yii\web\HttpException(403, 'Доступ запрещён');
        }
        $model = new \app\modules\org\models\Orgs(['scenario' => 'update']);
        $data = $helper->view($id);

        if(!$data['result'] && $data['code'] != OrgHelper::CODE_NOT_FOUND)
        {
            throw new \yii\web\HttpException(404, $data['msg']);
        }

        $model->setAttributes($data, false);

        if(Yii::$app->request->isPost)
        {
            $postData = ArrayHelper::getValue(Yii::$app->request->post(), $model->formName());
            $model->load(Yii::$app->request->post());

            $postData += array('id' => $model['id']);

            $updateResult = $helper->update($postData);

            if($updateResult['result'])
            {
                \Yii::$app->session->setFlash('org_update_success', $updateResult['msg']);
            }
            elseif($updateResult['code'] != OrgHelper::CODE_HAS_ERROR)
            {
                \Yii::$app->session->setFlash('org_update_error', $updateResult['msg']);
            }
            if($updateResult['code'] == OrgHelper::CODE_HAS_ERROR)
            {
                $model->addErrors($updateResult['errors']);
            }
        }
        return $this->render('update', [
            'model' => $model
        ]);
    }

    public function actionIndex(){
        $query = \app\modules\org\models\Orgs::find();
        /*if(!in_array('admin', array_keys(\Yii::$app->user->getIdentity()->getRoleList()))
            && !in_array('manager', array_keys(\Yii::$app->user->getIdentity()->getRoleList())))
        {
            $query->andWhere(['id' => \Yii::$app->user->getId()]);
        }*/

        $filterModel = new \app\modules\org\models\Orgs(['scenario' => 'search']);
        $filterModel->load(\Yii::$app->request->get());

        $query = OrgHelper::addFilter($filterModel, $query);
        $query = OrgHelper::addOrder($query, \Yii::$app->request->get('sort'));
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'filterModel' => $filterModel
        ]);
    }

    public function actionDonororg(){
        $helper = new \app\modules\rest\components\OrgHelper();
        $orgList = $helper->getList([], []);
        return $this->render('donororg', ['orgList' => $orgList['organizations']]);
    }

    public function actionStaff()
    {
        $helper = new \app\modules\rest\components\StaffHelper();
        $org = \app\modules\org\models\Orgs::find()
                ->where(['id' => \Yii::$app->request->get('id')])
                ->limit(1)->one();
        if(!$org)
        {
            throw new \yii\web\HttpException(404);
        }
        if(\Yii::$app->request->isPost)
        {
            \app\modules\org\models\Staff::deleteAll(['org_id' => $org->id]);
            foreach(\Yii::$app->request->post('users', []) as $userId)
            {
                $helper->create(['user_id' => $userId, 'org_id' => $org->id]);
            }
        }
        $staffList = $helper->getList(['org_id' => $org->id], []);
        $allUsers = [];
        $rows =  \app\models\User::find()
                ->select(['id', 'username'])
                ->where(['IN', 'id', \Yii::$app->authManager->getUserIdsByRole('manager')])
                ->asArray()
                ->all();
        foreach($rows as $row)
        {
            $allUsers[$row['id']] = "{$row['username']} ({$row['id']})";
        }
        $users = ArrayHelper::map($staffList['staff'], 'user_id', 'user_id');
        return $this->render('staff', ['users' => $users, 'allUsers' => $allUsers]);
    }
}

