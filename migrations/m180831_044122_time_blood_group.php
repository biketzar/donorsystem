<?php

use yii\db\Migration;

/**
 * Class m180831_044122_time_blood_group
 */
class m180831_044122_time_blood_group extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(app\modules\time\models\Time::tableName(), 'group', \yii\db\pgsql\Schema::TYPE_INTEGER . ' NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180831_044122_time_blood_group cannot be reverted.\n";
        return false;
    }
}
