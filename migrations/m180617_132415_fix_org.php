<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180617_132415_fix_org
 */
class m180617_132415_fix_org extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('Organisation', 'description', Schema::TYPE_TEXT);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180617_132415_fix_org cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180617_132415_fix_org cannot be reverted.\n";

        return false;
    }
    */
}
