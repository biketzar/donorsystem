<?php

use yii\db\pgsql\Schema;
use yii\db\Migration;

/**
 * Class m190112_080730_site_notifications
 */
class m190112_080730_site_notifications extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('Notifications', [
            'id' => 'SERIAL PRIMARY KEY',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'creator_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status' => Schema::TYPE_SMALLINT . ' NULL',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'text' => Schema::TYPE_TEXT,
            'add_date' => Schema::TYPE_DATETIME. ' NOT NULL'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190112_080730_site_notifications cannot be reverted.\n";

        return false;
    }
}
