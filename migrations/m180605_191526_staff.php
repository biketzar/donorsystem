<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180605_191526_staff
 */
class m180605_191526_staff extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('Staff', [
            'id' => 'SERIAL PRIMARY KEY',
            'user_id' => Schema::TYPE_INTEGER . '(128) NOT NULL',
            'org_id' => Schema::TYPE_INTEGER . '(256) NOT NULL'
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('Staff');
    }
}
