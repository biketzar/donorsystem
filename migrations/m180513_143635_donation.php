<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180513_143635_donation
 */
class m180513_143635_donation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('Donation', [
            'id' => 'SERIAL PRIMARY KEY',
            'org_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'donor_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'time_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'event_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0 '
        ]);
        $this->createTable('Donation_event_log', [
            'id' => 'SERIAL PRIMARY KEY',
            'donation_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status_from' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status_to' => Schema::TYPE_INTEGER . ' NOT NULL',
            'reason' => Schema::TYPE_TEXT . ' NULL ',
            'date_time' => Schema::TYPE_DATETIME . ' NOT NULL default CURRENT_TIMESTAMP',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('Donation');
        $this->dropTable('Donation_event_log');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180513_143635_donation cannot be reverted.\n";

        return false;
    }
    */
}
