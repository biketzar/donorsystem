<?php

use yii\db\Migration;

/**
 * Class m181114_182742_external_form
 */
class m181114_182742_external_form extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(
                app\modules\org\models\Orgs::tableName(), 
                'exterrnal_form_id',
                yii\db\pgsql\Schema::TYPE_STRING." NULL");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181114_182742_external_form cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181114_182742_external_form cannot be reverted.\n";

        return false;
    }
    */
}
