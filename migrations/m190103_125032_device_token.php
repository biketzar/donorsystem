<?php

use yii\db\Migration;

/**
 * Class m190103_125032_device_token
 */
class m190103_125032_device_token extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\models\User::tableName(),
            'ios_token',
            'character varying(64) DEFAULT NULL');

        $this->addColumn(\app\models\User::tableName(),
            'android_token',
            'character varying(256) DEFAULT NULL');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190103_125032_device_token cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190103_125032_device_token cannot be reverted.\n";

        return false;
    }
    */
}
