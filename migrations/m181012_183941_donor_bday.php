<?php

use yii\db\Migration;

/**
 * Class m181012_183941_donor_bday
 */
class m181012_183941_donor_bday extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(\app\modules\donor\models\Donor::tableName(), 'dob', \yii\db\pgsql\Schema::TYPE_DATE);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181012_183941_donor_bday cannot be reverted.\n";
        return false;
    }
}
