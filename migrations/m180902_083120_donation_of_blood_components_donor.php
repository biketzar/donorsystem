<?php

use yii\db\Migration;
use yii\db\pgsql\Schema;

/**
 * Class m180902_083120_donation_of_blood_components_donor
 */
class m180902_083120_donation_of_blood_components_donor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('Extension_settings_donor_org', [
            'id' => 'SERIAL PRIMARY KEY',
            'id_org' => yii\db\pgsql\Schema::TYPE_INTEGER . ' NOT NULL',
            'id_donor' => Schema::TYPE_INTEGER . ' NOT NULL',
            'id_settings' => Schema::TYPE_INTEGER . ' NOT NULL',
            'val' => Schema::TYPE_STRING . '(256) NOT NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180902_083120_donation_of_blood_components_donor cannot be reverted.\n";

        return false;
    }
}
