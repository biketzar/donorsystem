<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180529_181433_org
 */
class m180529_181433_org extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('Organisation', [
            'id' => 'SERIAL PRIMARY KEY',
            'name' => Schema::TYPE_STRING . '(128) NOT NULL',
            'description' => Schema::TYPE_STRING . '(256) NULL',
            'status' => Schema::TYPE_INTEGER . ' NULL',
            'address' => Schema::TYPE_STRING . '(256) NULL',
            'link' => Schema::TYPE_STRING . '(128) NULL',
            'phone' => Schema::TYPE_STRING . '(32) NULL',
            'point' => 'point NULL',
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('Organisation');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180529_181433_org cannot be reverted.\n";

        return false;
    }
    */
}
