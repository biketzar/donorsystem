<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180503_120039_donor
 */
class m180503_120039_donor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('Donor', [
            'id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'group' => Schema::TYPE_INTEGER . ' NULL',
            'resus' => Schema::TYPE_BOOLEAN . '(128) NULL',
            'name' => Schema::TYPE_STRING . '(128) NULL',
            'surname' => Schema::TYPE_STRING . '(128) NULL',
            'patronymic' => Schema::TYPE_STRING . ' NULL'
        ]);
        $this->addPrimaryKey('donor_id_pk', 'Donor', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
         $this->dropTable('Donor');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180503_120039_donor cannot be reverted.\n";

        return false;
    }
    */
}
