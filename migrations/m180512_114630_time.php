<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m180512_114630_time
 */
class m180512_114630_time extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('Time', [
            'id' => 'SERIAL PRIMARY KEY',
            'org_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'date_time' => Schema::TYPE_DATETIME . ' NOT NULL',
            'published' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT true',
            'type' => Schema::TYPE_INTEGER . ' DEFAULT 0 '
        ]);
        //$this->addPrimaryKey('time_id_pk', 'Time', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('Time');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180512_114630_time cannot be reverted.\n";

        return false;
    }
    */
}
