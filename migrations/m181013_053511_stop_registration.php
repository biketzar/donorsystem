<?php

use yii\db\Migration;

/**
 * Class m181013_053511_stop_registration
 */
class m181013_053511_stop_registration extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(app\modules\org\models\Orgs::tableName(), 
                'enable_stop_registration', 
                yii\db\pgsql\Schema::TYPE_SMALLINT.' DEFAULT 0');
        $this->addColumn(\app\modules\time\models\Time::tableName(), 
                'reg_stop_hours', 
                yii\db\pgsql\Schema::TYPE_INTEGER.' DEFAULT 0');
        $this->addColumn(\app\modules\time\models\TimeTemplateItem::tableName(), 
                'reg_stop_hours', 
                yii\db\pgsql\Schema::TYPE_INTEGER.' DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m181013_053511_stop_registration cannot be reverted.\n";

        return false;
    }
}
