<?php

use yii\db\Migration;

/**
 * Class m190109_174428_notification
 */
class m190109_174428_notification extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(app\modules\org\models\Orgs::tableName(), 
                'notification_emails', 
                yii\db\pgsql\Schema::TYPE_STRING." NULL");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190109_174428_notification cannot be reverted.\n";

        return false;
    }
}
