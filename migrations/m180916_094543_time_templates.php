<?php

use yii\db\Migration;

/**
 * Class m180916_094543_time_templates
 */
class m180916_094543_time_templates extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(\app\modules\time\models\TimeTemplate::tableName(), [
            'id' => 'SERIAL PRIMARY KEY',
            'org_id' => \yii\db\pgsql\Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => \yii\db\pgsql\Schema::TYPE_STRING . '(256) NOT NULL',
        ]);
        $this->createTable(\app\modules\time\models\TimeTemplateItem::tableName(), [
            'id' => 'SERIAL PRIMARY KEY',
            'template_id' => \yii\db\pgsql\Schema::TYPE_INTEGER . ' NOT NULL',
            'group' => \yii\db\pgsql\Schema::TYPE_INTEGER . ' NULL',
            'type' => \yii\db\pgsql\Schema::TYPE_INTEGER . ' DEFAULT 0 ',
            'time' => \yii\db\pgsql\Schema::TYPE_TIME . ' NOT NULL',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180916_094543_time_templates cannot be reverted.\n";

        return false;
    }
}
