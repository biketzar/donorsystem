<?php

use yii\db\Migration;

/**
 * Class m180902_073241_donation_of_blood_components
 */
class m180902_073241_donation_of_blood_components extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn(app\modules\org\models\Orgs::tableName(), 
                'enable_donation_of_blood_components', 
                yii\db\pgsql\Schema::TYPE_SMALLINT.' DEFAULT 0');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180902_073241_donation_of_blood_components cannot be reverted.\n";

        return false;
    }
}
