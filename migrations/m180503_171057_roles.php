<?php

use yii\db\Migration;

/**
 * Class m180503_171057_roles
 */
class m180503_171057_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $manager = \Yii::$app->authManager;
        
        $role = $manager->createRole('admin');
        $role->description = 'Administrator';
        $manager->add($role);
        
        $role = $manager->createRole('manager');
        $role->description = 'Manager';
        $manager->add($role);
        
        $role = $manager->createRole('donor');
        $role->description = 'Donor';
        $manager->add($role);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $manager = \Yii::$app->authManager;
        
        $role = $manager->createRole('admin');
        $manager->remove($role);
        
        $role = $manager->createRole('manager');
        $manager->remove($role);
        
        $role = $manager->createRole('donor');
        $manager->remove($role);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180503_171057_roles cannot be reverted.\n";

        return false;
    }
    */
}
