<?php

use yii\db\Migration;

/**
 * Class m180512_082217_fix_donor
 */
class m180512_082217_fix_donor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->renameColumn('Donor', 'resus', 'rhesus');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180512_082217_fix_donor cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180512_082217_fix_donor cannot be reverted.\n";

        return false;
    }
    */
}
