<?php
namespace app\controllers\user;
use yii\web\NotFoundHttpException;
use dektrium\user\controllers\RegistrationController as BaseRegistrationController;
class RegistrationController extends BaseRegistrationController
{
    public function beforeAction($action) {
        $this->addAfterRegister();
        return parent::beforeAction($action);
    }
    
    public function addAfterRegister()
    {
        $this->on(self::EVENT_AFTER_REGISTER, function($event){
            if(!\Yii::$app->user->isGuest)
            {
                return;
            }
            $form = $event->getForm();
            $user = \app\models\User::findIdentityByUsername($form->username);
            if($user 
                    && $user->validatePassword($form->password) 
                    && \Yii::$app->user->login($user))
            {
                \Yii::$app->response->redirect('/donor/default/update');
            }
        });
    }
    
    
    public function actionConfirm($id, $code)
    {
        $this->finder->setUserQuery(\dektrium\user\models\User::find());
        return parent::actionConfirm($id, $code);
    }
    
    /**
     * Displays the registration page.
     * After successful registration if enableConfirmation is enabled shows info message otherwise
     * redirects to home page.
     *
     * @return string
     * @throws \yii\web\HttpException
     */
    public function actionRegister()
    {
        if (!$this->module->enableRegistration) {
            throw new NotFoundHttpException();
        }

        /** @var RegistrationForm $model */
        $model = \Yii::createObject(\app\models\RegistrationForm::className());
        $event = $this->getFormEvent($model);

        $this->trigger(self::EVENT_BEFORE_REGISTER, $event);

        $this->performAjaxValidation($model);

        if ($model->load(\Yii::$app->request->post()) && $model->register()) {
            $this->trigger(self::EVENT_AFTER_REGISTER, $event);

            return $this->render('/message', [
                'title'  => \Yii::t('user', 'Your account has been created'),
                'module' => $this->module,
            ]);
        }

        return $this->render('register', [
            'model'  => $model,
            'module' => $this->module,
        ]);
    }
    
    
}