<?php
/**
 * Created by PhpStorm.
 * User: borovovaleksandr
 * Date: 10/06/2018
 * Time: 15:45
 */

namespace app\controllers\user;

use dektrium\user\models\LoginForm;
use dektrium\user\controllers\SecurityController as BaseSecurityController;

class SecurityController extends BaseSecurityController
{
    public function behaviors()
    {
        return [
            'rateLimiter' => [
                'class' => \app\components\RateLimiter::className(),
                'enableRateLimitHeaders' => FALSE,
                'user' => new \app\components\IpLimiter()
            ],
        ];
    }
    
    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            //$this->goHome();
            return $this->redirect(['/../donation/default/index']);
        }

        /** @var LoginForm $model */
        $model = \Yii::createObject(LoginForm::className());
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);

        if ($model->load(\Yii::$app->getRequest()->post()) && $model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            return $this->goBack();
        }

        return $this->render('login', [
            'model'  => $model,
            'module' => $this->module,
        ]);
    }
}