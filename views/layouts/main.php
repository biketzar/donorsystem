<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
AppAsset::register($this);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => '/favicon.ico']);
?><?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">

    <nav class="navbar navbar-expand general-navbar">
        <div class="collapse navbar-collapse justify-content-sm-center" id="navbarNavDropdown">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="navbar-brand" href="/">
                        <img height="25" alt="Система для доноров" src="/web/images/donorsystem_logo.png">
                    </a>
                </li>

                <?php if (!Yii::$app->user->isGuest) : ?>

                <!-- Выпадающее responsive меню -->
                    <li class="nav-item dropdown d-md-none ">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Меню
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a href="/donation/default" class="nav-link">Донации</a>
                            <a href="/donor/default/view" class="nav-link ">Настройки</a>

                            <?php if(Yii::$app->user->isManager() or Yii::$app->user->isAdmin()) :?>
                                <a href="/org/default" class="nav-link">Организации</a>
                                <a href="/donor/default/list" class="nav-link">Доноры</a>
                                <a href="/time/default" class="nav-link">Время</a>
                            <?php endif; ?>

                            <?php if(Yii::$app->user->isDonor()) :?>
                                <a href="/org/default/donororg" class="nav-link">Сдать кровь</a>
                                <a href="/donor/default/view" class="nav-link ">Настройки</a>
                            <?php endif; ?>
                        </div>
                    </li>
                <!-- **************************** -->

                    <li class="nav-item d-none d-md-block <?=(strpos($_SERVER['REQUEST_URI'], 'donation') ? 'active' : '')?>">
                        <a href="/donation/default" class="nav-link">Донации</a>
                    </li>

                    <?php if(Yii::$app->user->can('manager') or Yii::$app->user->can('admin')) :?>

                        <li class="nav-item d-none d-md-block <?=(strpos($_SERVER['REQUEST_URI'], 'org/') ? 'active' : '')?>">
                            <a href="/org/default" class="nav-link">Организации</a>
                        </li>

                        <li class="nav-item d-none d-md-block <?=(strpos($_SERVER['REQUEST_URI'], 'donor/default/list') ? 'active' : '')?>">
                            <a href="/donor/default/list" class="nav-link">Доноры</a>
                        </li>

                        <li class="nav-item d-none d-md-block <?=(strpos($_SERVER['REQUEST_URI'], 'time/') ? 'active' : '')?>">
                            <a href="/time/default" class="nav-link">Время</a>
                        </li>

                    <?php endif; ?>

                    <?php if(Yii::$app->user->can('donor')) :?>

                        <li class="nav-item d-none d-md-block <?=(strpos($_SERVER['REQUEST_URI'], 'donororg') ? 'active' : '')?>">
                            <a href="/org/default/donororg" class="nav-link">Сдать кровь</a>
                        </li>

                        

                    <?php endif; ?>
                    <li class="nav-item d-none d-md-block <?=(strpos($_SERVER['REQUEST_URI'], 'donor/default/view') ? 'active' : '')?>">
                        <a href="/donor/default/view" class="nav-link ">Настройки</a>
                    </li>

                <?php else :?>

                    <li class="nav-item <?=(preg_match('$(donor\b|org\b|donation\b|time\b)$', $_SERVER['REQUEST_URI']) ? 'active' : '')?>">
                        <a class="nav-link" href="/user/login">Личный кабинет</a>
                    </li>
                <?php endif; ?>

                <li class="nav-item <?=(strpos($_SERVER['REQUEST_URI'], 'donorinfo') ? 'active' : '')?>">
                    <a class="nav-link" href="/donorinfo">Донору</a>
                </li>
                <li class="nav-item <?=(strpos($_SERVER['REQUEST_URI'], 'orginfo') ? 'active' : '')?>">
                    <a class="nav-link" href="/orginfo">Медучреждению</a>
                </li>
                <?php if(!\Yii::$app->user->isGuest){
                    ?>
                <li class="nav-item <?=(strpos($_SERVER['REQUEST_URI'], 'notifications/') ? 'active' : '')?>">
                    <a class="nav-link" href="/notifications/default/index">
                        Уведомления
                        <span class="notification-count"><?= app\modules\rest\components\NotificationHelper::getNotSeenCount()?></span></a>
                </li>
                <li class="nav-item">
                    <?=yii\helpers\Html::beginForm('/user/logout', 'POST');?>
                    <?=yii\helpers\Html::button('Выход', ['type' => 'submit', 'class' => "list-group-item list-group-item-action", 'style' => 'cursor:pointer;']);?>
                    <?=yii\helpers\Html::endForm();?>
                </li>
                    <?php
                
                }?>
            </ul>

        </div>
    </nav>

    <div class="container" id="main-container">
<!--        //= Breadcrumbs::widget([
//            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
//        ]) -->
        <?php /*Alert::widget()*/ ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
            <p class="pull-left">&copy; donorsystem.org <?= date('Y') ?></p>
    </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
