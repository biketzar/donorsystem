<?php
/**
 * Created by PhpStorm.
 * User: borovovaleksandr
 * Date: 10/06/2018
 * Time: 16:16
 */
?>

<?php $this->beginContent('@app/views/layouts/main.php'); ?>
<div class="row">
    <div class="col-xs-4 col-sm-3 col-lg-2" id="sidebar">
        <div class="list-group">
            <?php
            if(\Yii::$app->user->isManager() || \Yii::$app->user->isAdmin())
            {
              ?>
                <a href="/org/default/" class="list-group-item list-group-item-action <?=(strpos($_SERVER['REQUEST_URI'], 'org/') ? 'active' : '')?>">
                    Пункты
                </a>
                <a href="/time/default" class="list-group-item list-group-item-action <?=(strpos($_SERVER['REQUEST_URI'], 'time/') ? 'active' : '')?>">Время</a>
              <?php  
            }
            ?>
            <a href="/donation/default" class="list-group-item list-group-item-action <?=(strpos($_SERVER['REQUEST_URI'], 'donation/') ? 'active' : '')?>">Донации</a>
            <a href="/donor/default/view" class="list-group-item list-group-item-action <?=(strpos($_SERVER['REQUEST_URI'], 'donor/') ? 'active' : '')?>">Настройки</a>
            <?php if(!\Yii::$app->user->isGuest){
                echo \yii\helpers\Html::a('Уведомления', '/notifications/default/index', [
                    'class' => 'list-group-item list-group-item-action '.(strpos($_SERVER['REQUEST_URI'], 'notifications/') ? 'active' : '')] );
                echo yii\helpers\Html::beginForm('/user/logout', 'POST');
                echo yii\helpers\Html::button('Выход', ['type' => 'submit', 'class' => "list-group-item list-group-item-action", 'style' => 'cursor:pointer;']);
                echo yii\helpers\Html::endForm();
            }?>
        </div>

    </div><!--/.sidebar-offcanvas-->
    <div class="col-xs-12 col-sm-9 col-lg-10">
<!--            <p class="pull-left visible-xs">-->
<!--                <button type="button" class="btn btn-default btn-xs btn-offcanvas" data-toggle="offcanvas">Меню</button>-->
<!--            </p>-->
        <?php echo $content; ?>
    </div><!--/.col-xs-12.col-sm-9-->


</div><!--/row-->
<?php $this->endContent();