<?php

/* @var $this yii\web\View */

$this->title = 'Донору';
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('//developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js');
$this->registerJsFile('//maps.googleapis.com/maps/api/js?key=AIzaSyB0UVyJGCyfHktevDxKHqFOPT50NsqzrPc');
?>
<div class="site-about">
    <?php
    if($orgList)
    { ?>
    <h4>Медицинские учреждения-партнеры</h4>
    Организации, в которых можно сдать кровь без очереди и в удобное время:
    <div>
        <input type="text" id="listbox-input" placeholder="Поиск" class="form-control">
        <ul id="listbox-ul">
        <?php
        foreach($orgList as $org)
        {
            ?>
            <li>
                <?= yii\helpers\Html::a($org['name'], $org['link'], ['target' => '_blank', 'rel' => 'nofollow'])?>
            </li>
            <?php
        }
        ?>
        </ul>
    </div>
    
    <script type="text/javascript">
        window.addEventListener('load', function(){
            $('#listbox-input').keyup(function(e){
                search(this);
            });
            function search(input) {
                var input, filter, ul, li, a, i;
                filter = input.value.toUpperCase();
                ul = document.getElementById("listbox-ul");
                li = ul.getElementsByTagName('li');
                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("a")[0];
                    if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                        li[i].style.display = "";
                    } else {
                        li[i].style.display = "none";
                    }
                }
            }
            
        });
    </script>
    
    
    <div id="map" style="width: 100%;height:350px;"></div>
    <script type="text/javascript">
    window.addEventListener('load', function(){
        var map;
        var orgList = <?= \yii\helpers\Json::encode(is_array($orgList) ? $orgList : []);?>;
        var infowindows = [];
        initialize();
        function initialize() {
            var mapOptions = {
              zoom: 10,
              center: new google.maps.LatLng(60.007743, 30.373234),
              mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            
            map = new google.maps.Map(document.getElementById('map'), mapOptions);
            
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                 var p = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                 map.setCenter(p);
                }, function() {
                });
            }
            var markers = orgList.map(function(org, i) {
                var infowindow = new google.maps.InfoWindow({
                    content: org.name
                });
                infowindows.push(infowindow);
                var marker = new google.maps.Marker({
                    position: {lat: org.point[0], lng: org.point[1]},
                  });
                marker.addListener('click', function() {
                    infowindows.forEach(function(iw){
                        iw.close()
                    });
                    infowindow.open(map, marker);
                  });
                return marker;
            });
            var markerCluster = new MarkerClusterer(map, markers,{imagePath: '//developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
        }
    });
    </script>
    <?php
    }
    ?>
    <h4>Противопоказания</h4>

    <p>
        Основные:
        <ul>
        <li>
            Вес менее 50 кг
        </li>
        <li>
            Температура выше 37°С
        </li>
        <li>
            Cистолическое давление менее 90 или выше 160 мм.рт.ст
		</li>
		<li>
			Диастолическое давление менее 60 или выше 100 мм.рт.ст
        </li>
        <li>
            Зрение ниже -6 Д
        </li>
        </ul>
        <!--a href="https://donor.spb.ru/contraind/" target="_blank">Расширенный список &raquo;</a-->
    </p>

    <h4>Приложения</h4>

    <a href="https://itunes.apple.com/ru/app/donor/id1404384111?l=ru&mt=8" target="_blank"><img class="icon-appstore" src="/web/images/appstore.svg"></a>
    <a href="https://play.google.com/store/apps/details?id=p0mami.mark.donorapp" target="_blank"><img class="icon-googleplay" src="/web/images/googleplay.png"></a>

</div>
