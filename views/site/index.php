<?php

/* @var $this yii\web\View */

$this->title = 'Система для доноров';
?>
<div class="site-index">

    <header class="general-header" style="width:100%; background-image: url(<?='/web/images/main/' . rand(1, 8) . '.jpg' ?>);background-size: cover;"></header>

    <div class="body-content general-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Регистрация</h2>

                <p>После регистрации можно без очереди сдать кровь
                    в 1 медицинским университете, 122 и Елизаветинской больницах, а также на днях донора университетов.</p>

                <p><a class="btn btn-default" href="/user/login">Войти &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Донору</h2>

                <p>Перед сдачей крови ознакомьтесь с полным списком медучреждений-партнеров и противопоказаниями к донорству,
                    скачайте приложение для iOS или Android.
                </p>

                <p><a class="btn btn-default" href="/donorinfo">Перейти &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Медучреждению</h2>

                <p>Как присоединиться? Условия сотрудничества, юридические ограничения, необходимое оборудование и наша история и ценности.</p>

                <p><a class="btn btn-default" href="/orginfo">Читать &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
