<?php
namespace app\components;

class IpLimiter implements \yii\filters\RateLimitInterface
{
    CONST LIMIT = 10;
    /**
     * Returns the maximum number of allowed requests and the window size.
     * @param \yii\web\Request $request the current request
     * @param \yii\base\Action $action the action to be executed
     * @return array an array of two elements. The first element is the maximum number of allowed requests,
     * and the second element is the size of the window in seconds.
     */
    public function getRateLimit($request, $action)
    {
      //не более 100 запросов в течении 600 секунд
      return [self::LIMIT, 60]; 
    }
    
    /**
     * Loads the number of allowed requests and the corresponding timestamp from a persistent storage.
     * @param \yii\web\Request $request the current request
     * @param \yii\base\Action $action the action to be executed
     * @return array an array of two elements. The first element is the number of allowed requests,
     * and the second element is the corresponding UNIX timestamp.
     */
    public function loadAllowance($request, $action)
    {
      //$count - считаем сколько уже запросов совершил юзер
        $count = (int)\Yii::$app->cache->get($this->getKey(), 0);
        return [self::LIMIT - $count, time()];
    }
    
    /**
     * Saves the number of allowed requests and the corresponding timestamp to a persistent storage.
     * @param \yii\web\Request $request the current request
     * @param \yii\base\Action $action the action to be executed
     * @param int $allowance the number of allowed requests remaining.
     * @param int $timestamp the current timestamp.
     */
    public function saveAllowance($request, $action, $allowance, $timestamp)
    {
        \Yii::$app->cache->set($this->getKey(), self::LIMIT - $allowance, 10);
    }
    
    protected function getKey()
    {
        return 'ip-filter-'.\Yii::$app->request->getRemoteIP();
    }
}

