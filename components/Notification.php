<?php
namespace app\components;

use app\models\User;

/**
 * Created by PhpStorm.
 * User: borovovaleksandr
 * Date: 02/01/2019
 * Time: 20:10
 */

class Notification {
    private $iosToken = '';
    private $androidToken = '';
    public $iOSKeyPassword;
    public $androidAPIKey;
    public $iOSTestToken;
    public $androidTestToken;
    private $apnsServerDev = 'ssl://gateway.sandbox.push.apple.com:2195';
    private $apnsServer = 'ssl://gateway.push.apple.com:2195';
    private $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
    
    /**
     * 
     * @param int $id
     */
    protected function setTokens($id)
    {
        $tokens = User::find()->where(['id' => $id])
                ->select(['ios_token', 'android_token'])
                ->limit(1)
                ->asArray()->one();
        if($tokens)
        {
            $this->iosToken = $tokens["ios_token"];
            $this->androidToken = $tokens["android_token"];
        }
        else
        {
            $this->iosToken = NULL;
            $this->androidToken = NULL;
        }
    }
    
    /**
     * отправка уведомления в приложение
     * @param int $userId
     * @param string $title
     * @param string $message
     */
    public function sendNotification($userId, $title, $message)
    {
        $this->setTokens($userId);
        if ($this->iosToken) {
            $notification = [];
            $notification['aps'] = [
                'alert' => trim(strip_tags($message)),
                'sound' => 'default',
                'badge' => 2,
            ];
            $this->sendIos($notification);
        }

        if ($this->androidToken) {
            $notification = [
                'title' => trim(strip_tags($title)),
                'body' => trim(strip_tags($message)),
                'icon' =>'myIcon',
                'sound' => 'mySound'
            ];
            $this->sendAndroid($notification);
        }
    }
    
    /**
     * 
     * @param \app\modules\donation\models\Donation $donation
     */
    public function denial($donation)
    {
        $this->setTokens($donation->donor_id);
        $text = "Запись на ".date("H:i d.m.Y", strtotime($donation->time->date_time))." отменена";
        if ($this->iosToken) {
            $message = [];
            $message['aps'] = [
                'alert' => $text,
                'sound' => 'default',
                'badge' => 2,
            ];
            $this->sendIos($message);
        }

        if ($this->androidToken) {
            $notification = [
                'title' => $text,
                'body' => '',
                'icon' =>'myIcon',
                'sound' => 'mySound'
            ];
            $this->sendAndroid($notification);
        }
    }
    
    /**
     * 
     * @param \app\modules\donation\models\Donation $donation
     */
    public function complete($donation) {
        $this->setTokens($donation->donor_id);
        if ($this->iosToken) {
            $message = [];
            $message['aps'] = [
                'alert' => 'Сдача крови подтверждена!',
                'sound' => 'default',
                'badge' => 2,
            ];
            $this->sendIos($message);
        }

        if ($this->androidToken) {
            $notification = [
                'title' => 'Сдача крови подтверждена!',
                'body' => 'Спасибо, донор',
                'icon' =>'myIcon',
                'sound' => 'mySound'
            ];
            $this->sendAndroid($notification);
        }
    }

    public function test() {
        $this->androidToken = $this->androidTestToken;
        $this->iosToken = $this->iOSTestToken;

//        echo "android token is: " . $this->androidToken . "<br/>";
//        echo "ios token is: " . $this->iosToken . "<br/>";
//        echo "android api key is: " . $this->androidAPIKey . "<br/>";

        if ($this->iosToken) {
            $message = [];
            $message['aps'] = [
                'alert' => 'Сдача крови подтверждена!',
                'sound' => 'default',
                'badge' => 2,
            ];
            $this->sendIos($message);
        }

        if ($this->androidToken) {
            $notification = [
                'title' => 'Сдача крови подтверждена!',
                'body' => 'Спасибо, донор',
                'icon' =>'myIcon',
                'sound' => 'mySound'
            ];
            $this->sendAndroid($notification);
        }
    }

    private function sendIos($message) {
        // donorsystem.pem if apnsServer
        $pushCertAndKeyPemFile = \Yii::getAlias('@app') . '/cert/donorsystem.pem';
        $stream = stream_context_create();
        stream_context_set_option($stream, 'ssl', 'passphrase', $this->iOSKeyPassword);
        stream_context_set_option($stream, 'ssl', 'local_cert', $pushCertAndKeyPemFile);

        $connectionTimeout = 20;
        $connectionType = STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT;
        $connection = stream_socket_client($this->apnsServer, $errorNumber, $errorString, $connectionTimeout, $connectionType, $stream);

//        if (!$connection){
//            echo "Failed to connect to the APNS server. Error no = $errorNumber<br/>";
//            exit;
//        } else {
//            echo "Successfully connected to the APNS. Processing...</br>";
//        }

        $payload = json_encode($message);
        $notification = chr(0) . pack('n', 32) . pack('H*', $this->iosToken) . pack('n', strlen($payload)) . $payload;

        $wroteSuccessfully = fwrite($connection, $notification, strlen($notification));
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/runtime/logs/ios.log', date('c')
                ."\n".($wroteSuccessfully ? 'Successfully sent the message' : 'Could not send the message')
                ."\n".print_r($message, true)."\n", FILE_APPEND);
//        if (!$wroteSuccessfully) {
//            echo "Could not send the message<br/>";
//        } else {
//            echo "Successfully sent the message<br/>";
//        }

        fclose($connection);
    }

    private function sendAndroid($notification) {
        $extraNotificationData = [
            "moredata" =>'dd'
        ];

        $fcmNotification = [
            'to'        => $this->androidToken,
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=' . $this->androidAPIKey,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        
        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/runtime/logs/android.log', date('c')
                ."\n".print_r($result, true)
                ."\n".print_r($notification, true)."\n", FILE_APPEND);
//        echo $result;
    }
}
