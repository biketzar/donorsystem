<?php
namespace app\components;

class User extends \yii\web\User
{
    public function init() {
        parent::init();
        $this->loginUrl = '/user/login';
    }
    
    public function isDonor()
    {
        return !$this->isGuest 
                && method_exists($this->getIdentity(), 'isDonor')
                && $this->getIdentity()->isDonor();
    }
    
    public function isManager()
    {
        return !$this->isGuest 
                && method_exists($this->getIdentity(), 'isManager')
                && $this->getIdentity()->isManager();
    }
    
    public function isAdmin()
    {
        return !$this->isGuest 
                && method_exists($this->getIdentity(), 'isAdmin')
                && $this->getIdentity()->isAdmin();
    }
    
    public function getOrgIDs()
    {
        static $list;
        if(is_array($list))
        {
            return $list;
        }
        if($this->isGuest)
        {
            return $list = [];
        }
        return $list = \app\modules\org\models\Staff::find()
                ->select('org_id')
                ->where(['user_id' => $this->getIdentity()->id])->column();
    }
}