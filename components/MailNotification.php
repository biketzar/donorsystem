<?php
namespace app\components;
class MailNotification extends \yii\base\Component
{
    /**
     * уведомление "Запись на сдачу"
     * @param \app\modules\donation\models\Donation $donation
     */
    public function enroll($donation)
    {
        if(!$donation 
                || !$donation->org
                || !$donation->donor
                || !$donation->time)
        {
            return;
        }
        $emails = $donation->org->notification_emails;
        if(!$emails)
        {
            return;
        }
        $params = [];
        $params['link'] = \yii\helpers\Url::to(['/donation/default/index', 'Donation' => ['id' => $donation->id]], true);
        $params['name'] = "{$donation->donor->surname} {$donation->donor->name} {$donation->donor->patronymic}";
        $params['bDay'] = strtotime($donation->donor->dob) !== false ? date('d.m.Y', strtotime($donation->donor->dob)) : 'Не указано';
        $params['time'] = date('d.m.Y H:i', strtotime($donation->time->date_time));
        foreach(explode(',', $emails) as $email)
        {
            \Yii::$app->mailer->compose('@app/mail/notifications/enroll', $params)
                ->setFrom('no-reply@donorsystem.org')
                ->setTo(trim($email))
                ->setSubject('Уведомление о записи на донацию')
                ->send();
        }
        $partialRenderedMail = \Yii::$app->view->render('@app/mail/notifications/enroll', $params);
        foreach($donation->org->users as $user)
        {
            \app\modules\notifications\models\Notification::addNotification(
                    'Уведомление о записи на донацию', 
                    $partialRenderedMail, 
                    $user->id);
        }
        
    }
    
    /**
     * Уведомление об отмене сдачи по медицинским причинам
     * @param \app\modules\donation\models\Donation $donation
     */
    public function denial($donation)
    {
        if(!$donation 
                || !$donation->org
                || !$donation->donor
                || !$donation->time)
        {
            return;
        }
        $emails = $donation->org->notification_emails;
        if(!$emails)
        {
            return;
        }
        $params = [];
        $params['link'] = \yii\helpers\Url::to(['/donation/default/index', 'Donation' => ['id' => $donation->id]], true);
        $params['name'] = "{$donation->donor->surname} {$donation->donor->name} {$donation->donor->patronymic}";
        $params['bDay'] = strtotime($donation->donor->dob) !== false ? date('d.m.Y', strtotime($donation->donor->dob)) : 'Не указано';
        $params['time'] = date('d.m.Y H:i', strtotime($donation->time->date_time));
        $params['reason'] = $donation->reason;
        foreach(explode(',', $emails) as $email)
        {
            \Yii::$app->mailer->compose('@app/mail/notifications/denial', $params)
                ->setFrom('no-reply@donorsystem.org')
                ->setTo(trim($email))
                ->setSubject('Уведомление об отмене сдачи')
                ->send();
        }
        $partialRenderedMail = \Yii::$app->view->render('@app/mail/notifications/denial', $params);
        \app\modules\notifications\models\Notification::addNotification(
                    'Уведомление об отмене сдачи', 
                    $partialRenderedMail, 
                    $donation->donor->id);
    }
    
    /**
     * Уведомление об отмене записи на сдачу донором
     * @param \app\modules\donation\models\Donation $donation
     */
    public function cancel($donation)
    {
        if(!$donation 
                || !$donation->org
                || !$donation->donor
                || !$donation->time)
        {
            return;
        }
        $emails = $donation->org->notification_emails;
        if(!$emails)
        {
            return;
        }
        $params = [];
        $params['link'] = \yii\helpers\Url::to(['/donation/default/index', 'Donation' => ['id' => $donation->id]], true);
        $params['name'] = "{$donation->donor->surname} {$donation->donor->name} {$donation->donor->patronymic}";
        $params['bDay'] = strtotime($donation->donor->dob) !== false ? date('d.m.Y', strtotime($donation->donor->dob)) : 'Не указано';
        $params['time'] = date('d.m.Y H:i', strtotime($donation->time->date_time));
        $params['reason'] = $donation->reason;
        foreach(explode(',', $emails) as $email)
        {
            \Yii::$app->mailer->compose('@app/mail/notifications/cancel', $params)
                ->setFrom('no-reply@donorsystem.org')
                ->setTo(trim($email))
                ->setSubject('Уведомление об отмене записи на сдачу донором')
                ->send();
        }
        $partialRenderedMail = \Yii::$app->view->render('@app/mail/notifications/cancel', $params);
        foreach($donation->org->users as $user)
        {
            \app\modules\notifications\models\Notification::addNotification(
                    'Уведомление об отмене записи на сдачу донором', 
                    $partialRenderedMail, 
                    $user->id);
        }
    }
}

