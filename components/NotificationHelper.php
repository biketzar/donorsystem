<?php
namespace app\components;
class NotificationHelper extends \yii\base\Component
{
    /**
     * уведомление "Запись на сдачу"
     * @param \app\modules\donation\models\Donation $donation
     */
    public static function enroll($donation)
    {
        \Yii::$app->mailNotification->enroll($donation);
    }
    
    /**
     * Уведомление об отмене сдачи по медицинским причинам
     * @param \app\modules\donation\models\Donation $donation
     */
    public static function denial($donation)
    {
        \Yii::$app->mailNotification->denial($donation);
        \Yii::$app->appNotification->denial($donation);
    }
    
    /**
     * Уведомление об отмене записи на сдачу донором
     * @param \app\modules\donation\models\Donation $donation
     */
    public static function cancel($donation)
    {
        \Yii::$app->mailNotification->cancel($donation);
    }
    
    /**
     * Уведомление о сдаче
     * @param \app\modules\donation\models\Donation $donation
     */
    public static function complete($donation)
    {
        \Yii::$app->appNotification->complete($donation);
    }
}