<?php
namespace app\components;

class VKontakte extends \dektrium\user\clients\VKontakte
{
    CONST API_VERSION = 5.73;
    /**
     * 
     * @return boolean|string TRUE или текст ошибки
     */
    public function reInitUserAttributes()
    {
        $response = $this->api('users.get.json', 'GET', [
            'fields' => implode(',', $this->attributeNames),
            'version' => self::API_VERSION
        ]);
        if(array_key_exists('response', $response))
        {
            $attributes = array_shift($response['response']);
            $accessToken = $this->getAccessToken();
            if (is_object($accessToken)) {
                $accessTokenParams = $accessToken->getParams();
                unset($accessTokenParams['access_token']);
                unset($accessTokenParams['expires_in']);
                $attributes = array_merge($accessTokenParams, $attributes);
            }
            $this->setUserAttributes($attributes);
            return TRUE;
        }
        elseif(array_key_exists('error', $response) 
                && array_key_exists('error_msg', $response['error']))
        {
            return $response['error']['error_msg'];
        }
        return 'Undefined error';
    }
    
    protected function initUserAttributes()
    {
        $response = $this->api('users.get.json', 'GET', [
            'fields' => implode(',', $this->attributeNames),
            'version' => self::API_VERSION
        ]);
        $attributes = array_shift($response['response']);

        $accessToken = $this->getAccessToken();
        if (is_object($accessToken)) {
            $accessTokenParams = $accessToken->getParams();
            unset($accessTokenParams['access_token']);
            unset($accessTokenParams['expires_in']);
            $attributes = array_merge($accessTokenParams, $attributes);
        }

        return $attributes;
    }
}

