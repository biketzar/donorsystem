<?php
namespace app\commands;
use yii\console\Controller;
use yii\console\ExitCode;

class TasksController extends Controller
{
    /**
     * автотическое закрытие регистрации
     * @return type
     */
    public function actionCloseRegistration()
    {
        echo "Start \n";
        echo date("Y-m-d H:i:s")." \n";
        $updateRowsCount = 0;
        $orgsTableName = \app\modules\org\models\Orgs::getTableSchema()->fullName;
        $updateIdList = [];
        $rows = \app\modules\time\models\Time::find()->where(
                ['AND', 
                    ['published' => true],
                    "reg_stop_hours > 0 AND date_time > :date_time",
                    "org_id IN (SELECT id FROM \"{$orgsTableName}\" WHERE enable_stop_registration=1)",
                ])->addParams([':date_time' => date('Y-m-d H:i:s')])->asArray()->all();
        foreach($rows as $row)
        {
            $time = (double) preg_replace('/[^\d]/', '', $row['date_time']);
            $stopTime = (double)date("YmdHis", strtotime(date("Y-m-d H:i:s")) + (int)$row['reg_stop_hours'] * 3600);            
            if($time > 0 && $time <= $stopTime)
            {
                $updateIdList[] = $row['id'];
            }
        }
        if($updateIdList)
        {
            $updateRowsCount = \app\modules\time\models\Time::updateAll(['published' => false], ['IN', 'id', $updateIdList]);
        }
        echo "{$updateRowsCount} rows updated\n";
        echo implode(', ', $updateIdList)."\n";
        echo "End \n";
        return ExitCode::OK;
    }
}